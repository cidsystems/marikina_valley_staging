<script src='https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=ch223xqxfn7t8v1kauqe1bf82k8ut75dwppo50p0d7l63eh7'></script>
<script>
	$(function () {
		tinymce.init({
			height : "50",
			selector: '#content',
			height: 500,
			theme: 'modern',
			fontsize_formats: "8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt",
			plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars code fullscreen',
			'insertdatetime media nonbreaking save table contextmenu directionality',
			'emoticons paste textcolor template colorpicker textpattern imagetools'
			],
			toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | numlist bullist | fontsizeselect | fontselect',
			toolbar2: 'print preview media | forecolor backcolor emoticons',
			templates: [
    {title: 'Template 1', description: 'Template 1', content: '<p>Hey [first name],</p> <p>I wanted to give you a short message to introduce to you Oojeema, the <strong>first </strong>accounting software designed specifically for Philippine businesses to make their bookkeeping and accounting processes so much simpler than you could ever imagine.</p> <p><img src="https://ourwebprojects.com/marikinavalley/uploads/tinymce_uploads/122123831-5cbd256567d7b.png" alt="" width="442" height="144" /></p><p>Want to experience headache-free accounting? Try <a href="http://www.oojeema.com">Oojeema</a> for 14 days FREE. You may also shoot us a message if you have questions.</p><p>Regards</p><p>Roberto D. Felicio</p><p>Cid Systems Solution Services</p><p><a href="http://www.cid-systems.com">www.cid-systems.com</a></p><p><a href="http://www.oojeema.com">www.oojeema.com</a></p>'}
  ],
	
			image_advtab: true,
			setup: function (editor) {
				editor.on('change', function () {
					editor.save();
				});
			},
			convert_urls: false,
      images_upload_handler: function (blobInfo, success, failure) { 
        var xhr, formData; 
        var json = '';

        xhr = new XMLHttpRequest();
        xhr.withCredentials = false; 
        xhr.open('POST', '<?=MODULE_URL?>ajax/ajax_create_upload'); 

        xhr.onload = function() {

          if (xhr.status != 200) { 
            failure('HTTP Error: ' + xhr.status); 
            return; 
          } 
					
          json = JSON.parse(xhr.response);
          success(json);
        };

        formData = new FormData(); 
        formData.append('file', blobInfo.blob(), blobInfo.filename());

        xhr.send(formData); 
      },
      setup: function (editor) {
        editor.on('change', function () {
          editor.save();
        });
      }
		});	
	});
	
</script>

<section class="content">
	<div class="box box-primary">
		<div class="box-body">
			<br>
			<form action="" method="post" class="form-horizontal" id="form">
				<div class="row">
					<div class="col-md-12">
					<div class="row">
						<div class="col-md-10">
								<?php
									echo $ui->formField('text')
										->setLabel('Subject')
										->setSplit('col-md-2', 'col-md-10')
										->setName('subject')
										->setId('subject')
										->setValue($subject)
										->setValidation('required')
										->draw($show_input);
								?>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<textarea id="content" name = "content"><?php echo $content; ?></textarea>
							</div>
							
							
						</div>
						<hr>
						<div class="row">
							<div class="col-md-12 text-center">
							<?php if($show_input):?>
							<?php if($stat != 'send'){ ?>
										<input type = "submit" id="send" class = "btn btn-primary" value="Send">
							<?php } else { ?>
								<input type = "submit" id="send" class = "btn btn-primary" value="Resend">
							<?php } ?>
								<input type = "button" id="draft" class = "btn btn-primary" value="Save as draft">
								<input type = "button" id="preview" class = "btn btn-primary" value="Preview as email">
								<?php endif ?>
								<!-- <a href="<?=MODULE_URL?>" class="btn btn-default" data-toggle="back_page">Cancel</a> -->
							</div>
						</div>
					</form>
				</div>
			</div>
		</section> 

		<!-- Preview Email Modal -->
		<div class="modal fade" id="preview_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Email Preview</h4>
        </div>
        <div class="modal-body">
		<html>
      	<head>
        <style>
          .body, table { font-size: 12px; }
          h4 { margin-bottom: 5px; }
          .body { background-color: #eee; font-family: Arial; }
          body { margin: 0; }
          html { height: 100%; }
          body, .body { min-height: 100%; }
          .body { padding: 15px 0; }
          .main { max-width: 600px; width: 100%; margin: 0 auto; box-sizing: border-box; }
          .container { background-color: #fff; padding: 15px; box-shadow: 0px 1px 2px 0px rgba(0,0,0,.1); }
          .logo { font-size: 16px; font-weight: 700; margin: 0 auto; display: block; }
          .header-caption { background-color: #19a9e5; margin: 15px 0; padding: 2px; color: #fff; font-family: Verdana; font-size: 14px; font-weight: 500; text-align: center; }
          .head, .foot-note, .head a, .foot-note a { color: #888; }
          .socicon img { vertical-align: middle; margin-right: 5px; }
          .socicon { margin-right: 15px; }
					img {max-width:100%;max-height:100%;}
          
        </style>
      </head>
      <body>
        <div class="body">
          <div class="main">
          
            <div class="head">
              <p></p>
            </div>
          
      		<div class="preview">
              <img src="https://ourwebprojects.com/marikinavalley/assets/home/images/LATESTMV.png" width="25%" class="logo" alt="Marikina Valley Medical Center">
              <div class="header-caption"></div>
              <div class="content" id="preview_message"></div>
            
            <div class="foot-note">
              <p>If you have some questions or you need some help. contact <a href="mailto:info@mvmc.com.ph"></a></p>
              <p>To keep updated on what's happening.</p>
              <a href="https://www.facebook.com/MarikinaValleyMedicalCenter/" class="fa fa-facebook">&nbsp;Like us on Facebook</a>
              <a href="https://www.instagram.com/marikinavalleymedicalcenter/" class="fa fa-instagram">&nbsp;Follow us on Instagram</a>

             <!--  <a href="http://ourwebprojects.com/raion/unsubscribe/".sha1($email.$var). " \" class=\"foot_note\"><img  height=\"16px\"/> Unsubscribe -->
              
            </div>
          </div>
        </div>
      </div>
      </body>
      </html>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
		<?php if ($show_input): ?>
			<script>
				$('form').submit(function(e) {
					e.preventDefault();

					$(this).find('.form-group').find('input, textarea, select').trigger('blur');
					if ($(this).find('.form-group.has-error').length == 0) {
						$.post('<?=MODULE_URL?>ajax/<?=$ajax_task?>', $(this).serialize() + '&status=send', function(data) {
							if (data.success) {
								window.location = data.redirect;
							}
						});
					} else {
						$(this).find('.form-group.has-error').first().find('input, textarea, select').focus();
					}
				});

				$('#draft').on('click', function() {

					$('#form').find('.form-group').find('input, textarea, select').trigger('blur');
					if ($('#form').find('.form-group.has-error').length == 0) {
						$.post('<?=MODULE_URL?>ajax/<?=$ajax_task?>', $('#form').serialize() + '&status=draft', function(data) {
							if (data.success) {
								window.location = data.redirect;
							}
						});
					} else {
						$('#form').find('.form-group.has-error').first().find('input, textarea, select').focus();
					}
				});

				$('#preview').on('click', function() {
					var content = $('#content').val();
					$('#preview_message').html(content);
					$('#preview_modal').modal('show');
				});
			</script>
		<?php endif ?>

		