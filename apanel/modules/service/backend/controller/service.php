<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->service_model	= new service_model();
		$this->session			= new session();
		$this->fields 			= array(
			'id',
			'image',
            		'service',
            		'content'
		);
	}

	public function listing() {
		$this->view->title = 'Medical Services';
		$data['ui'] = $this->ui;
		$all = (object) array('ind' => 'null', 'val' => 'Filter: All');
        	$this->view->load('service_list', $data);
	}

	public function create() {
		$this->view->title = 'Add Service';
		$data = $this->input->post($this->fields);
		$data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_create';
		$data['ajax_post'] = '';
		$data['show_input'] = true;
		$this->view->load('service', $data);
	}

	public function edit($id) {
		$this->view->title = 'Edit Service';
		$data = (array) $this->service_model->getServiceById($this->fields, $id);
        	$data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_edit';
		$data['ajax_post'] = "&id=$id";
		$data['show_input'] = true;
		$this->view->load('service', $data);
	}

	public function view($id) {
		$this->view->title = 'View Service';
		$data = (array) $this->service_model->getServiceById($this->fields, $id);
        	$data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_view';
		$data['show_input'] = false;
		$this->view->load('service', $data);
	}
	
	public function ajax($task) {
		$ajax = $this->{$task}();
		if ($ajax) {
			header('Content-type: application/json');
			echo json_encode($ajax);
		}
	}

	private function ajax_list() {
        $data  = $this->input->post(array('search', 'sort'));
        $search  = $data['search'];
        $sort  = $data['sort'];
    
        $pagination = $this->service_model->getService($this->fields, $sort , $search);
        $table = '';
        if (empty($pagination->result)) {
          $table = '<tr><td colspan="9" class="text-center"><b>No Records Found</b></td></tr>';
        }
        foreach ($pagination->result as $key => $row) {
          $table .= '<tr>';
          $dropdown = $this->ui->loadElement('check_task')
          ->addView()
          ->addEdit()
          ->addPrint()
          ->addDelete()
          ->addCheckbox()
          ->setValue($row->id)
          ->draw();
          $table .= '<td align = "center">' . $dropdown . '</td>';
          $table .= '<td>
          <img style = "width: 120px;
          height: 120px;
          border-radius: 20px;" src="' . str_replace('/apanel', '', BASE_URL) . "uploads/items/large/".$row->image.'"></td>';
		  $table .= '<td>' . $row->service . '</td>';
		  $table .= '<td>' . $row->content . '</td>';
		  $table .= '</tr>';
        }
        
        $pagination->table = $table;
        return $pagination;
      }
	

	private function ajax_create() {
		$image_uploader = new image_uploader();
        $filename = $image_uploader->setSize(array('large','thumb'))
        ->setFolderName('../uploads/items')
        ->getImage('item_image');

        $data = $this->input->post($this->fields);
        $textarea = addslashes($_POST['mytextarea']);	
        $data['content'] = $textarea;
		$data['image'] = $filename;
		
		$result = $this->service_model->saveService($data);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
		);
	}

	private function ajax_edit() {
		$image_uploader = new image_uploader();
        $filename = $image_uploader->setSize(array('large','thumb'))
        ->setFolderName('../uploads/items')
        ->getImage('item_image');

        $data = $this->input->post($this->fields);
		$textarea = addslashes($_POST['mytextarea']);	
		$data['content'] = $textarea;

		if(empty($filename)) {
			$image_name = $this->input->post('image-edit');
			$data['image'] = $image_name;
		} else {
			$data['image'] = $filename;
		}
		$id = $this->input->post('id');
		$result = $this->service_model->updateService($data, $id);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
		);
	}

	private function ajax_delete() {
		$delete_id = $this->input->post('delete_id');
		$error_id = array();
		if ($delete_id) {
			$error_id = $this->service_model->deleteService($delete_id);
		}
		return array(
			'success'	=> (empty($error_id)),
			'error_id'	=> $error_id
		);
	}


}