<script src='https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=ch223xqxfn7t8v1kauqe1bf82k8ut75dwppo50p0d7l63eh7'></script>
<script>
	$(function () {
		tinymce.init({
			height : "50",
			selector: '#content',
			<?php if($ajax_task == 'ajax_view') : ?>
				readonly: true,
			<?php endif;?>
			height: 500,
			theme: 'modern',
			fontsize_formats: "8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt",
			plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars code fullscreen',
			'insertdatetime media nonbreaking save table contextmenu directionality',
			'emoticons template paste textcolor colorpicker textpattern imagetools'
			],
			toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | numlist bullist | fontsizeselect | fontselect',
			toolbar2: 'print preview media | forecolor backcolor emoticons',
			image_advtab: true,
			setup: function (editor) {
				editor.on('change', function () {
					editor.save();
				});
			}
		});	
	});
</script>
<section class="content">
		<form action="" method="post" class="form-horizontal">
			<div class="nav-tabs-custom">
				<div class="tab-content no-padding">
					<div class="tab-pane active" style="padding: 15px">
						<div class="row">
							<div class="col-md-6">
								<?php
									echo $ui->formField('text')
										->setLabel('Title')
										->setSplit('col-md-1', 'col-md-11')
										->setName('title')
										->setId('title')
										->setValue($title)
										->setValidation('required')
										->draw($show_input);
								?>
							</div>
							<div class="col-md-6">
								<?php
									echo $ui->formField('text')
										->setLabel('Date')
										->setClass('datepicker-input')
										->setAddon('calendar')
										->setSplit('col-md-1', 'col-md-11')
										->setName('date')
										->setId('date')
										->setValue($date)
										->draw($show_input);
								?>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<textarea id="content" name = "content"><?php echo $content; ?></textarea>
							</div>
						</div><br>
						<div class="row">
							<div class="col-md-12">
								<div class = "col-md-8 col-md-offset-4">
									<div class="form-group">
										<input type="hidden" name="image-edit" id = "image-edit" value = "<?php echo $image; ?>">

										<label for = "image">
											<?php if($ajax_task == 'ajax_view')  { ?>

											<input id="image" name = "image" type="file" class = "hidden" disabled
											/>

											<?php } else if($ajax_task == 'ajax_edit') { ?>

											<input id="image" name = "image" type="file" class = "hidden"
											value = "<?php echo $image; ?>" accept="image/*"/>

											<?php } else if($ajax_task == 'ajax_create') { ?>
											<input id="image" name = "image" type="file" accept="image/*"/>
											<?php } ?>	
											
											<div style = "margin-left:5%">
												<span class="img-thumbnail">
													<?php echo '<img src="' . str_replace('/apanel', '', BASE_URL) . "uploads/items/large/".$image.'" class="img-responsive img-upload-view" alt ="" style = "height: 300px;">' ?>
												</span>
											</div>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 text-center" style="padding-bottom: 15px">
							<?php //echo $ui->drawSubmit($show_input); ?>
							<?php if ($ajax_task == 'ajax_view') { ?>
								<a href = "<?=MODULE_URL?>edit/<?php echo $id; ?>" class = "btn btn-primary">Edit</a>
							<?php } else { ?>
								<button type = "submit" class = "btn btn-primary">Publish</button> 
							<?php } if (($ajax_task == 'ajax_create' || $ajax_task == 'ajax_edit')) { ?>
								<button type = "button" id = "draft" class = "btn btn-info">Draft</button> 
							<?php } ?>
							<a href="<?=MODULE_URL?>" class="btn btn-default">Cancel</a>
						</div>
					</div>
				</div>
			</div>
		</form>
	</section>
<script>
	$('#image').on('change', function() {
		var img = new Image();
		var jinput = $(this);
		readURL($(this)[0], jinput, img);
	});
	function readURL(input, jinput, img) { 
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {		
				var x = `<span class="img-thumbnail">
				<img src="` + e.target.result + `" class="img-responsive img-upload-view">
			</span>`;

			jinput.closest('.form-group').find('label div').html(x);
			jinput.closest('.form-group').find('#image').addClass('hidden');
			};
		reader.readAsDataURL(input.files[0]);
		}
	}
</script>
<?php if($ajax_task == 'ajax_create'): ?>
	<script>

		if($("#image")[0].files.length == 0 ){
			$(".img-thumbnail").addClass("hidden");
		}
	</script>
<?php endif; ?>
<?php if ($show_input): ?>
	<script>
		$('form').submit(function(e) {
			e.preventDefault();
			$(this).find('.form-group').find('input, textarea, select').trigger('blur');
			if ($(this).find('.form-group.has-error').length == 0) {
				var formData = new FormData($('form')[0]);
				formData.append('item_image', $('#image')[0].files[0]);
				formData.append('id', '<?php echo $id ?>');
				formData.append('image-edit', '<?php echo $image ?>');
				$.ajax({
					url: '<?=MODULE_URL?>ajax/<?=$ajax_task?>',
					type: "POST",
					data: formData,
					processData: false,
					contentType: false,
					success: function(data){
						window.location = data.redirect;
					}
				});
			} else{
				$(this).find('.form-group.has-error').first().find('input, textarea , select'),focus();
			}
		});
		$('#draft').click(function(e) {
			e.preventDefault();
			$(this).find('.form-group').find('input, textarea, select').trigger('blur');
			if ($(this).find('.form-group.has-error').length == 0) {
				var formData = new FormData($('form')[0]);
				formData.append('item_image', $('#image')[0].files[0]);
				formData.append('id', '<?php echo $id ?>');
				formData.append('image-edit', '<?php echo $image ?>');
				$.ajax({
					<?php if ($ajax_task == 'ajax_create') { ?>
						url: '<?=MODULE_URL?>ajax/ajax_create_draft',
					<?php } else { ?>
						url: '<?=MODULE_URL?>ajax/ajax_edit_draft',
					<?php } ?>
					type: "POST",
					data: formData,
					processData: false,
					contentType: false,
					success: function(data){
						window.location = data.redirect;
					}
				});
			} else{
				$(this).find('.form-group.has-error').first().find('input, textarea , select'),focus();
			}
		});
	</script>
<?php endif ?>