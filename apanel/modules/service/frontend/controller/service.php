<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->service_model	= new service_model();
		$this->session			= new session();
		$this->fields 			= array(
			'id',
			'image',
			'service',
			'content'
			
		);
	}

	public function listing() {
		$data['service1'] = $this->service_model->getServiceFront($this->fields);
		$this->view->title	= 'Service List';
		$data['ui']			= $this->ui;
		$this->view->load('service_list', $data);
	}
	public function view($id) {
		$data = $this->input->post($this->fields);
		$data = (array) $this->service_model->getServiceById($this->fields, $id);
		$data['service1'] = $this->service_model->getServiceFront($this->fields);
		$data['ui']			= $this->ui;
		$data['show_input'] = false;
		$this->view->load('service', $data);
	}
}