<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui			= new ui();
		$this->news_model	= new news_model();
		$this->input		= new input();
		$this->fields		= array('id','image','title', 'content', 'pin', 'status', 'date');
	}

	public function listing() {
		$this->view->title	= 'News List';
		$data['ui']			= $this->ui;
		$this->view->load('news_list', $data);
	}

	public function create() {
		$this->view->title = 'News';
		$data = $this->input->post($this->fields);
		$data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_create';
		$data['ajax_post'] = '';
		$data['show_input'] = true;
		$this->view->load('news', $data);
	}

	// public function pin() {
	// 	$this->view->title = 'Pin';
	// 	$data['all'] = $this->news_model->getAllNews();
	// 	$data['ui'] = $this->ui;
	// 	$data['ajax_task'] = 'ajax_create_pin';
	// 	$data['ajax_post'] = '';
	// 	$data['show_input'] = true;
	// 	$this->view->load('pin_news', $data);
	// }

	public function view($id) {
		$this->view->title = 'News View';
		$data = (array) $this->news_model->getNewsById($this->fields, $id);
		$data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_view';
		$data['show_input'] = false;
		$this->view->load('news', $data);
	}

	public function edit($id) {
		$this->view->title = 'News Edit';
		$data = (array) $this->news_model->getNewsById($this->fields, $id);
		$data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_edit';
		$data['ajax_post'] = "&id=$id";
		$data['show_input'] = true;
		$this->view->load('news', $data);
	}

	public function ajax($ajax_function) {
		$result = $this->{$ajax_function}();
		header('Content-type: application/json');
		echo json_encode($result);
	}

	private function ajax_list() {
		$data  = $this->input->post(array('search'));
		$search  = $data['search'];

		$pagination = $this->news_model->getNewsList($search);
		$table = '';
		if (empty($pagination->result)) {
			$table = '<tr><td colspan="9" class="text-center"><b>No Records Found</b></td></tr>';
		}
		foreach ($pagination->result as $key => $row) {
			$table .= '<tr>';
			$dropdown = $this->ui->loadElement('check_task')
			->addView()
			->addEdit()
			->addPrint()
			->addDelete()
			->addCheckbox()
			->setValue($row->id)
			->draw();
			$table .= '<td align = "center">' . $dropdown . '</td>';
			$table .= '<td>
			<img style = "width: 120px;
			height: 120px;
			border-radius: 20px;" src="' . str_replace('/apanel', '', BASE_URL) . "uploads/items/large/".$row->image.'"></td>';
			$table .= '<td>' . $row->title . '</td>';
			$table .= '<td>' . $row->content . '</td>';
			$table .= '<td>' . $row->date . '</td>';
			$table .= '<td>' . $row->status . '</td>';
			$table .= '</tr>';
		}

		$pagination->table = $table;
		return $pagination;
	}
	

	private function ajax_create() {
		$image_uploader = new image_uploader();
		$filename = $image_uploader->setSize(array('large','thumb'))
		->setFolderName('../uploads/items')
		->getImage('item_image');

		$data = $this->input->post($this->fields);
		$textarea = $_POST['content'];		
		$data['content'] = addslashes($textarea);
		$data['image'] = $filename;
		$data['status'] = 'Published';
		$result = $this->news_model->saveNews($data);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
		);
	}

	private function ajax_create_draft() {
		$image_uploader = new image_uploader();
		$filename = $image_uploader->setSize(array('large','thumb'))
		->setFolderName('../uploads/items')
		->getImage('item_image');

		$data = $this->input->post($this->fields);
		$textarea = $_POST['content'];		
		$data['content'] = addslashes($textarea);
		$data['image'] = $filename;
		$data['status'] = 'Draft';
		$result = $this->news_model->saveNews($data);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
		);
	}

	// private function ajax_create_pin() {
	// 	$checkbox = $this->input->post('checkbox');

	// 	$result = $this->news_model->getNewsForPins($checkbox);

	// 	return array(
	// 		'redirect'	=> MODULE_URL,
	// 		'success'	=> $result
	// 		);
	// }

	private function ajax_edit() {
		$image_uploader = new image_uploader();
		$filename = $image_uploader->setSize(array('large','thumb'))
		->setFolderName('../uploads/items')
		->getImage('item_image');
		
		$id = $this->input->post('id');

		if(empty($filename)) {
			$data = $this->input->post($this->fields);
			$image_name = $this->input->post('image-edit');
			$data['image'] = $image_name;
			$textarea = $_POST['content'];		
			$data['content'] = addslashes($textarea);
			$data['status'] = 'Published';
		} else {
			$data = $this->input->post($this->fields);
			$data['image'] = $filename;
			$textarea = $_POST['content'];		
			$data['content'] = addslashes($textarea);
			$data['status'] = 'Published';
		}
		$result = $this->news_model->updateNews($data, $id);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
		);
	}

	private function ajax_edit_draft() {
		$image_uploader = new image_uploader();
		$filename = $image_uploader->setSize(array('large','thumb'))
		->setFolderName('../uploads/items')
		->getImage('item_image');
		
		$id = $this->input->post('id');

		if(empty($filename)) {
			$data = $this->input->post($this->fields);
			$image_name = $this->input->post('image-edit');
			$data['image'] = $image_name;
			$textarea = $_POST['content'];		
			$data['content'] = $textarea;
			$data['status'] = 'Draft';
		} else {
			$data = $this->input->post($this->fields);
			$data['image'] = $filename;
			$textarea = $_POST['content'];		
			$data['content'] = $textarea;
			$data['status'] = 'Draft';
		}
		$result = $this->news_model->updateNews($data, $id);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
		);
	}

	private function ajax_delete() {
		$delete_id = $this->input->post('delete_id');
		$error_id = array();
		if ($delete_id) {
			$error_id = $this->news_model->deleteNews($delete_id);
		}
		return array(
			'success'	=> (empty($error_id)),
			'error_id'	=> $error_id
		);
	}
	
}