<script src='https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=fscaicw4qlktvlve2pbdlj52q8bd8523oc4yo09t9jlqodig'></script>
<script>
	$(function () {
		tinymce.init({
			height : "50",
			selector: '#mytextarea',
			height: 500,
			theme: 'modern',
			fontsize_formats: "8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt",
			plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars code fullscreen',
			'insertdatetime media nonbreaking save table contextmenu directionality',
			'emoticons template paste textcolor colorpicker textpattern imagetools'
			],
			toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | numlist bullist | fontsizeselect | fontselect',
			toolbar2: 'print preview media | forecolor backcolor emoticons',
			image_advtab: true,
			setup: function (editor) {
				editor.on('change', function () {
					editor.save();
				});
			}
		});	
	});
	
</script>
<section class="content">
		<div class="box box-primary">
			<div class="box-body">
				<br>
				<form action="" method="post" class="form-horizontal">
					<div class="row">
						<div class="col-md-11">
							<div class="row">
							
								<div class="col-md-6">
									<?php
										echo $ui->formField('text')
											->setLabel('Directory')
											->setSplit('col-md-4', 'col-md-8')
											->setName('directory')
											->setId('directory')
											->setValue($directory)
											->setValidation('required')
											->draw($show_input);
									?>
								</div>

								<div class="col-md-6">
									<?php
										echo $ui->formField('dropdown')
											->setLabel('Building')
											->setSplit('col-md-4', 'col-md-8')
											->setList(array('Main Hospital Building' => 'Main Hospital Building', 'Medical Arts Building' => 'Medical Arts Building'))
											->setName('type')
											->setId('type')
											->setValue($type)
											->setValidation('required')
											->draw($show_input);
									?>
								</div>
								
								<div class="col-md-6 col-md-offset-3">
								<textarea id="mytextarea" name = "mytextarea"><?php echo $content; ?></textarea>
							</div>
							
					</div>
					<hr>
					<div class="row">
						<div class="col-md-12 text-center">
							<?php echo $ui->drawSubmit($show_input); ?>
							<a href="<?=MODULE_URL?>" class="btn btn-default" data-toggle="back_page">Cancel</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
	<?php if ($show_input): ?>
	<script>
       $('form').submit(function(e) {
        e.preventDefault();
        $(this).find('.form-group').find('input, textarea, select').trigger('blur');
        if ($(this).find('.form-group.has-error').length == 0) {
            $.post('<?=MODULE_URL?>ajax/<?=$ajax_task?>', $(this).serialize() + '<?=$ajax_post?>', function(data) {
                if (data.success) {
                    window.location = data.redirect;
                }
            });
        } else {
            $(this).find('.form-group.has-error').first().find('input, textarea, select').focus();
        }
    });
    </script>

<?php endif ?>