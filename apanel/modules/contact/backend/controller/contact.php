<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->contact_model	= new contact_model();
		$this->input			= new input();
		$this->fields			= array('id', 'datesent','name','email','message');
		$this->fields1			= array('id','building','street','city','mobile','tel','email');
	}

	public function listing() {
		$this->view->title	= 'Messages';
		$data['ui']			= $this->ui;
		$this->view->load('contact_list', $data);
	}

	public function create() {
		$this->view->title = 'Address';
		$data = $this->input->post($this->fields1);
		$data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_create';
		$data['ajax_post'] = '';
		$data['show_input'] = true;
		$this->view->load('address', $data);
	}

	public function view($id) {
		$this->view->title = 'Message View';
		$data = (array) $this->contact_model->getContactById($this->fields, $id);
		$data['ui'] = $this->ui;
		$data['show_input'] = false;
		$this->view->load('contact', $data);
	}

	public function edit() {
		$this->view->title = 'Message Edit';
		$data = (array) $this->contact_model->getAddressById($this->fields1);
		$data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_edit';
		$data['ajax_post'] = "&id=";
		$data['show_input'] = true;
		$this->view->load('address', $data);
	}

	public function ajax($ajax_function) {
		$result = $this->{$ajax_function}();
		header('Content-type: application/json');
		echo json_encode($result);
	}

	public function ajax_list() {
		$pagination = $this->contact_model->getContactList();
		$table = '';
		
		if (empty($pagination->result)) {
          $table = '<tr><td colspan="9" class="text-center"><b>No Records Found</b></td></tr>';
        }
		
		foreach($pagination->result as $row) {
			$dropdown = $this->ui->loadElement('check_task')
			->addView()
			//->addEdit()
			->addPrint()
			->addDelete()
			->addCheckbox()
			->setValue($row->id)
			->draw();
			$table .= '<tr>
							<td>' . $dropdown . '</td>
							<td>' . date('F d, Y', strtotime($row->datesent)) . '</td>
							<td>' . $row->name . '</td>
							<td>' . $row->email . '</td>
							
						</tr>';
		}

		$pagination->table = $table;

		return $pagination;
	}

	
	public function ajax_create() {
		$data = $this->input->post($this->fields1);
		$this->contact_model->saveAddress($data);
		$data['ajax_post'] = '';
	}

	private function ajax_edit() {
		$data = $this->input->post($this->fields1);
		$id = $this->input->post('id');
		$result = $this->contact_model->updateAddress($data, $id);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
		);
	}

	private function ajax_delete() {
		$delete_id = $this->input->post('delete_id');
		$error_id = array();
		if ($delete_id) {
			$error_id = $this->contact_model->deleteContact($delete_id);
		}
		return array(
			'success'	=> (empty($error_id)),
			'error_id'	=> $error_id
		);
	}
	
}