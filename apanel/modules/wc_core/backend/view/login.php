<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Login</title>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="<?= BASE_URL ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?= BASE_URL ?>assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?= BASE_URL ?>assets/css/ionicons.min.css">
		<link rel="stylesheet" href="<?= BASE_URL ?>assets/css/AdminLTE.min.css">
		<link rel="stylesheet" href="<?= BASE_URL ?>assets/css/skin.css">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<script src="<?= BASE_URL ?>assets/js/jquery-2.2.3.min.js"></script>
		<script src="<?= BASE_URL ?>assets/js/bootstrap.min.js"></script>
	</head>
	<body class="hold-transition login-page">
		<div class="login-box">
			<div class="login-logo">
				<a href=""><b>MVMC</b></a>
			</div>
			<div class="login-box-body" id="login">
				<?php if ( ! empty($error_msg)): ?>
					<p class="login-box-msg text-red"><?=$error_msg?></p>
				<?php else: ?>
					<p class="login-box-msg">Sign in to start your session</p>
				<?php endif ?>
				<?php if ( ! empty($locktime)): ?>
					<p class="login-box-msg text-red">Login Locked Till: <?=$locktime?></p>
				<?php endif ?>
				<form action="" method="post" id="loginnn">
					<div class="form-group has-feedback">
						<input type="text" name="username" class="form-control" placeholder="Username" value="<?php echo $username ?>">
						<span class="glyphicon glyphicon-user form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input type="password" name="password" class="form-control" placeholder="Password">
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
						</div>
					</div><br>
					<!-- <div class="row" style="float:right"><a id="forgot_pw" style = "cursor : pointer;">Forgot Password?</a>&nbsp;&nbsp;</div><br> -->
				</form>
			</div>

			<div class="login-box-body" id="forgot" hidden>
				<?php if ( ! empty($error_msg)): ?>
					<p class="login-box-msg text-red"><?=$error_msg?></p>
				<?php else: ?>
					<p class="login-box-msg">Enter your email</p>
				<?php endif ?>
				<?php if ( ! empty($locktime)): ?>
					<p class="login-box-msg text-red">Login Locked Till: <?=$locktime?></p>
				<?php endif ?>
					<div class="form-group has-feedback">
						<input type="text" name="email" id="email" class="form-control" placeholder="Email" value="">
						<span class="glyphicon glyphicon-user form-control-feedback"></span>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<button type="submit" id="send" class="btn btn-primary btn-block btn-flat">Send</button>
						</div><br>
					</div><br>
					<div class="row" style="float:right"><a id="back" style = "cursor : pointer;">Back to Login</a>&nbsp;&nbsp;</div><br>
			</div>
		</div>

	<div id="successmodal" class="modal fade" role="dialog">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
					<h4 class="modal-title modal-success"><span class="glyphicon glyphicon-ok"></span> Success!</h4>
					</div>
					<div class="modal-body">
					<p id = "message">New password has been sent to your email. </p>
					</div>
					<div class="modal-footer">
					<!-- <button type="button" class="btn btn-success" data-dismiss="modal">Ok</button> -->
					</div>
				</div>
			</div>
	</div>
		<script>
			if ($('[name="username"]').val().length > 0) {
				$('[name="password"]').focus();
			} else {
				$('[name="username"]').focus();
			}
			
			$('#forgot_pw').on('click', function(){
				$('#login').attr('hidden',true);
				$('#forgot').removeAttr('hidden');
			});

			$('#back').on('click', function(){
				$('#login').attr('hidden',false);
				$('#forgot').attr('hidden',true);
			});

			$('#send').on('click', function(){
				var email = $('#email').val();
				$('#send').html('Sending Request...').attr('disabled', true);
				$.post('<?=MODULE_URL?>ajax/ajax_checkemail', 'email=' + email, function(data) {
					if(data.error_msg){
						$('.login-box-msg').html(data.error_msg).css('color','red');
					}else{
						$('.login-box-msg').html('Enter your email').css('color','#666');
						$('#successmodal').modal('show');
						$('#login').attr('hidden',false);
						$('#forgot').attr('hidden',true);
					}
				});
			});

		</script>
	</body>
</html>
