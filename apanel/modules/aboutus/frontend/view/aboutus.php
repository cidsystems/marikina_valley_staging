         <!-- About Section -->
 <section id="aboutus" class="aboutus">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                         <div class="head_title text-center margin-top-60">
                 <!-- <img class="img2" src="<?php echo BASE_URL; ?>assets/home/images/home.jpg" alt="" />-->
                     <h4 class="h4">About Us</h4>
                        </div><!-- End of head title -->
                        <hr class="divider"> 
                        <ul class="nav navbar-nav navbar-left">
                                            <li <?php if(MODULE_URL==BASE_URL.'aboutus/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>aboutus" <?php if(MODULE_URL==BASE_URL.'aboutus/'){ echo 'class="active"';}?>>Company Profile</a></li>
                     <li <?php if(MODULE_URL==BASE_URL.'mission/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>mission" <?php if(MODULE_URL==BASE_URL.'mission/'){ echo 'class="active"';}?>>Mission, Vision & Values</a></li>
                      <li <?php if(MODULE_URL==BASE_URL.'mvmc/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>mvmc/" <?php if(MODULE_URL==BASE_URL.'mvmc/'){ echo 'class="active"';}?>>MVMC Leadership</a></li>
                     
</ul>
                        <div class="main_about_area"> 
                        
                            <div class="row">
                            
                                </div><br>
                          
                            
                                <h4 class="aboutus">Company Profile</h4><br>
                                <div class="row">
                        <img class="missionimg" src="<?= BASE_URL ?>assets/home/images/mvmc.jpg" align="left">
                                <b class="aboutus">History</b><br>
                                <?php  foreach ($history->result as $row) {
                                            ?>
                                            <p class="about"><?php
                                            echo $row->content;
                                        ?>
                                        </p>
                                      <?php  }
                                        ?> 
                                        </div>
                        </div>
                    </div><!-- End of col-sm-12 -->
                </div><!-- End of row -->
            </div><!-- End of Container -->
            <hr />
        </section><!-- End of about Section -->