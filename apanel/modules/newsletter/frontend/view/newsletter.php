<!-- Contact Section -->
<section id="contact" class="contactus">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                
                    <!-- <div class="head_title text-center margin-top-80">
                    <img class="img2"src="<?php echo BASE_URL; ?>assets/home/images/home.jpg">
                        <h1>Contact Us</h1>
                    </div> -->
                    <div class="main_contact">
                    <div class="row">
                        <div class="contact_contant">
                            <div class="col-sm-12 col-xs-12">
                                <div class="single_message_right_info">
                                    <form action="" method="post" id="form">
                                            <!--<div class="col-lg-8 col-md-8 col-sm-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-1">-->
                                            <div class="row">
                                            <div class="col-md-12">
                                            <h4 class="h4">Get Updated! Sign up for our Newsletter!</h4>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    Name
                                                <input type="text" class="form-control" name="name" id="name" placeholder="name" required="required">
                                                
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    E-mail
                                                    <input type="email" class="form-control" name="email" id="email" placeholder="email" required="required">
                                                </div>
                                            </div>

                                            <!-- <div class="form-group">
                                                Your message
                                                <textarea class="form-control" name="message" id="message" placeholder="message" cols="52" rows="8" required="required"></textarea>
                                                
                                            </div>&nbsp; -->
                                            <!-- <div id = "html_element" class="g-recaptcha" data-sitekey="6LetdEkUAAAAACIIcEl-CeUBtAZia65j9bYcqazY"></div>
                                            <div id = "error"></div> -->
                                            <br><br><br>
                                            <div class="row">
                                                <div class="form-group col-md-6" text-center>
                                                    <input type="submit" class="btn btn-default2" value="Submit"><br><br><br>
                                                </div>
                                            </div>
                                            
                                            <!--</div>--> 
                                        </form>
                                </div>
<br><br><br><br>
 
                </div><!-- End of col-sm-6 -->
                
<div class="modal fade mod" id="hey" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="exampleModalLabel">Thank you.</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <span>You are now subscribed to Marikina.</span>
        </div>
      <div class="modal-footer">
        <button type="button" class = "btn" id = "close" data-dismiss = "modal">Okay</button>
      </div>
    </div>
  </div>
</div>         
                           
                        </div> <!-- End of messsage contant-->
                    </div>
                </div>
            </div>
        </div><!-- End of row -->
    </div><!-- End of container -->
</section><!-- End of contact Section -->

<!-- <script src='https://www.google.com/recaptcha/api.js'></script> -->
<script>
$('form').submit(function(e) {
e.preventDefault();
    $('#hey').modal('show');
    $.post('<?=MODULE_URL?>ajax/ajax_create', $(this).serialize() + '<?=$ajax_post?>', function(data) {
    if (data.success) {
        $('#close').on('click', function() {
            // window.location = data.redirect;
             });            
    }
});

});

$('#email').on('input', function(){
    var email = $(this).val();
    $.post('<?=MODULE_URL?>ajax/check_duplicate', 'email='+email, function(data) {
        if(data === true){
			error_message 	=	"<b>Email already exists</b>";
				$('#email').closest('.form-group').addClass("has-error").find('p.help-block').html(error_message);
		}else{
				$('#email').closest('.form-group').removeClass('has-error').find('p.help-block').html('');
		}
        });   
});
</script>

 