<?php
class mail_config extends wc_model {

		public function __construct() {
		parent::__construct();
		$this->log = new log();
	}

	public function retrieveMailConfig($data) {
		$result = $this->db->setTable('mail_configuration')
							->setFields($data)
							->runSelect()
							->getRow();

		return $result;
	}

	public function updateMailConfig($data) {
		if($data['password']){
			// $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
			$data['password'] = $data['password'];
		}else{
			unset($data['password']);
		}
			$result = $this->db->setTable('mail_configuration')
							->setFields('mailserver')
							->runSelect()
							->getRow();
			if($result){
				$result1 = $this->db->setTable('mail_configuration')
							->setValues($data)
							->setWhere('companycode = "CID"')
							->setLimit(1)
							->runUpdate();
							// echo $this->db->getQuery();
				
			}else{
				$result1 = $this->db->setTable('mail_configuration')
							->setValues($data)
							->runInsert();
			}

		return $result1;
	}

	public function updatePassword($email,$reset) {
		$data['password'] = password_hash($reset, PASSWORD_BCRYPT);
		$result = $this->db->setTable(PRE_TABLE . '_users')
				->setValues($data)
				->setWhere("email = '$email'")
				->setLimit(1)
				->runUpdate();

		return $result;
	}
}