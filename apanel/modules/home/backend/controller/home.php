<?php
class controller extends wc_controller {

    public function __construct() {
		parent::__construct();
		$this->home			= new home_model();
		$this->ui			= new ui();
		
    }
    
    public function index() {
		$this->view->title	= 'Dashboard';
		$data['applicants'] = $this->home->getApplicants();
		$data['messages'] = $this->home->getMessages();
		$data['ui']			= $this->ui;
		$this->view->load('home', $data);
	}
}