<section class="content">
		<div class="box box-primary">
			<div class="box-body">
				<br>
				<form action="" method="post" class="form-horizontal" id="form">
					<div class="row">
						<div class="col-md-11">
						<div class="row">
                                                                <div class="col-md-12">
									<?php
										echo $ui->formField('text')
											->setLabel('Position')
											->setSplit('col-md-4', 'col-md-6')
											->setName('position')
											->setId('position')
											->setValue($position)
											->setValidation('required')
											->draw($show_input);
									?>
								
							</div>
								<div class="col-md-12">
									<?php
										echo $ui->formField('text')
											->setLabel('First Name')
											->setSplit('col-md-4', 'col-md-6')
											->setName('firstname')
											->setId('firstname')
											->setValue($firstname)
											->setValidation('required')
											->draw($show_input);
									?>
								
							</div>
								<div class="col-md-12">
									<?php
										echo $ui->formField('text')
											->setLabel('Last Name')
											->setSplit('col-md-4', 'col-md-6')
											->setName('lastname')
											->setId('lastname')
											->setValue($lastname)
											->setValidation('required')
											->draw($show_input);
									?>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<?php
										echo $ui->formField('textarea')
											->setLabel('Address')
											->setSplit('col-md-4', 'col-md-6')
											->setName('address')
											->setId('address')
											->setValue($address)
											->setValidation('required')
											->draw($show_input);
									?>
								</div>
							</div>
                                                        <div class="row">
								<div class="col-md-12">
									<?php
										echo $ui->formField('textarea')
											->setLabel('Contact')
											->setSplit('col-md-4', 'col-md-6')
											->setName('contact')
											->setId('contact')
											->setValue($contact)
											->setValidation('required')
											->draw($show_input);
									?>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<?php
										echo $ui->formField('text')
											->setLabel('Email')
											->setSplit('col-md-4', 'col-md-6')
											->setName('email')
											->setId('email')
											->setValue($email)
											->setValidation('required')
											->draw($show_input);
									?>
                                </div>
                                <div class="col-md-12">
									<?php
										echo $ui->formField('textarea')
											->setLabel('Message')
											->setSplit('col-md-4', 'col-md-6')
											->setName('message')
											->setId('message')
											->setValue($message)
											->setValidation('required')
											->draw($show_input);
									?>
                                </div>
                                <div class="col-md-12">
									<?php
										echo $ui->formField('text')
											->setLabel('Resume')
											->setSplit('col-md-4', 'col-md-6')
											->setName('resume')
											->setId('resume')
											->setValue('<a href="'.str_replace('/apanel', '', BASE_URL) . "uploads/".$resume.'">'.$resume.'</a>')
											->setValidation('required')
											->draw($show_input);
									?>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-12 text-center">
							<?php //echo $ui->drawSubmit($show_input); ?>
							<a href="<?=MODULE_URL?>" class="btn btn-default" data-toggle="back_page">Back</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
    <script>
    $('form').submit(function(e) {
        e.preventDefault();
        $.post('<?=BASE_URL?>careers/ajax/<?=$ajax_task?>', $(this).serialize() + '<?=$ajax_post?>', function(data) {
            if (data.success) {
				window.location = data.redirect;
			}
        });
    });
   
    

    </script>
