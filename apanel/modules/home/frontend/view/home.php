 <!-- Home Section -->
 <style>
 .rate {
    position: fixed;
    z-index: 1000;
    background-color: #00BFA5;
    border-radius: 10px;
    margin: 5px 5px 5px 5px;
    width: 10%;
    right: 0px;
}
a{
    color:white;
}
a:hover{
    color:white;
}
.btn-morenewshere {
     margin-top: 45px;
 }
 @media only screen and (max-width: 1200px) {
    .btn-morenewshere {
         margin-top: 52px;
     } 
 }
 @media only screen and (max-width: 991px) {
    .btn-morenewshere {
         margin-top: 40px;
     } 
 }
 </style>
 <section class="carousel">
            <?php if(!in_array($_SERVER['REMOTE_ADDR'],$ip)): ?>
                <div class="col-md-3 text-left rate" style="display:none;>
                    <div class='starrr text-center' id='star1'>
                        <label for="asd" style="color:white;margin: 0px 0px 0px -10px;">Rate us:</label><br>
                    </div>
                    <div class="col-md-12 text-left">
                        <span class='your-choice-was' style='display: none;color:white'>
                                <span class='choice' style="font-size:small"></span>
                        </span>
                    </div>
                </div>        
            <?php endif ?> 
            <div class="container">
                <div class="row">
                    <div class="main_team_area"> 
                        <div class="main_team text-center">
                        <?php foreach($banner as $row) { ?>

                        <?php
                            if (strpos($row->title, 'http://') !== false || strpos($row->title, 'https://') !== false) {
                                $redirect = $row->title;
                            }
                            else {
                                $redirect = '#';
                            }
                        ?>
                          
                                        <a href="<?= $redirect ?>"><div class="item"><img class="bannerimg" src="<?php str_replace('/apanel', '', BASE_URL)?>uploads/items/large/<?php echo $row->image ?>">
                                        <div class="caption text-center"></div></div></a>
            
                         
                            <?php } ?>
                            
                        
                        </div> 
                    </div>
                </div><!-- End of row -->
            </div><!-- End of Container -->
        </section><!-- End of counter Section -->






        <!-- Service Section -->
        <section id="home" class="home">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                     <div class="main_service_area"> 
                            <div class="single_service_area">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="signle_service_left">
                                        <div class="panel1">  
                                        <form method="POST" action="search">
                                        
                                            <div class="container-fluid">
                                            
                                         
                                           <br>
                                            <b class="b">WHAT IS YOUR CONCERN?</b>
                                            <select id="options" class="dd">
                                                <option style="display:none">
                                                <option value="doctors">Find a Doctor</option>
                                                <option value="service">Find a Service</option>
                                          </select>&nbsp; <br>
                                            <!-- <input autocomplete="off" class="input1" type="text" id="search" name="search" placeholder=" Enter Doctor's Name or Department"><br><br> -->
                        
                        
                                            <!-- <div class="ui-widget">
                                                <label for="tags">Tags: </label>
                                                 <input id="tags">
                                            </div> -->

                                            <div class = "panel panel-default">
                                                <div class = "panel-body">
                                                    <ul id = "suggestion-box">

                                                    </ul>
                                                </div>
                                            </div>
                               
                                            </div></div><br> 
                                            </div>    
                                        </form>

                                            <div class="panel2">
                                            <div class="h3">LATEST <b>NEWS</b></div>
                                            <?php  foreach ($date as $row) { ?>
                                                <div class="row">
                                                <div class="col-sm-12 col-sm-push-1">    
                                                <img class="img" src="<?= BASE_URL ?>uploads/items/large/<?php echo $row->image ?>" align="left">
                                             <?php
                                            echo '<p class="homenews">'.substr($row->title, 0, 75); ?><a href="<?= BASE_URL ?>news/view/<?= $row->id ?>"><button class="btn btn-defaultnews">READ MORE</button></a></p>
                                           <!--  <a href="<?= BASE_URL ?>news/view/<?= $row->id ?>"><button class="btn btn-default">READ MORE</button></a></p> -->
                                            </div>
                                            </div>

                                            <?php } ?>
                                            <a href="<?= BASE_URL ?>news/"><button class="btn btn-defaultmore btn-morenewshere">MORE NEWS HERE</button></a>
                                            
                                            
                                            </div>
                              
                                    </div>

                                    <div class="col-sm-6 col-sm-push-1">
                                        <div class="single_service">
                                        <div class="panel3">
                                            <h3>MEDICAL SERVICES</h3>
                                            <?php foreach ($service as $row) { ?>
                                            <div class="row">
                                            <a class="home" href="<?= BASE_URL ?>service/view/<?= $row->id ?>">
                                            <h5><?php echo $row->service; ?></h5><img class="img1" src="<?= BASE_URL ?>uploads/items/large/<?php echo $row->image ?>" align="right">
                                           <!--  <div class="separator2"></div> -->
                                            <p><?php echo substr($row->content, 0, 153).'...'; ?></a></p><br> 
                                            </div>                                         
                                            
                                            <?php } ?>
                                            <a href="<?= BASE_URL ?>service/"><button class="btn btn-defaultmore">MORE MEDICAL SERVICES</button></a>
                                           <br><br><br>
                                        </div>
                                    </div>
                                </div>
                               
                                <div>
                                               
                                </div>
                            </div>
                            </section><br><br><!-- End of single service area -->
                                
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.js"></script>
<script src="<?= BASE_URL ?>assets/home/dist/starrr.js"></script>
<script>
    $('#star1').starrr({
      change: function(e, value){
        if (value) {
          $('.your-choice-was').show();
          if(value == 1){
              msg = 'You have given a poor rating, 1 star. Thank you.';
          }else if(value == 2){
            msg = 'You have given a fair rating, 2 stars. Thank you.';
          }else if(value == 3){
            msg = 'You have given a good rating, 3 stars. Thank you.';
          }else if(value == 4){
            msg = 'You have given a very good rating, 4 stars. Thank you.';
          }else if(value == 5){
            msg = 'You have given the highest rating, 5 stars. Thank you.';
          }
          $('.choice').text(msg);
          $.post('<?=MODULE_URL?>ajax/ajax_rate', 'rate='+ value + '&ip_address='+'<?php echo $_SERVER['REMOTE_ADDR'];?>', function(data) {
                $( ".rate" ).delay(1000).fadeOut("slow");
          });
          
        } else {
          $('.your-choice-was').hide();
        }
      }
    });
  </script>
<script>
$('#options').on('change',function () {
    if($('#options').val() == 'service') {
        $('#searchService').modal('show');
    } else {
        window.location = '<?=BASE_URL?>doctors';
    }
    });

ajax_call = '';
$('.panel').hide();
  $('#search').on('input', function () {
    $('.panel').show();
    if (ajax_call != '') {
      ajax_call.abort();
    }
    var search = $(this).val();
    ajax_call = $.post('<?=MODULE_URL?>ajax/ajax_search', 'search=' + search, 
      function(data) {
          $('#suggestion-box').empty();

          if($('#search').val()) {
            data.search.forEach(function (value, key) {
            $('#suggestion-box').append("<li><a style = 'cursor: pointer;' onclick = \"selectSearch('" + value.firstname + " " + value.lastname + "')\" > " + value.firstname + " " + value.lastname + "</li></a>");
          });
        } else {
            $('.panel').hide();
        }
      });
  });
$('#servicesearch').on('submit', function (e) {
    e.preventDefault(); 
   var search = $('#searchservice').val();
   window.location = '<?= MODULE_URL ?>home/ajax_search/'+search;

  });
  function selectSearch(val) {
    $('#search').val(val);
    $('.panel').hide();
  }
 </script>  
