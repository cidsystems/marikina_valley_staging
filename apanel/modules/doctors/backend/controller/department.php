<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->doctors_model	= new doctors_model();
		$this->session			= new session();
		$this->fields 			= array(
            'id',
            'name'
		);
	}

	public function listing() {
		$this->view->title = 'Department List';
		$data['ui'] = $this->ui;
		// $all = (object) array('ind' => 'null', 'val' => 'Filter: All');
		// $data = $this->doctors_model->getGroupList();
		$this->view->load('department_list', $data);
	}

	public function create() {
		$this->view->title = 'Add Department';
		$data = $this->input->post($this->fields);
		$data['ui'] = $this->ui;
		// $data['group_list'] = $this->doctors_model->getGroupList();
		$data['ajax_task'] = 'ajax_create';
		$data['ajax_post'] = '';
		$data['show_input'] = true;
		$this->view->load('department', $data);
	}

	public function edit($id) {
		$this->view->title = 'Edit Department';
		$data = (array) $this->doctors_model->getDeptById($this->fields, $id);
		$data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_edit';
		$data['ajax_post'] = "&id=$id";
		$data['show_input'] = true;
		$this->view->load('department', $data);
	}

	public function view($id) {
		$this->view->title = 'View Department';
		$data = (array) $this->doctors_model->getDeptById($this->fields, $id);
		$data['ui'] = $this->ui;
		$data['show_input'] = false;
		$this->view->load('department', $data);
	}
	
	public function ajax($task) {
		$ajax = $this->{$task}();
		if ($ajax) {
			header('Content-type: application/json');
			echo json_encode($ajax);
		}
	}

	public function ajax_list() {
		$pagination = $this->doctors_model->getDeptList();
		$table = '';
		
		foreach($pagination->result as $row) {
			$dropdown = $this->ui->loadElement('check_task')
			->addView()
			->addEdit()
			->addPrint()
			->addDelete()
			->addCheckbox()
			->setValue($row->id)
            ->draw();
            $table .= '<tr>';
			$table .= '<td align = "center">' . $dropdown . '</td>';
			$table .= '<td>' . $row->name . '</td>';
			$table .= '</tr>';
		}
        
                $pagination->table = $table;
        
                return $pagination;
            }

	private function ajax_create() {
		$data = $this->input->post($this->fields);
		$result = $this->doctors_model->saveDept($data);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
		);
	}

	private function ajax_edit() {
		$data = $this->input->post($this->fields);
		$id = $this->input->post('id');
		$result = $this->doctors_model->updateDept($data, $id);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
		);
	}

	private function ajax_delete() {
		$delete_id = $this->input->post('delete_id');
		$error_id = array();
		if ($delete_id) {
			$error_id = $this->doctors_model->deleteDept($delete_id);
		}
		return array(
			'success'	=> (empty($error_id)),
			'error_id'	=> $error_id
		);
	}


}