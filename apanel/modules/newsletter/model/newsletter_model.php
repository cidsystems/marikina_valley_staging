<?php
class newsletter_model extends wc_model {

public function getNewsletterList($sort){
    if($sort){
        $sort = $sort;
    }else{
        $sort = 'entereddate DESC';
    }
    $result = $this->db->setTable('newsletter')
                        ->setFields('id,subject,content,status,entereddate')
                        ->setOrderBy($sort)
                        ->runPagination();
    return $result;
}

public function getNewsletterById($fields, $id) {
    return $this->db->setTable('newsletter')
                    ->setFields($fields)
                    ->setWhere("id = '$id'")
                    ->setLimit(1)
                    ->runSelect()
                    ->getRow();
}

public function saveInfo($fields) {
    $result = $this->db->setTable('subscribers')
    ->setValues($fields)
    ->runInsert();

    return $result;

}

public function saveNewsletter($fields) {
    $result = $this->db->setTable('newsletter')
    ->setValues($fields)
    ->runInsert();
    
    return $result;

}

public function updateNewsletter($fields,$id) {
    $result = $this->db->setTable('newsletter')
    ->setValues($fields)
    ->setWhere("id = '$id'")
    ->runUpdate();
    
    return $result;

}

public function deleteNewsletter($data) {
    $error_id = array();
    foreach ($data as $id) {
        $result =  $this->db->setTable('newsletter')
                            ->setWhere("id = '$id'")
                            ->runDelete();
    }

    return $error_id;
}

public function check_duplicate($email) {
    $result = $this->db->setTable('subscribers')
    ->setFields('email')
    ->setWhere("email = '$email' AND status = 'subscribed'")
    ->runSelect()
    ->getResult();
    
    return $result;

}

public function getEmails() {
    $result = $this->db->setTable('subscribers')
    ->setFields("email")
    ->setWhere("status = 'subscribed'")
    ->runSelect()
    ->getResult();

    $asd = array();
    foreach($result as $row){
        $asd[] = $row->email;
    }

    return $asd;

}



} 