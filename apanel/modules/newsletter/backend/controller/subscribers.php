<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->subscribers_model= new subscribers_model();
		$this->input			= new input();
		$this->fields			= array('id','name','email','status');
	}

	public function listing() {
		$this->view->title	= 'Subscribers List';
		$data['ui']			= $this->ui;
		$data['show_input'] = true;
		$data['ajax_task']  = '';
		$this->view->load('subscribers_list', $data);
	}

	public function ajax($ajax_function) {
		$result = $this->{$ajax_function}();
		header('Content-type: application/json');
		echo json_encode($result);
	}	

	public function ajax_list() {
		$pagination = $this->subscribers_model->getSubscribersList();
		$table = '';
		
		if (empty($pagination->result)) {
			$table = '<tr><td colspan="5" class="text-center"><b>No Records Found</b></td></tr>';
		}
		foreach($pagination->result as $row) {
			$date = $this->date->dateFormat($row->entereddate);
            $dropdown = $this->ui->loadElement('check_task')
			->addCheckbox()
            ->setValue($row->id)
			->draw();
			$table .= '<tr>
							<td>' . $dropdown . '</td>
							<td>' . $row->name . '</td>
							<td>' . $row->email . '</td>
							<td>' . $row->status . '</td>
							<td>' . $date . '</td>							
						</tr>';
		}

		$pagination->table = $table;

		return $pagination;
	}

	

	private function ajax_delete() {
		$delete_id = $this->input->post('delete_id');
		$error_id = array();
		if ($delete_id) {
			$error_id = $this->subscribers_model->deleteSubscriber($delete_id);
		}
		return array(
			'success'	=> (empty($error_id)),
			'error_id'	=> $error_id
			);
	}
	
} 