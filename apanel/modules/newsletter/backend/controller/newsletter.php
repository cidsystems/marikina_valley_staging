<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->newsletter_model	= new newsletter_model();
		$this->input			= new input();
		$this->fields			= array('id','subject','content','status');
	}

	public function listing() {
		$this->view->title	= 'Newsletter List';
		$data['ui']			= $this->ui;
		$data['show_input'] = true;
		$data['ajax_task']  = '';
		$this->view->load('newsletter_list', $data);
	}

	public function create() {
		$this->view->title = 'Newsletter Create';
		$data = $this->input->post($this->fields);
		$data['stat'] = '';
		$data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_create';
		$data['ajax_post'] = '';
		$data['show_input'] = true;
		$this->view->load('newsletter', $data);
	}

	public function view($id) {
		$this->view->title = 'Newsletter View';
		$data = (array) $this->newsletter_model->getNewsletterById($this->fields, $id);
		$data['stat'] = $data['status'];
		$data['ui'] = $this->ui;
		$data['show_input'] = false;
		$this->view->load('newsletter', $data);
	}

	public function edit($id) {
		$this->view->title = 'Newsletter Edit';
		$data = (array) $this->newsletter_model->getNewsletterById($this->fields, $id);
		$data['stat'] = $data['status'];
		$data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_create';
		$data['ajax_post'] = "&id=$id";
		$data['show_input'] = true;
		$this->view->load('newsletter', $data);
	}


	public function ajax($ajax_function) {
		$result = $this->{$ajax_function}();
		header('Content-type: application/json');
		echo json_encode($result);
	}	

	public function ajax_list() {
		$sort		= $this->input->post('sort');
		$pagination = $this->newsletter_model->getNewsletterList($sort);
		$table = '';
		
		if (empty($pagination->result)) {
			$table = '<tr><td colspan="5" class="text-center"><b>No Records Found</b></td></tr>';
		}
		foreach($pagination->result as $row) {
			$date = $this->date->dateFormat($row->entereddate);
			$dropdown = $this->ui->loadElement('check_task')
			->addView()
			->addEdit()
			->addPrint()
			->addDelete()
			->addCheckbox()
			->setValue($row->id)
			->draw();
			$table .= '<tr>';
							$table .= '<td>' . $dropdown . '</td>';
							$table .= '<td>' . $row->subject . '</td>';
							// $table .= '<td>' . strtok($row->content, "\n") . '</td>';
							$table .= ($row->status == 'send') ? '<td>sent</td>' : '<td>draft</td>';
							$table .= '<td>' . $date . '</td>';						
							$table .= '</tr>';
		}

		$pagination->table = $table;

		return $pagination;
	}

	private function ajax_create() {
		$data = $this->input->post($this->fields);
		$textarea = addslashes($_POST['content']);	
		$data['content'] = $textarea;
		$data['status'] = $this->input->post('status');
		$email = $this->newsletter_model->getEmails();

		


		if($data['status'] == 'send'){
			foreach($email as $row){
				$html  = 
		"<html>
			<head>
				<style>
					.body, table { font-size: 12px; }
					h4 { margin-bottom: 5px; }
					.body { background-color: #eee; font-family: Arial; }
					body { margin: 0; }
					html { height: 100%; }
					body, .body { min-height: 100%; }
					.body { padding: 15px 0; }
					.main { max-width: 600px; width: 100%; margin: 0 auto; box-sizing: border-box; }
					.container { background-color: #fff; padding: 15px; box-shadow: 0px 1px 2px 0px rgba(0,0,0,.1); }
					.logo { font-size: 16px; font-weight: 700; margin: 0 auto; display: block; }
					.header-caption { background-color: #19a9e5; margin: 15px 0; padding: 2px; color: #fff; font-family: Verdana; font-size: 14px; font-weight: 500; text-align: center; }
					.head, .foot-note, .head a, .foot-note a { color: #888; }
					.socicon img { vertical-align: middle; margin-right: 5px; }
					.socicon { margin-right: 15px; }
					img {max-width:100%;max-height:100%;}
					
				</style>
			</head>
			<body>
				<div class=\"body\">
					<div class=\"main\">
					
						<div class=\"head\">
							<p></p>
						</div>
					
						<div class=\"container\">
							<img src=\"https://ourwebprojects.com/marikinavalley/assets/home/images/LATESTMV.png\" width=\"25%\" class=\"logo\" alt=\"Marikina Valley Medical Center\">
							<div class=\"header-caption\"></div>
							<div class=\"content\"><p>". html_entity_decode(stripslashes($data['content'])) ."</p></div>
						
						<div class=\"foot-note\">
							<p>If you have some questions or you need some help. contact <a href=\"mailto:info@mvmc.com.ph\">info@mvmc.com.ph</a></p>
							<p>To keep updated on what's happening.</p>
							<a href=\"https://facebook.com/MarikinaValleyMedicalCenter/\" class=\"fa fa-facebook\">&nbsp;Like us on Facebook</a>
							<a href=\"https://instagram.com/marikinavalleymedicalcenter/\" class=\"fa fa-instagram\">&nbsp;Follow us on Instagram</a><br>
							<a href=\"https://ourwebprojects.com/marikinavalley/unsubscribe?email=$row\">&nbsp;Click here to unsubscribe</a>
							
							
						</div>
					</div>
				</div>
			</div>
			</body>
			</html>";
				$mail = new PHPMailer();
				$mail->IsSendmail();
				try { 
					$mail->Sender = 'admin@ourwebprojects.com';    
					$mail->SetFrom('info@mvmc.com.ph');
					$mail->AddReplyTo('info@mvmc.com.ph');
					$mail->AddAddress("$row");
					$mail->AddBCC('marion.rosales@cid-systems.com');
					$mail->Subject = $data['subject'];      
					$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
					$mail->MsgHTML($html);
					$mail->Send();      
				} catch (phpmailerException $e) {
					$e->errorMessage();
					$json['email_sent'] = "Message Not Sent!";
				}
			}
		}
		
		$result = $this->newsletter_model->saveNewsletter($data);

		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
		);
	
	}

	private function ajax_edit() {
		$data = $this->input->post($this->fields);
		$id = $this->input->post('id');
		$textarea = addslashes($_POST['content']);	
		$data['content'] = $textarea;
		$data['status'] = $this->input->post('status');
		$email = $this->newsletter_model->getEmails();

		$html  = 
		"<html>
			<head>
				<style>
					.body, table { font-size: 12px; }
					h4 { margin-bottom: 5px; }
					.body { background-color: #eee; font-family: Arial; }
					body { margin: 0; }
					html { height: 100%; }
					body, .body { min-height: 100%; }
					.body { padding: 15px 0; }
					.main { max-width: 600px; width: 100%; margin: 0 auto; box-sizing: border-box; }
					.container { background-color: #fff; padding: 15px; box-shadow: 0px 1px 2px 0px rgba(0,0,0,.1); }
					.logo { font-size: 16px; font-weight: 700; margin: 0 auto; display: block; }
					.header-caption { background-color: #19a9e5; margin: 15px 0; padding: 2px; color: #fff; font-family: Verdana; font-size: 14px; font-weight: 500; text-align: center; }
					.head, .foot-note, .head a, .foot-note a { color: #888; }
					.socicon img { vertical-align: middle; margin-right: 5px; }
					.socicon { margin-right: 15px; }
					img {max-width:100%;max-height:100%;}

					
				</style>
			</head>
			<body>
				<div class=\"body\">
					<div class=\"main\">
					
						<div class=\"head\">
							<p></p>
						</div>
					
						<div class=\"container\">
							<img src=\"https://ourwebprojects.com/marikinavalley/assets/home/images/LATESTMV.png\" width=\"25%\" class=\"logo\" alt=\"Marikina Valley Medical Center\">
							<div class=\"header-caption\"></div>
							<div class=\"content\"><p>". html_entity_decode(stripslashes($data['content'])) ."</p></div>
						
						<div class=\"foot-note\">
							<p>If you have some questions or you need some help. contact <a href=\"mailto:info@mvmc.com.ph\">info@mvmc.com.ph</a></p>
							<p>To keep updated on what's happening.</p>
							<a href=\"https://facebook.com/MarikinaValleyMedicalCenter/\" class=\"fa fa-facebook\">&nbsp;Like us on Facebook</a>
							<a href=\"https://instagram.com/marikinavalleymedicalcenter/\" class=\"fa fa-instagram\">&nbsp;Follow us on Instagram</a><br>
							<a href=\"https://ourwebprojects.com/marikinavalley/unsubscribe?email=$row\">&nbsp;Click here to unsubscribe</a>
							
							
						</div>
					</div>
				</div>
			</div>
			</body>
			</html>";


		if($data['status'] == 'send'){
			foreach($email as $row){
				$mail = new PHPMailer();
				$mail->IsSendmail();
				try { 
					$mail->Sender = 'admin@ourwebprojects.com';    
					$mail->SetFrom('info@mvmc.com.ph');
					$mail->AddReplyTo('info@mvmc.com.ph');
					$mail->AddAddress("$row");
					$mail->AddBCC('marion.rosales@cid-systems.com');
					$mail->Subject = $data['subject'];      
					$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
					$mail->MsgHTML($html);
					$mail->Send();      
				} catch (phpmailerException $e) {
					$e->errorMessage();
					$json['email_sent'] = "Message Not Sent!";
				}
			}
		}
		
		$result = $this->newsletter_model->updateNewsletter($data,$id);

		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
		);
	
	}

	private function ajax_delete() {
		$delete_id = $this->input->post('delete_id');
		$error_id = array();
		if ($delete_id) {
			$error_id = $this->newsletter_model->deleteNewsletter($delete_id);
		}
		return array(
			'success'	=> (empty($error_id)),
			'error_id'	=> $error_id
			);
	}

	private function ajax_create_upload() {
		$accepted_origins = array("http://localhost", "https://ourwebprojects.com", "https://marikinavalleymedicalcenter.com");
	
		$imageFolder = "../uploads/tinymce_uploads/";
	
		reset ($_FILES);
		$temp = current($_FILES);
		if (is_uploaded_file($temp['tmp_name'])){
		  if (isset($_SERVER['HTTP_ORIGIN'])) {
			if (in_array($_SERVER['HTTP_ORIGIN'], $accepted_origins)) {
			  header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
			} else {
			  header("HTTP/1.1 403 Origin Denied");
			  return;
			}
		  }
		  if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
			header("HTTP/1.1 400 Invalid file name.");
			return;
		  }
	
		  if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("gif", "jpg", "png"))) {
			header("HTTP/1.1 400 Invalid extension.");
			return;
		  }
		  $type = explode('/', $temp['type']);
		  $random = rand();
		  $id = uniqid();
		  $filename = $random . '-' . $id . '.' .$type[1];
		  $filetowrite = $imageFolder . $filename;
		  move_uploaded_file($temp['tmp_name'], $filetowrite);
		  $img = str_replace('/apanel', '', BASE_URL) . "uploads/tinymce_uploads/" .$filename;
		  return $img;
		} else {
		  header("HTTP/1.1 500 Server Error");
		}
	  }
	
} 