<?php
class subscribers_model extends wc_model {

public function getSubscribersList(){
    $result = $this->db->setTable('subscribers')
                        ->setFields('id,name,email,status,entereddate')
                        ->runPagination();
    return $result;
}

public function deleteSubscriber($data) {
    $error_id = array();
    foreach ($data as $id) {
        $result =  $this->db->setTable('subscribers')
                            ->setWhere("id = '$id'")
                            ->runDelete();
    }

    return $error_id;
}

} 