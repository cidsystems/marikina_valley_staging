<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->contact_model	= new contact_model();
		$this->input			= new input();
		$this->fields			= array('datesent','name','email','message');
	}


	public function listing() {
		$data = $this->input->post($this->fields);
		//$this->contact_model->saveMessage($data);
		$data['ui'] = $this->ui;
		$data['content'] = $this->contact_model->getAddressList();
		$data['ajax_task'] = 'ajax_create';
		$data['ajax_post'] = '';
		$data['show_input'] = true;
		$this->view->load('contact', $data);
	}


	public function ajax($ajax_function) {
		header('Content-type: application/json');
		$result = $this->{$ajax_function}();
		echo json_encode($result);
	}

	public function ajax_create() {
		$data = $this->input->post($this->fields);
		
        		$name = $this->input->post('name');
        		$email = $this->input->post('email');
        		$message = $this->input->post('message');
				$html = "
				<b style='font-family:Sans-serif;'>From: </b>Marikina Valley Medical Center<br>
				<b style='font-family:Sans-serif;'>To: </b>".$name."<br>
				<b style='font-family:Sans-serif;'>Subject: </b>Marikina Valley Medical Center<br>
				<p style='font-family:Sans-serif;'>Marikina Valley Medical Center</p>
				<p style='font-family:Sans-serif;'><b>Hello ".$name.",</b></p>
				<p style='font-family:Sans-serif;'>Thank you for your inquiry.</p>
				<p style='font-family:Sans-serif;'>We will reply to your inquiry as soon as possible. We do make every effort to provide an informative, detailed response.</p>
				
				<p style='color:gray;font-size: 86%;font-family:Open Sans;'>If you have some questions or you need some help. contact <a href='#' style='color:gray'>info@mvmc.com.ph</a></p>
				<p style='color:gray;font-size: 86%;font-family:Open Sans;'>To keep updated on what's happening.</p>
				<p style='color:gray;font-size: 86%;font-family:Open Sans;'><u><a href='https://www.facebook.com/MarikinaValleyMedicalCenter/' style='color:gray'>Like us on Facebook</a></u></p>
				";


                
				$mail = new PHPMailer();
				$mail->IsSendmail();
				try { 
					$mail->Sender = 'admin@marikinavalleymedicalcenter.com';    
					$mail->SetFrom('info@mvmc.com.ph');
					$mail->AddReplyTo('info@mvmc.com.ph');
					$mail->AddAddress($email);
                    $mail->AddBCC('marion.rosales@cid-systems.com');
					$mail->Subject = "Contact Us";      
					$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
					$mail->MsgHTML($html);
					$mail->Send();      
				} catch (phpmailerException $e) {
					$e->errorMessage();
					$json['email_sent'] = "Message Not Sent!";
				}

				$message = str_replace('â€œ','"',str_replace('â€œâ€Ãˆ','"',str_replace('Ã±','ñ',$message)));
				
				$html1 = "
				<h2 style='font-family:Sans-serif;'>Marikina Valley Medical Center</h2>
				<b style='font-family:Sans-serif;'>Subject: </b><br><br>Inquiry<br><br>
				<b style='font-family:Sans-serif;'>Name: </b><br><br>".$name."<br><br>
				<b style='font-family:Sans-serif;'>Email: </b><br><br><a href='#'>".$email."</a><br><br>
				<b style='font-family:Sans-serif;'>Message: </b><br><br>".htmlentities(stripslashes($message))."<br><br>
				";

                
				$mail = new PHPMailer();
				$mail->IsSendmail();
				try {  
					$mail->Sender = 'admin@marikinavalleymedicalcenter.com'; 
					$mail->SetFrom('info@mvmc.com.ph','Inquiry');
					$mail->AddReplyTo($email);
					$mail->AddAddress('info@mvmc.com.ph');
					//$mail->AddAddress('blescover@mvmc.com.ph');
					$mail->AddAddress('mdtnarciso@mvmc.com.ph');					
					// $mail->AddAddress('marion.rosales@cid-systems.com');
					$mail->AddBCC('marion.rosales@cid-systems.com');
                    $mail->AddBCC('roberto.felicio@cid-systems.com');
					$mail->Subject = "Contact Us";      
					$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
					$mail->MsgHTML($html1);
					$mail->Send();      
				} catch (phpmailerException $e) {
					$e->errorMessage();
					$json['email_sent'] = "Message Not Sent!";
				}


				$result = $this->contact_model->saveMessage($data);
				return array(
					'redirect'	=> MODULE_URL,
					'success'	=> $result
				);


		
		
	}

}
