<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Marikina Valley Medical Center</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="<?php echo BASE_URL ?>assets/home/images/LATESTMV.png">

        <!--Google fonts links-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
 
 
        <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/home/css/fonticons.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/home/css/slider-pro.css">
        <!--<link rel="stylesheet" href="<?php echo BASE_URL ?>assets/css/stylesheet.css">-->
        <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/home/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/home/css/bootstrap.min.css">


        <!--For Plugins external css-->
        <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/home/css/plugins.css" />
        <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/home/css/floating-button.css" />

        <!--Theme custom css -->
        <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/home/css/style.css">

        <!--Theme Responsive css-->
             <script src="<?= BASE_URL ?>assets/js/jquery-2.2.3.min.js"></script>
        <script src="<?= BASE_URL ?>assets/js/bootstrap.min.js"></script>
        <script src="<?= BASE_URL ?>assets/js/global.js"></script>
        <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/home/css/responsive.css" />
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
        <script src="<?php echo BASE_URL ?>assets/home/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    
        <!-- Facebook Pixel Code -->
<!--<script>-->
<!--  !function(f,b,e,v,n,t,s)-->
<!--  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?-->
<!--  n.callMethod.apply(n,arguments):n.queue.push(arguments)};-->
<!--  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';-->
<!--  n.queue=[];t=b.createElement(e);t.async=!0;-->
<!--  t.src=v;s=b.getElementsByTagName(e)[0];-->
<!--  s.parentNode.insertBefore(t,s)}(window, document,'script',-->
<!--  'https://connect.facebook.net/en_US/fbevents.js');-->
<!--  fbq('init', '654958158230404');-->
<!--  fbq('track', 'PageView');-->
<!--</script>-->
<!--<noscript><img height="1" width="1" style="display:none"-->
<!--  src="https://www.facebook.com/tr?id=654958158230404&ev=PageView&noscript=1"-->
<!--/></noscript>-->
<!-- End Facebook Pixel Code -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init','197263424324792'); 
fbq('track','PageView');
</script>
<noscript>
<img height="1" width="1"
src="https://www.facebook.com/tr?id=197263424324792&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
    </head>
    <style>
      .floating-button {
      background: url("<?= BASE_URL ?>assets/images/floating-button.png") no-repeat center 43%;
    }
    </style>
    <body data-spy="scroll" data-target=".navbar-collapse">
      <div class="floating-buttons">
        <ul>
          <li><a href="https://mvmcvirtualcare.com" target="_blank" class=" floating-icons floating-button">Virtual Care</a></li>
        </ul>
      </div>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div><div></div></div>
        <header id="main_menu">            
            <div class="main_menu_bg">
                <div class="container">
                    <div class="row">
                    
                       <div class="nave_menu">
                               <div class="call" align="right">
                               <!-- <div class="cc"> -->
                                   Call us now 
                                    <b class="call1">(+63) 917  810 0639</b><br>
                                    <b class="call1">(+63) 998 593 7169</b><br>
                                    <b class="call1">(+63-2) 682 2222</b>
                                </div>
<form method="POST" id="servicesearch">
<div class="modal fade" id="searchService" tabindex="-1" role="dialog" aria-labelledby="searchServiceLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="searchServiceLabel">Search Service</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <!-- <label>Search: </label> -->
        <input type="text" class="inputs" name="searchservice" id = "searchservice" placeholder="Search..."><br>
      </div>
      <div class="modal-footer">
        <input type="submit" value = "Submit" id="submitsearch" class="btn-primary">
      </div>
    </div>
  </div>
</div>
</form>                                
<!-- Modal -->
<form method="POST" action="<?= BASE_URL ?>searchbar">
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="exampleModalLabel">Search</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <!-- <label>Search: </label> -->
        <input type="text" class="inputs" name="searchbar" id = "searchbar" placeholder="Search..."><br>
      </div>
      <div class="modal-footer">
        <input type="submit" value = "Submit" class="btn-primary">
      </div>
    </div>
  </div>
</div>
</form>
<!-- <script>
function myFunction() {
    document.getElementById("demo").innerHTML = '<input name="searchbar" type="search">';
}
</script></div> -->
                            <nav class="navbar navbar-default" id="navmenu">
                                <div class="container-fluid">
                                    <!-- Brand and toggle get grouped for better mobile display -->

                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                        <div class="image">
                                        <a href="<?= BASE_URL ?>">
                                            <img class="logo" src="<?php echo BASE_URL ?>assets/home/images/LATESTMV.png"/>
                                        </a>
                                    </div>
                                    </div>

                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                    <div class="row">
                                    <div class="navcolor">
                                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                        <ul class="nav navbar-nav navbar-right">
                                            <li <?php if(MODULE_URL==BASE_URL){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>">HOME</a></li>
                                             <li <?php if(MODULE_URL==BASE_URL.'aboutus/' || MODULE_URL==BASE_URL.'mission/' || MODULE_URL==BASE_URL.'mvmc/' || MODULE_URL==BASE_URL.'board/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>aboutus">ABOUT US</a></li>
                                              <li <?php if(MODULE_URL==BASE_URL.'service/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>service">MEDICAL SERVICES</a></li>
                                              <li <?php if(MODULE_URL==BASE_URL.'doctors/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>doctors">OUR DOCTORS</a></li>
                                              <li <?php if(MODULE_URL==BASE_URL.'patients/' || MODULE_URL==BASE_URL.'admission/' || MODULE_URL==BASE_URL.'references/' || MODULE_URL==BASE_URL.'facilities/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>patients">PATIENTS & VISITORS</a></li>
                                              <li <?php if(MODULE_URL==BASE_URL.'news/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>news">NEWS</a></li>
                                              <li <?php if(MODULE_URL==BASE_URL.'contact/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>contact">CONTACT US</a></li>
                                              
                       <a href="#" data-toggle="modal" data-target="#exampleModal"><img class="search" onclick="myFunction()" src="<?php echo BASE_URL ?>assets/home/images/search10"></a></button>
                        </div>
                                        </ul>    
                                    </div>
                                    </div>
                                </div>
                            </nav>
                        </div>  
                    </div>

                </div>

            </div>
        </header> <!--End of header -->