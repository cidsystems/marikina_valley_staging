<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->careers_model	= new careers_model();
		$this->input			= new input();
		$this->fields			= array('id', 'position', 'jobbrief', 'responsibilities', 'requirements');
	}

	public function listing() {
		$this->view->title	= 'Careers';
		$data['ui']			= $this->ui;
		$this->view->load('careers_list', $data);
	}

	public function create() {
		$this->view->title = 'Careers';
		$data = $this->input->post($this->fields);
		$data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_create';
		$data['ajax_post'] = '';
		$data['show_input'] = true;
		$this->view->load('careers', $data);
	}

	public function view($id) {
		$this->view->title = 'Careers View';
		$data = (array) $this->careers_model->getCareersById($this->fields, $id);
		$data['ui'] = $this->ui;
		$data['show_input'] = false;
		$this->view->load('careers', $data);
	}

	public function edit($id) {
		$this->view->title = 'Careers Edit';
		$data = (array) $this->careers_model->getCareersById($this->fields, $id);
		$data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_edit';
		$data['ajax_post'] = "&id=$id";
		$data['show_input'] = true;
		$this->view->load('careers', $data);
	}

	public function ajax($ajax_function) {
		$result = $this->{$ajax_function}();
		header('Content-type: application/json');
		echo json_encode($result);
	}

	public function ajax_list() {
		$pagination = $this->careers_model->getCareersList();
		$table = '';
		
		foreach($pagination->result as $row) {
			$dropdown = $this->ui->loadElement('check_task')
			->addView()
			->addEdit()
			->addPrint()
			->addDelete()
			->addCheckbox()
			->setValue($row->id)
			->draw();
			$table .= '<tr>
							<td>' . $dropdown . '</td>
							<td>' . $row->position . '</td>
							<td>' . $row->jobbrief . '</td>
							
						</tr>';
		}

		$pagination->table = $table;

		return $pagination;
	}
	
	public function ajax_create() {
		$data = $this->input->post($this->fields);
		$textarea = $_POST['mytextarea'];		
		$data['jobbrief'] = $textarea;
		$result = $this->careers_model->saveCareers($data);
		$data['ajax_post'] = '';	
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
		);

	}

	private function ajax_edit() {
		$data = $this->input->post($this->fields);
		$textarea = $_POST['mytextarea'];		
		$data['jobbrief'] = $textarea;
		$id = $this->input->post('id');
		$result = $this->careers_model->updateCareers($data, $id);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
		);
	}

	private function ajax_delete() {
		$delete_id = $this->input->post('delete_id');
		$error_id = array();
		if ($delete_id) {
			$error_id = $this->careers_model->deleteCareers($delete_id);
		}
		return array(
			'success'	=> (empty($error_id)),
			'error_id'	=> $error_id
		);
	}
	
}