<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->patients_model	= new patients_model();
		$this->session			= new session();
		$this->fields 			= array(
			'id',
			'content',
            'image');
	}

	public function listing() {
        $this->view->title	= 'Patients & Visitors';
        $data['rooms'] = $this->patients_model->getRooms();
		$data['ui']			= $this->ui;
		$this->view->load('room', $data);
    }
	
}