<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->home_model		= new home_model();
		$this->session			= new session();
		$this->fields 			= array(
            'id',
            'firstname',
			'lastname',
			'department',
			'specialization',
			'schedule',
			'room',
			'affiliations',
			'contact',
			'image'
		);
		
	}

	public function listing() {
		$search = $this->input->post('search');
		$firstname = $this->input->post('firstname');
		$lastname = $this->input->post('lastname');
		$department = $this->input->post('department');
		$specialization = $this->input->post('specialization');
		$data['sched'] = $this->home_model->getSched();
		$data['doc'] = $this->home_model->getGroupList();
		$data['search'] = $this->home_model->search($search);
		$data['search1'] = $this->home_model->search1($firstname,$lastname,$department,$specialization);
//$data['search2'] = $this->home_model->search2($firstname,$lastname,$department,$specialization);
		$data['doc'] = $this->home_model->search($search);
		$data['department_list'] = $this->home_model->getDept('');
        $data['specialization_list'] = $this->home_model->getSP('');
		$data['show_input'] = true;
		$data['ui']			= $this->ui;
		$this->view->load('search', $data);
		
	}

	public function view($id) {
		$this->view->title = 'View Doctor';
		$data = (array) $this->home_model->getUserById($this->fields, $id);
        $data['ui'] = $this->ui;
        $data['department_list'] = $this->home_model->getDept('');
        $data['specialization_list'] = $this->home_model->getSP('');
        $data['sched'] = $this->home_model->getSched();
		$data['show_input'] = false;
		$this->view->load('doc', $data);
	}


	

}