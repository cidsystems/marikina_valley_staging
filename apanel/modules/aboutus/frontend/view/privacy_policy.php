         <!-- About Section -->
 <section id="aboutus" class="aboutus">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                         <div class="head_title text-center margin-top-60">
                            <h4 class="h4">Privacy Policy</h4>
                        </div>
                        <hr class="divider"> 
                        <div class="main_about_area"> 
                            <div class="row">
                                <?php echo $content; ?>
                            </div>
                            <br><br><br><br>
                        </div>
                    </div><!-- End of col-sm-12 -->
                </div><!-- End of row -->
            </div><!-- End of Container -->
            <hr />
</section><!-- End of about Section -->