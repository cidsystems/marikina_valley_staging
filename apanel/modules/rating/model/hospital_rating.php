<?php
class hospital_rating extends wc_model {

	public function __construct() {
		parent::__construct();
		$this->log = new log();
	}

    public function getRating($star,$sort,$datefilter) {
		if($sort){
			$sort = $sort;
		}else{
			$sort = '';
		}
		$datefilterArr		= explode(' - ',$datefilter);
		$datefilterFrom		= (!empty($datefilterArr[0])) ? date("Y-m-d",strtotime($datefilterArr[0])) : "";
		$datefilterTo		= (!empty($datefilterArr[1])) ? date("Y-m-d",strtotime($datefilterArr[1])) : "";

		$condition = '';
		
		if ($datefilter) {
			$condition = " entereddate BETWEEN '$datefilterFrom 00:00:00' AND '$datefilterTo 23:59:59'";
		}
		
	    $result = $this->db->setTable('rating_hospital')
							->setFields('rating,ip_address,entereddate')
							->setWhere($condition." AND rating = '$star'")
							->setOrderBy($sort)
							->runPagination();

    	return $result;
	}

	public function getCount($sort,$datefilter,$star) {
		if($sort){
			$sort = $sort;
		}else{
			$sort = '';
		}

		$datefilterArr		= explode(' - ',$datefilter);
		$datefilterFrom		= (!empty($datefilterArr[0])) ? date("Y-m-d",strtotime($datefilterArr[0])) : "";
		$datefilterTo		= (!empty($datefilterArr[1])) ? date("Y-m-d",strtotime($datefilterArr[1])) : "";

		
		if ($datefilter) {
			$condition = " entereddate BETWEEN '$datefilterFrom 00:00:00' AND '$datefilterTo 23:59:59'";
		}
		
	    $result = $this->db->setTable('rating_hospital')
							->setFields('COUNT(id) count')
							->setWhere($condition." AND rating = '$star'")
							->runSelect()
							->getResult();

    	return $result;
	}

	public function getRatingCsv($sort,$datefilter) {
		if($sort){
			$sort = $sort;
		}else{
			$sort = '';
		}
	    $datefilterArr		= explode(' - ',$datefilter);
		$datefilterFrom		= (!empty($datefilterArr[0])) ? date("Y-m-d",strtotime($datefilterArr[0])) : "";
		$datefilterTo		= (!empty($datefilterArr[1])) ? date("Y-m-d",strtotime($datefilterArr[1])) : "";

		
		if ($datefilter) {
			$condition = " entereddate BETWEEN '$datefilterFrom 00:00:00' AND '$datefilterTo 23:59:59'";
		}
		
	    $result = $this->db->setTable('rating_hospital')
							->setFields('rating,ip_address,entereddate')
							->setWhere($condition)
							->setOrderBy($sort)
							->runSelect()
							->getResult();

    	return $result;
	}

	public function getAvgRating($datefilter) {
		$datefilterArr		= explode(' - ',$datefilter);
		$datefilterFrom		= (!empty($datefilterArr[0])) ? date("Y-m-d",strtotime($datefilterArr[0])) : "";
		$datefilterTo		= (!empty($datefilterArr[1])) ? date("Y-m-d",strtotime($datefilterArr[1])) : "";

		
		if ($datefilter) {
			$condition = " entereddate BETWEEN '$datefilterFrom 00:00:00' AND '$datefilterTo 23:59:59'";
		}

	    $result = $this->db->setTable('rating_hospital')
				->setFields('AVG(rating) avg, COUNT(id) count')
				->setWhere($condition)
				->runSelect()
				->getRow();

    	return $result;
	}
    
} 