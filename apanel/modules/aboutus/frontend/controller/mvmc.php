<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->aboutus_model	= new aboutus_model();
		$this->session			= new session();
		$this->fields 			= array(
			'id',
			'name',
			'title',
			'content',
			'category',
            'image'
			
		);
	}

	public function listing() {
		$data['exec'] = $this->aboutus_model->getExec();
		$data['med'] = $this->aboutus_model->getMed();
		$data['ui']			= $this->ui;
		$this->view->load('mvmc', $data);
	}

	
}