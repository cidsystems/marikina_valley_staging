<section class="content">
		<div class="box box-primary">
			<div class="box-body">
				<br>
				<form action="" method="post" class="form-horizontal" id="form">
					<div class="row">
						<div class="col-md-11">
							<div class="row">
							<div class="col-md-14">
								<div class="row"> 
								<div class="col-md-6">
									<?php
										echo $ui->formField('text')
											->setLabel('Email')
											->setSplit('col-md-7', 'col-md-2')
											->setName('email')
											->setId('email')
											->setValue($email)
											->draw($show_input);
									?>
								</div>
								<div class="col-md-6">
									<?php
										echo $ui->formField('text')
											->setLabel('Date')
											->setSplit('col-md-4', 'col-md-3')
											->setName('date')
											->setId('date')
											->setValue($datesent)
											->draw($show_input);
									?>
								</div>
								</div>
								</div>
							<div class="col-md-14">
								<div class="row"> 
								<div class="col-md-11">
									<?php
										echo $ui->formField('text')
											->setLabel('Name')
											->setSplit('col-md-4', 'col-md-4')
											->setName('name')
											->setId('name')
											->setValue($name)
											->draw($show_input);
									?>
								</div>
								</div>
								</div>
								<div class="col-md-14">
								<div class="row"> 
								<div class="col-md-11">
									<?php
										echo $ui->formField('textarea')
											->setLabel('Message')
											->setSplit('col-md-4', 'col-md-4')
											->setName('message')
											->setId('message')
											->setValue($message)
											->draw($show_input);
									?>
								</div>
								</div>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-12 text-center">
							<?php //echo $ui->drawSubmit($show_input); ?>
							<a href="<?=MODULE_URL?>" class="btn btn-default" data-toggle="back_page">Back</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
    <script>
    $('form').submit(function(e) {
        e.preventDefault();
        $.post('<?=BASE_URL?>contact/ajax/<?=$ajax_task?>', $(this).serialize() + '<?=$ajax_post?>', function(data) {
            if (data.success) {
				window.location = data.redirect;
			}
        });
    });
   
    

    </script>
