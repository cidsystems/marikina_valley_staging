<?php
class service_model extends wc_model {

	public function __construct() {
		parent::__construct();
		$this->log = new log();
	}

	public function saveService($data) {
		$data['module'] = 'service';
		$result = $this->db->setTable('service')
							->setValues($data)
							->runInsert();

		return $result;
    }
    
    
	public function updateService($data, $id) {
		$result = $this->db->setTable('service')
							->setValues($data)
							->setWhere("id = '$id'")
							->setLimit(1)
							->runUpdate();

		if ($result) {
			$this->log->saveActivity("Update Service [$id]");
		}

		return $result;
    }
    
	public function deleteService($data) {
		$error_id = array();
		foreach ($data as $id) {
			$result =  $this->db->setTable('service')
								->setWhere("id = '$id'")
								->setLimit(1)
								->runDelete();
		
			if ($result) {
				$this->log->saveActivity("Delete Item Type [$id]");
			} else {
				if ($this->db->getError() == 'locked') {
					$error_id[] = $id;
				}
			}
		}

		return $error_id;
    }
    
   

	public function getServiceById($fields, $id) {
		return $this->db->setTable('service')
						->setFields($fields)
						->setWhere("id = '$id'")
						->setLimit(1)
						->runSelect()
						->getRow();
    }
    

	public function getService($fields, $sort, $search) {
		$condition = '';
		if ($search) {
			$condition .= $this->generateSearch($search, array('service' , 'content'));
		}
		$result = $this->db->setTable('service')
		->setFields($fields)
		->setWhere($condition)
		->setOrderBy($sort)
		->runPagination();

		return $result;
	}
	
	public function getServiceFront($fields) {
		
		$result = $this->db->setTable('service')
		->setFields($fields)
                ->setOrderBy('service')
		->runSelect()
                ->getResult();

		return $result;
	}

	private function generateSearch($search, $array) {
		$temp = array();
		foreach ($array as $arr) {
			$temp[] = $arr . " LIKE '%" . str_replace(' ', '%', $search) . "%'";
		}
		return '(' . implode(' OR ', $temp) . ')';
	}

   
}