<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->careers_model	= new careers_model();
		$this->input			= new input();
		$this->fields			= array('id', 'position', 'firstname', 'lastname', 'address', 'email', 'contact','message', 'resume');
	}

	public function listing() {
		$this->view->title	= 'Resume';
		$data['ui']			= $this->ui;
		$this->view->load('careersfile_list', $data);
	}


	public function view($id) {
		$this->view->title = 'Careers View';
		$data = (array) $this->careers_model->getResumeById($this->fields, $id);
		$data['ui'] = $this->ui;
		$data['show_input'] = false;
		$this->view->load('careersfile', $data);
	}


	public function ajax($ajax_function) {
		$result = $this->{$ajax_function}();
		header('Content-type: application/json');
		echo json_encode($result);
	}

	public function ajax_list() {
		$search	= $this->input->post('search');
		$sort	= $this->input->post('sort');
		$pagination = $this->careers_model->getResumeList1($search, $sort);
		$table = '';
		
		foreach($pagination->result as $row) {
			$dropdown = $this->ui->loadElement('check_task')
			->addView()
			->addPrint()
			->addDelete()
			->addCheckbox()
			->setValue($row->id)
			->draw();
			$table .= '<tr>
                            <td>' . $dropdown . '</td>
                            <td>' . $row->firstname.' '.$row->lastname . '</td>
                            <td>' . $row->position . '</td>
							<td>' . $row->email . '</td>
							<td>' . date('F d, Y', strtotime($row->entereddate)) . '</td>
							
						</tr>';
		}

		$pagination->table = $table;

		return $pagination;
	}
	
	public function ajax_create() {
		$data = $this->input->post($this->fields);
		$result = $this->careers_model->saveResume($data);
		$data['ajax_post'] = '';	
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
		);

	}


	private function ajax_delete() {
		$delete_id = $this->input->post('delete_id');
		$error_id = array();
		if ($delete_id) {
			$error_id = $this->careers_model->deleteResume($delete_id);
		}
		return array(
			'success'	=> (empty($error_id)),
			'error_id'	=> $error_id
		);
	}
	
}