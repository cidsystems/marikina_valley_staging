<?php
class contact_model extends wc_model {

    public function getContactList() {
        $result = $this->db->setTable('messages')
                            ->setFields('id, datesent,name,email,message')
                            ->setOrderBy('datesent DESC')
                            ->runPagination();

        return $result;
    }
    public function getAddressList() {
        $result = $this->db->setTable('address')
                            ->setFields('id, building, street, city, mobile,tel, email')
                            ->runPagination();

        return $result;
    }

    public function saveAddress($fields1) {
        $result = $this->db->setTable('address')
                           ->setValues($fields1)
                           ->runInsert();

    }

    public function saveMessage($fields) {
        $fields['datesent'] = $this->date->dateDbFormat();
        $result = $this->db->setTable('messages')
                           ->setValues($fields)
                           ->runInsert();
                        //    if ($result) {
                        //     $this->log->saveActivity("Created News");
                        //   }
                          return $result;
    }

    public function getContactById($fields, $id) {
		return $this->db->setTable('messages')
        ->setFields($fields)
        ->setWhere("id = '$id'")
        ->setLimit(1)
        ->runSelect()
        ->getRow();
    }
    public function getAddressById($fields1) {
		return $this->db->setTable('address')
        ->setFields($fields1)
        ->setLimit(1)
        ->runSelect()
        ->getRow();
    }

    public function updateAddress($data, $id) {
		
		$result = $this->db->setTable('address')
                            ->setValues($data)
                            ->setWhere($id='1')
							->setLimit(1)
                            ->runUpdate();
                            //echo $this->db->getQuery();
		return $result;
    }
    
    public function deleteContact($data) {
		$error_id = array();
		foreach ($data as $id) {
			$result =  $this->db->setTable('messages')
								->setWhere("id = '$id'")
                                ->runDelete();
		}

		return $error_id;
    }
    

}