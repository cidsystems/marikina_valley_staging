<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->newsletter_model	= new newsletter_model();
		$this->input			= new input();
		$this->fields			= array('name','email');
	}


	public function listing() {
		$data = $this->input->post($this->fields);
		$data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_create';
		$data['ajax_post'] = '';
		$data['show_input'] = true;
		$this->view->load('newsletter', $data);
	}


	public function ajax($ajax_function) {
		header('Content-type: application/json');
		$result = $this->{$ajax_function}();
		echo json_encode($result);
	}

	public function check_duplicate(){
		$email = $this->input->post('email');
		$check = $this->newsletter_model->check_duplicate($email);
		if($check){
			$check = true;
		}else{
			$check = false;
		}
		return $check;
	}

	public function ajax_create() {
		$data = $this->input->post($this->fields);
				
		$result = $this->newsletter_model->saveInfo($data);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
		);
	}

}
