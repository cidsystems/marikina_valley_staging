<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->doctors_model	= new doctors_model();
		$this->session			= new session();
		$this->fields 			= array(
			'id',
			'firstname',
			'lastname',
			'department',
			'specialization',
			'schedule',
			'contact',
			'room',
			'affiliations',
			'image',
			'secretary'
			);
	}

	public function listing() {
		$this->view->title = 'Doctors List';
		$data['ui'] = $this->ui;
		$all = (object) array('ind' => 'null', 'val' => 'Filter: All');
		$data['department_list'] = array_merge(array($all),  $this->doctors_model->getDept(''));
		$data['specialization_list'] = array_merge(array($all),  $this->doctors_model->getSP(''));
		$this->view->load('doctors_list', $data);
	}

	public function create() {
		$this->view->title = 'Add Doctor';
		$data = $this->input->post($this->fields);
		$data['ui'] = $this->ui;
		$data['department_list'] = $this->doctors_model->getDept('');
		$data['specialization_list'] = $this->doctors_model->getSP('');
		// $data['sched'] = $this->doctors_model->getSched();
		$data['ajax_task'] = 'ajax_create';
		$data['ajax_post'] = '';
		$data['show_input'] = true;
		$this->view->load('doctors', $data);
	}

	public function edit($id) {
		$this->view->title = 'Edit Doctor';
		$data = (array) $this->doctors_model->getUserById($this->fields, $id);
		$data['ui'] = $this->ui;
		$data['department_list'] = $this->doctors_model->getDept('');
		$data['specialization_list'] = $this->doctors_model->getSP('');
		// $data['sched'] = $this->doctors_model->getSched();
		$data['ajax_task'] = 'ajax_edit';
		$data['ajax_post'] = "&id=$id";
		$data['show_input'] = true;
		$this->view->load('doctors', $data);
	}

	public function view($id) {
		$this->view->title = 'View Doctor';
		$data = (array) $this->doctors_model->getUserById($this->fields, $id);
		$data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_view';
		$data['department_list'] = $this->doctors_model->getDept('');
		$data['specialization_list'] = $this->doctors_model->getSP('');
		// $data['sched'] = $this->doctors_model->getSched();
		$data['show_input'] = false;
		$this->view->load('doctors', $data);
	}
	
	public function ajax($task) {
		$ajax = $this->{$task}();
		if ($ajax) {
			header('Content-type: application/json');
			echo json_encode($ajax);
		}
	}

	private function ajax_list() {
		$data  = $this->input->post(array('search', 'sort'));
		$search  = $data['search'];
		$sort  = $data['sort'];

		$pagination = $this->doctors_model->getGroupList($this->fields, $sort , $search);
		$table = '';
		if (empty($pagination->result)) {
			$table = '<tr><td colspan="9" class="text-center"><b>No Records Found</b></td></tr>';
		}
		foreach ($pagination->result as $key => $row) {
			$table .= '<tr>';
			$dropdown = $this->ui->loadElement('check_task')
			->addView()
			->addEdit()
			->addPrint()
			->addDelete()
			->addCheckbox()
			->setValue($row->id)
			->draw();
			$table .= '<td align = "center">' . $dropdown . '</td>';
			$table .= '<td>
			<img style = "width: 120px;
			height: 120px;
			border-radius: 20px;" src="' . str_replace('/apanel', '', BASE_URL) . "uploads/items/thumb/".$row->image.'"></td>';
			$table .= '<td>' . $row->firstname . '</td>';
			$table .= '<td>' . $row->lastname . '</td>';
			$table .= '<td>' . $row->department . '</td>';
			$table .= '<td>' . $row->specialization . '</td>';
			$table .= '<td>' . $row->schedule . '</td>';
			$table .= '<td>' . $row->contact . '</td>';
			$table .= '<td>' . $row->room . '</td>';
			$table .= '<td>' . $row->affiliations . '</td>';
			$table .= '</tr>';
		}

		$pagination->table = $table;
		return $pagination;
	}
	

	private function ajax_create() {
		$image_uploader = new image_uploader();
		$filename = $image_uploader->setSize(array('large','thumb'))
		->setFolderName('../uploads/items')
		->getImage('item_image');

		$data = $this->input->post($this->fields);
		$data['image'] = $filename;

		$data['specialization']= implode('/', $this->input->post('specialization'));
		
		
		// if(empty($this->input->post('schedule'))) {
		// 	$data['schedule'] = '';
		// } else {
		// 	$data['schedule'] = implode('/', $this->input->post('schedule'));
		// }
		
		$result = $this->doctors_model->saveUser($data);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
			);
	}

	private function ajax_edit() {
		$image_uploader = new image_uploader();
		$filename = $image_uploader->setSize(array('large','thumb'))
		->setFolderName('../uploads/items')
		->getImage('item_image');

		if(empty($filename)) {
			$data = $this->input->post($this->fields);
			$image_name = $this->input->post('image-edit');
			$data['image'] = $image_name;
		} else {
			$data = $this->input->post($this->fields);
			$data['image'] = $filename;
		}

		$data['specialization']= implode('/', $this->input->post('specialization'));

		// $data['schedule'] = implode('/', $this->input->post('schedule'));

		$id = $this->input->post('id');
		$result = $this->doctors_model->updateUser($data, $id);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
			);
	}

	private function ajax_delete() {
		$delete_id = $this->input->post('delete_id');
		$error_id = array();
		if ($delete_id) {
			$error_id = $this->doctors_model->deleteUsers($delete_id);
		}
		return array(
			'success'	=> (empty($error_id)),
			'error_id'	=> $error_id
			);
	}


}