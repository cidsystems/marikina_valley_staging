<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->aboutus_model	= new aboutus_model();
		$this->session			= new session();
		$this->fields 			= array(
			'id',
			'title',
			'content',
			'category',
			
		);
	}

	public function listing() {
		$data['video'] = $this->aboutus_model->getVideo();
		$data['values'] = $this->aboutus_model->getValues();
		$data['mission'] = $this->aboutus_model->getMission();
		$data['vision'] = $this->aboutus_model->getVision();
		$data['history'] = $this->aboutus_model->getHistory();
		$data['ui']			= $this->ui;
		$this->view->load('mission', $data);
	}

	
}