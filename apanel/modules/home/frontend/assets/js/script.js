$('body').on('click', 'a[data-toggle="back_page"]', function(e) {
	if (document.referrer) {
		e.preventDefault();
		if (window.history.length > 1) {
			window.history.back()
		} else {
			window.location = document.referrer;
		}
	}
});