<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->patients_model	= new patients_model();
		$this->session			= new session();
		$this->fields 			= array(
			'id',
			'directory',
			'content',
			'type'
           
		);
	}

	public function listing() {
		$this->view->title = 'Hospital Directory';
		$data['ui'] = $this->ui;
		$all = (object) array('ind' => 'null', 'val' => 'Filter: All');
        $this->view->load('directory_list', $data);
	}

	public function create() {
		$this->view->title = 'Add Directory';
		$data = $this->input->post($this->fields);
		$data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_create';
		$data['ajax_post'] = '';
		$data['show_input'] = true;
		$this->view->load('directory', $data);
	}

	public function edit($id) {
		$this->view->title = 'Edit Directory';
		$data = (array) $this->patients_model->getDirectoryById($this->fields, $id);
        $data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_edit';
		$data['ajax_post'] = "&id=$id";
		$data['show_input'] = true;
		$this->view->load('directory', $data);
	}

	public function view($id) {
		$this->view->title = 'View Directory';
		$data = (array) $this->patients_model->getDirectoryById($this->fields, $id);
        $data['ui'] = $this->ui;
		$data['show_input'] = false;
		$this->view->load('directory', $data);
	}
	
	public function ajax($task) {
		$ajax = $this->{$task}();
		if ($ajax) {
			header('Content-type: application/json');
			echo json_encode($ajax);
		}
	}

	private function ajax_list() {
        $data  = $this->input->post(array('search', 'sort'));
        $search  = $data['search'];
        $sort  = $data['sort'];
    
        $pagination = $this->patients_model->getDirectory($this->fields, $sort , $search);
        $table = '';
        if (empty($pagination->result)) {
          $table = '<tr><td colspan="9" class="text-center"><b>No Records Found</b></td></tr>';
        }
        foreach ($pagination->result as $key => $row) {
          $table .= '<tr>';
          $dropdown = $this->ui->loadElement('check_task')
          ->addView()
          ->addEdit()
          ->addPrint()
          ->addDelete()
          ->addCheckbox()
          ->setValue($row->id)
          ->draw();
          $table .= '<td align = "center">' . $dropdown . '</td>';
          $table .= '<td>' . $row->directory . '</td>';
          $table .= '<td>' . $row->content . '</td>';
          $table .= '</tr>';
        }
        
        $pagination->table = $table;
        return $pagination;
      }
	

	private function ajax_create() {
		$data = $this->input->post($this->fields);
		$textarea = $_POST['mytextarea'];		
		$data['content'] = $textarea;
		$result = $this->patients_model->saveDirectory($data);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
		);
	}

	private function ajax_edit() {
        $data = $this->input->post($this->fields);
		$id = $this->input->post('id');
		$textarea = $_POST['mytextarea'];		
		$data['content'] = $textarea;
		$result = $this->patients_model->updateDirectory($data, $id);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
		);
	}

	

	private function ajax_delete() {
		$delete_id = $this->input->post('delete_id');
		$error_id = array();
		if ($delete_id) {
			$error_id = $this->patients_model->deleteDirectory($delete_id);
		}
		return array(
			'success'	=> (empty($error_id)),
			'error_id'	=> $error_id
		);
	}


}