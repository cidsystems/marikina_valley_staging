<script src='https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=fscaicw4qlktvlve2pbdlj52q8bd8523oc4yo09t9jlqodig'></script>
<script>
	$(function () {
		tinymce.init({
			height : "50",
			selector: '#mytextarea',
			height: 500,
			theme: 'modern',
			fontsize_formats: "8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt",
			plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars code fullscreen',
			'insertdatetime media nonbreaking save table contextmenu directionality',
			'emoticons template paste textcolor colorpicker textpattern imagetools'
			],
			toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | numlist bullist | fontsizeselect | fontselect',
			toolbar2: 'print preview media | forecolor backcolor emoticons',
			image_advtab: true,
			setup: function (editor) {
				editor.on('change', function () {
					editor.save();
				});
			}
		});	
	});
	
</script>

<section class="content">
	<div class="box box-primary">
		<div class="box-body">
			<br>
			<form action="" method="post" class="form-horizontal">
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12">
								<textarea id="mytextarea" name = "mytextarea"><?php echo $content; ?></textarea>
							</div>
							
							
						</div>
						<hr>
						<div class="row">
							<div class="col-md-12 text-center">
								<?php echo $ui->drawSubmit($show_input); ?>
								<a href="<?=MODULE_URL?>" class="btn btn-default" data-toggle="back_page">Cancel</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</section>
		<?php if ($show_input): ?>
			<script>
				$('form').submit(function(e) {
					e.preventDefault();

					$(this).find('.form-group').find('input, textarea, select').trigger('blur');
					if ($(this).find('.form-group.has-error').length == 0) {
						$.post('<?=MODULE_URL?>ajax/<?=$ajax_task?>', $(this).serialize() + '&id='+'<?php echo $id?>', function(data) {
							if (data.success) {
								window.location = data.redirect;
							}
						});
					} else {
						$(this).find('.form-group.has-error').first().find('input, textarea, select').focus();
					}
				});
			</script>
		<?php endif ?>

		<script>
			$('#banner_form').submit(function(e) {
				$('.required').each(function() {
					if ($(this).val().replace(/\s/g,'') == '') {
						$(this).closest('.form-group').addClass('has-error').find('p').html('This field is required.');
					} else {
						$(this).closest('.form-group').removeClass('has-error').find('p').html('');
					}
				});
				if ($('.required_image').closest('label').find('div').html().replace(/\s/g,'') != '') {
					console.log($('.required_image').closest('label').find('div').html().replace(/\s/g,''));
					$('.required_image').closest('.form-group').removeClass('has-error').find('p').html('');
				}
				if ($('.form-group.has-error').length) {
					$('.form-group.has-error').first().find('.form-control').focus();
					e.preventDefault();
				}
			});
			$('body').on('input', '.required', function() {
				if ($(this).val().replace(/\s/g,'') == '') {
					$(this).closest('.form-group').addClass('has-error').find('p').html('This field is required.');
				} else {
					$(this).closest('.form-group').removeClass('has-error').find('p').html('');
				}
			});
			$('#banner_image').on('change', function() {
				var img = new Image();
				var jinput = $(this);
				readURL($(this)[0], jinput, img);
			});
			function readURL(input, jinput, img) { 
				if (input.files && input.files[0]) {
					var reader = new FileReader();
					reader.onload = function (e) {
						var x = `<span class="img-thumbnail">
						<img src="` + e.target.result + `" class="img-responsive img-upload-view">
					</span>`;
					jinput.closest('.form-group').find('label div').html(x);
					jinput.closest('.form-group').find('label.btn').addClass('hidden');
				};
				reader.readAsDataURL(input.files[0]);
			}
		}
	</script>