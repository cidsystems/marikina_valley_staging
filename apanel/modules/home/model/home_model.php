<?php
class home_model extends wc_model {

	public function __construct() {
		parent::__construct();
		$this->log = new log();
	}
	
	public function getApplicants() {
		return $this->db->setTable('resume')
		->setFields('position')
		->runSelect()
		->getResult();
	}

	public function getMessages() {
		return $this->db->setTable('messages')
		->setFields('name')
		->runSelect()
		->getResult();
	}

	public function search($search) {
		$result = $this->db->setTable('doctors')
		->setFields('id,firstname,lastname,department,specialization,schedule,contact,image')
		->setWhere("firstname LIKE '%".$search."%' OR lastname LIKE '%".$search."%' OR department LIKE '%".$search."%' OR CONCAT(FIRSTNAME, ' ', LASTNAME) LIKE '%".$search."%'")
		->runSelect()
		->setLimit(10)
		->getResult();	
		return $result;
	}
	public function search1($firstname,$lastname,$department,$specialization) {
		$result = $this->db->setTable('doctors')
		->setFields('id,firstname,lastname,department,specialization,schedule,contact,image')
		->setWhere("firstname like '%$firstname%' AND lastname like '%$lastname%' AND department like '%$department%' AND specialization like '%$specialization%' ")
		->runSelect()
		->getResult();	
		return $result;
	}
	
	public function searchService($search) {
		$result = $this->db->setTable('service')
		->setFields('id,service')
		->setWhere("service LIKE '%".$search."%' OR content LIKE '%".$search."%' OR module LIKE '%".$search."%' ")
		->runSelect()
		->getResult();	
		return $result;
	}

	public function getNewsList() {
		$result = $this->db->setTable('news')
		->setFields('id, date, title, content,image, pin, updatedate')
		->setWhere("status = 'Published'")
		//->setOrderBy('entereddate DESC')
		->setOrderBy('id DESC')
		->setLimit(3) 
		->runSelect() 
		->getResult();

		return $result;
	}

	public function getService() {
		$result = $this->db->setTable('service')
		->setFields('id, service, content, image')
							//->setWhere("service like '%$searchbar%' ")
		->setLimit(3) 
		-> runSelect() 
		->getResult();

		return $result;
	}
	public function getService1($searchbar) {
		$result = $this->db->setTable('service')
		->setFields('id, service, content')
		->setWhere("service like '%$searchbar%' OR content like '%$searchbar%' OR module like '%$searchbar%'")
		->runSelect()
		->getResult();

		return $result;
	}

public function getDoctors1($searchbar) {
		$result = $this->db->setTable('doctors')
		->setFields('id, firstname, lastname')
		->setWhere("firstname like '%$searchbar%' OR lastname like '%$searchbar%' OR department like '%$searchbar%' OR specialization like '%$searchbar%'")
		->runSelect()
		->getResult();

		return $result;
	}
	
	public function getNewsList1($searchbar) {
		$result = $this->db->setTable('news')
		->setFields('id, date, title, content')
		->setWhere("status = 'Published' AND title like '%$searchbar%' OR module like '%$searchbar%'")
		->setOrderBy('date DESC')
		->runSelect()
		->getResult();

		return $result;
	}

public function getPatients1($searchbar) {
		$result = $this->db->setTable('patients')
		->setFields('id, title,link')
		->setWhere("title like '%$searchbar%' OR module like '%$searchbar%'")
		->runSelect()
		->getResult();

		return $result;
	}
	
	public function getDeptList1($searchbar) {
		$result = $this->db->setTable('department')
		->setFields('id, name, module')
		->setWhere("name like '%$searchbar%' OR module like '%$searchbar%'")
		->runSelect()
		->getResult();

		return $result;

	}

	public function getSpecializationList1($searchbar) {
		$result = $this->db->setTable('specialization')
		->setFields('id, name, module')
		->setWhere("name like '%$searchbar%' OR module like '%$searchbar%'")
		->runSelect()
		->getResult();

		return $result;

	}
	
	public function getFacilityList1($searchbar) {
		$result = $this->db->setTable('facilities')
					->setFields('id,facility')
					->setWhere("facility like '%$searchbar%' OR module like '%$searchbar%'")
                    ->runPagination();

            return $result;

    }
	public function getBanner() {
		$result = $this->db->setTable('banner')
		->setFields('id, title, image')
		->runSelect()
		->getResult();

		return $result;
	}

	public function saveBanner($data) {
		$result = $this->db->setTable('banner')
		->setValues($data)
		->runInsert();

		return $result;
	}


	public function getNewsById($fields, $id) {
		return $this->db->setTable('news')
		->setFields($fields)
		->setWhere("id = '$id'")
		->setLimit(1)
		->runSelect()
		->getRow();
	}

	public function getBannerById($fields, $id) {
		return $this->db->setTable('banner')
		->setFields($fields)
		->setWhere("id = '$id'")
		->setLimit(1)
		->runSelect()
		->getRow();

		return $result;
	}

	public function updateBanner($data, $id) {
		$result = $this->db->setTable('banner')
		->setValues($data)
		->setWhere("id = '$id'")
		->setLimit(1)
		->runUpdate();

		if ($result) {
			$this->log->saveActivity("Update Banner [$id]");
		}

		return $result;
	}

	public function deleteBanner($data) {
		$error_id = array();
		foreach ($data as $id) {
			$result =  $this->db->setTable('banner')
			->setWhere("id = '$id'")
			->setLimit(1)
			->runDelete();

			if ($result) {
				$this->log->saveActivity("Delete Item Type [$id]");
			} else {
				if ($this->db->getError() == 'locked') {
					$error_id[] = $id;
				}
			}
		}

		return $error_id;
	}
	
	public function getUserById($fields, $id) {
		return $this->db->setTable('doctors')
		->setFields($fields)
		->setWhere("id = '$id'")
		->setLimit(1)
		->runSelect()
		->getRow();
	}

	public function getDeptById($fields, $id) {
		return $this->db->setTable('department')
		->setFields($fields)
		->setWhere("id = '$id'")
		->setLimit(1)
		->runSelect()
		->getRow();
	}

	public function getSPById($fields, $id) {
		return $this->db->setTable('specialization')
		->setFields($fields)
		->setWhere("id = '$id'")
		->setLimit(1)
		->runSelect()
		->getRow();
	}

	public function getGroupList() {
		$result = $this->db->setTable('doctors')
		->setFields('id,firstname,lastname,department,specialization,schedule,room,affiliations')
		->setLimit(3)
		->runPagination();

		return $result;

	}

	public function getSched() {
		$result = $this->db->setTable('schedule')
		->setFields('id,day,day_code')
		->runSelect()
		->getResult();

		return $result;

	}

	public function getDeptList() {
		$result = $this->db->setTable('department')
		->setFields('id,name')
		->runPagination();

		return $result;

	}

	public function getSPList() {
		$result = $this->db->setTable('specialization')
		->setFields('id,name')
		->runPagination();

		return $result;

	}

	public function getDept($search = '') {
		$condition = '';
		if ($search) {
			$condition = " department = '$search'";
		}
		$result = $this->db->setTable('department')
		->setFields('name ind, name val')
		->setWhere($condition)
		->setOrderBy('name')
		->runSelect()
		->getResult();

		return $result;
	}

	public function getSP($search = '') {
		$condition = '';
		if ($search) {
			$condition = " specialization = '$search'";
		}
		$result = $this->db->setTable('specialization')
		->setFields('name ind, name val')
		->setWhere($condition)
		->setOrderBy('name')
		->runSelect()
		->getResult();

		return $result;
	}

	public function saveRate($rate,$ip_address) {
		$result = $this->db->setTable('rating_hospital')
							->setValues(array('rating' => $rate, 'ip_address' => $ip_address))
							->runInsert();

		return $result;
	}
	
	public function getIP() {
		$date = $this->date->dateDbFormat();
		$result = $this->db->setTable('rating_hospital')
		->setFields('ip_address')
		->setWhere("entereddate BETWEEN '$date 00:00:00' AND '$date 23:59:59' ")
		->runSelect()
		->getResult();

			$ip = array();
			foreach($result as $row){
				$ip[] = $row->ip_address;
			}

		return $ip;
	}

	public function unsubscribe($email) {
		$result = $this->db->setTable('subscribers')
		->setValues(array('status' => 'unsubscribed'))
		->setWhere("email = '$email'")
		->setLimit(1)
		->runUpdate();

		if ($result) {
			$this->log->saveActivity("Update Status [$email]");
		}

		return $result;
	}

 
}