 <!-- About Section -->
 <section id="aboutus" class="aboutus">
 <div class="container">
     <div class="row">
         <div class="col-sm-12">
             <div class="head_title text-center margin-top-60">
             <!-- <img class="img2" src="<?php echo BASE_URL; ?>assets/home/images/home.jpg" alt="" />
                 <h1>Careers</h1> -->
             </div><!-- End of head title -->

             <div class="main_about_area"> 
             
              
             <div class="col-md-14">
             <h4 class="h4 text-center">News & Events</h4>
             <div class="single_about">
              <br><br>
              <div class="row">
              <hr class="divider">
              <div class="col-md-3">
              <div class="col-sm-14 col-sm-push-1">
              <div>
                  <?php  foreach ($news->result as $row) { ?>
                  <a class="news1" href="<?= BASE_URL ?>news/view/<?= $row->id ?>">
                  <div class="col-md-12 vcenter">
                  <img class="newsimg" src="<?= BASE_URL ?>uploads/items/large/<?php echo $row->image ?>" align="center">
                 <!--  <div class="separator2"></div> -->
               
                 <div class="col-md-5 vcenter">
                  <p style="margin-left: -12px;"><?php echo substr($row->title, 0, 20).'...'; ?></p><br>
                  </div></div>
                  <?php } ?></a>
                 
             
          </div>
      </div>
              </div>
              <div class="col-md-9">
              <?php  foreach ($date as $row) {
                    echo '<b>'.$row->title.'</b>';
                  }
                      ?>
                <hr class="divider">
                  <?php  foreach ($date as $row) { ?>
                    <img class="newsimage" src="<?= BASE_URL ?>uploads/items/large/<?php echo $row->image ?>" align="right">
                    <?php
                    echo '<p>'.$row->content.'</p>';
                  }
                      ?>
                     
                     
                    <div> 
                  </div>
                
             

                  

                 <!--  <a href="" class="btn btn-default">get in touch</a> -->
              </div>
          </div>

      </div>
      
                 </div>
             </div>
         </div><!-- End of col-sm-12 -->
     </div><!-- End of row -->
 </div><!-- End of Container -->
 <hr />
</section><!-- End of about Section -->
<script>
  var ajax = {};
  var ajax_call = '';
  function  getList() {
    if (ajax_call != '') {
      ajax_call.abort();
    }
    
    ajax_call = $.post('<?php echo MODULE_URL ?>ajax/ajax_list', ajax, function(data) {
      $('#right_sub_content #content').html(data.table);
      $('#right_sub_content #pagination').html(data.pagination);
    });
  }
  $('#pagination').on('click', '.pagination li a', function() {
    ajax.page = $(this).attr('data-page');
    getList();
  });
  getList();
</script>