<section class="content">
	<div class="box box-primary">
		<div class="box-body">
			<br>
			<form action="" method="post" class="form-horizontal" id="form">
				<div class="row">
					<div class="col-md-11">
						<div class="row">
							<div class="col-md-6">
								<?php
								echo $ui->formField('text')
								->setLabel('First Name')
								->setSplit('col-md-4', 'col-md-8')
								->setName('firstname')
								->setId('firstname')
								->setValue($firstname)
								->setValidation('required')
								->draw($show_input);
								?>
							</div>
							<!-- </div> -->
							<!-- <div class="row"> -->
							<div class="col-md-6">
								<?php
								echo $ui->formField('text')
								->setLabel('Last Name')
								->setSplit('col-md-4', 'col-md-8')
								->setName('lastname')
								->setId('lastname')
								->setValue($lastname)
								->setValidation('required')
								->draw($show_input);
								?>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<?php
								echo $ui->formField('dropdown')
								->setLabel('Department')
								->setPlaceholder('Select Group')
								->setSplit('col-md-4', 'col-md-8')
								->setName('department')
								->setId('department')
								->setList($department_list)
								->setValue($department)
								->setValidation('required')
								->draw($show_input);
								?>
							</div><!-- 
							</div>
							<div class="row"> -->
								<div class="col-md-6">
									<?php
									$spec_arr = explode('/', $specialization);
									echo $ui->formField('dropdown')
									->setLabel('Specialization')
									->setPlaceholder('Select Group')
									->setSplit('col-md-4', 'col-md-8')
									->setName('specialization[]')
									->setId('specialization')
									->setList($specialization_list)
									->setValue($spec_arr[0])
									->setValidation('required')
									->draw($show_input);
									?>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-md-offset-6">
									<div id="div">
										<?php
										unset($spec_arr[0]);
										foreach ($spec_arr as $spec) {
											echo $ui->formField('dropdown')
											->setLabel('Specialization')
											->setPlaceholder('Select Group')
											->setSplit('col-md-4', 'col-md-8')
											->setName('specialization[]')
											->setId('specialization')
											->setList($specialization_list)
											->setValue($spec)
											->setValidation('required')
											->setButtonAddon('trash')
											->draw($show_input);
											?>
											<!-- <button type="button" class = "btn btn-info delete" 
											value = "<?php echo $spec; ?>"></button> -->
											<?php } ?>
										</div>
									</div>
								</div>

								<div class = "row">
									<div class="col-md-6 col-md-offset-8">
										<?php if($ajax_task == 'ajax_view') { ?>
										<a href="#" id = "spc" hidden = "hidden"><span class = "fa fa-plus-circle"></span> Add Specialization</a>
										<?php } else { ?>
										<a href="#" id = "spc"><span class = "fa fa-plus-circle"></span> Add Specialization</a>
										<?php } ?>
									</div>
								</div>

								<br>

								<div class="row">
									<div class="col-md-6">
										<?php
										echo $ui->formField('text')
										->setLabel('Contact')
										->setSplit('col-md-4', 'col-md-8')
										->setName('contact')
										->setId('contact')
										->setValue($contact)
										->setValidation('required')
										->draw($show_input);
										?>
									</div>
									<div class="col-md-6">
										<div class="col-md-12">
								<div class = "col-md-8 col-md-offset-4">
									<div class="form-group">
										<input type="hidden" name="image-edit" id = "image-edit" value = "<?php echo $image; ?>">

										<label for = "image">
											<?php if($ajax_task == 'ajax_view')  { ?>

											<input id="image" name = "image" type="file" class = "hidden" disabled
											/>

											<?php } else if($ajax_task == 'ajax_edit') { ?>

											<input id="image" name = "image" type="file" class = "hidden"
											value = "<?php echo $image; ?>" accept="image/*"/>

											<?php } else if($ajax_task == 'ajax_create') { ?>
											<input id="image" name = "image" type="file" accept="image/*"/>
											<?php } ?>	
											
											<div>
												<span class="img-thumbnail">
													<?php echo '<img src="' . str_replace('/apanel', '', BASE_URL) . "uploads/items/large/".$image.'" class="img-responsive img-upload-view" alt ="" style = "height: 300px;">' ?>
												</span>
											</div>
										</label>
									</div>
								</div>
							</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<?php
										echo $ui->formField('text')
										->setLabel('Room')
										->setSplit('col-md-4', 'col-md-8')
										->setName('room')
										->setId('room')
										->setValue($room)
										->setValidation('required')
										->draw($show_input);
										?>
									</div>
									<!-- </div> -->
									<!-- <div class="row"> -->
									<div class="col-md-6">
										<?php
										echo $ui->formField('text')
										->setLabel('HMO Affiliations')
										->setSplit('col-md-4', 'col-md-8')
										->setName('affiliations')
										->setId('affiliations')
										->setValue($affiliations)
										->setValidation('required')
										->draw($show_input);
										?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">

										<?php
										echo $ui->formField('text')
										->setLabel('Schedule')
										->setSplit('col-md-4', 'col-md-8')
										->setName('schedule')
										->setId('schedule')
										->setValue($schedule)
										->setValidation('required')
										->draw($show_input);
										
										?>
									</div>
 
									<div class="col-md-6">

										<?php
										echo $ui->formField('text')
										->setLabel('Secretary')
										->setSplit('col-md-4', 'col-md-8')
										->setName('secretary')
										->setId('secretary')
										->setValue($secretary)
										->setValidation('required')
										->draw($show_input);

										?>
										</div>
								</div>


							</div>
							<hr>
							<div class="row">
								<div class="col-md-12 text-center">
									<?php echo $ui->drawSubmit($show_input); ?>
									<a href="<?=MODULE_URL?>" class="btn btn-default" data-toggle="back_page">Cancel</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</section>
			<script>
	$('#image').on('change', function() {
		var img = new Image();
		var jinput = $(this);
		readURL($(this)[0], jinput, img);
	});
	function readURL(input, jinput, img) { 
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				var x = `<span class="img-thumbnail">
				<img src="` + e.target.result + `" class="img-responsive img-upload-view">
			</span>`;

			jinput.closest('.form-group').find('label div').html(x);
			jinput.closest('.form-group').find('#image').addClass('hidden');
		};
		reader.readAsDataURL(input.files[0]);
	}
}
</script>
<?php if($ajax_task == 'ajax_create'): ?>
	<script>
		if($("#image")[0].files.length == 0 ){
			$(".img-thumbnail").addClass("hidden");
		}
	</script>
<?php endif; ?>
			<?php if ($show_input): ?>
				<script>
					$('form').submit(function(e) {
						e.preventDefault();
						$(this).find('.form-group').find('input, textarea, select').trigger('blur');
						if ($(this).find('.form-group.has-error').length == 0) {
							var formData = new FormData($('form')[0]);
							formData.append('item_image', $('#image')[0].files[0]);
							formData.append('id', '<?php echo $id ?>');
							formData.append('image-edit', '<?php echo $image ?>');
							$.ajax({
								url: '<?=MODULE_URL?>ajax/<?=$ajax_task?>',
								type: "POST",
								data: formData,
								processData: false,
								contentType: false,
								success: function(data){
									window.location = data.redirect;
								}
							});
						} else{
							$(this).find('.form-group.has-error').first().find('input, textarea , select'),focus();
						}

					});

				</script>
				<script>
					$('#spc').on('click', function() {
						$('#div').append('<?php
							echo $ui->formField('dropdown')
							->setLabel('Specialization')
							->setPlaceholder('Select Group')
							->setSplit('col-md-4', 'col-md-8')
							->setName('specialization[]')
							->setId('specialization')
							->setList($specialization_list)
							->setValue($specialization)
							->setValidation('required')
							->draw($show_input);
							?>');
						drawTemplate();
					});

					$('#div').on('click', 'button', function() {
						$(this).closest('.form-group').remove();
					});
				</script>

			<?php endif ?>