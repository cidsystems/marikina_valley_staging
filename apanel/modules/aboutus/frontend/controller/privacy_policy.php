<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->aboutus_model	= new aboutus_model();
		$this->session			= new session();
		$this->fields 			= array(
			'id',
			'content'
			
		);
	}

	public function listing() {
		$result = $this->aboutus_model->getPPContent();
		$data['content'] = $result->content;
		$data['ui']			= $this->ui;
		$this->view->load('privacy_policy', $data);
	}

	
}