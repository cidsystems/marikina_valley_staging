       <!-- About Section -->
         <section id="aboutus" class="aboutus">
         <div class="container">
             <div class="row">
                 <div class="col-sm-12">
                     <div class="head_title text-center margin-top-60">
                     <!-- <img class="img2" src="<?php echo BASE_URL; ?>assets/home/images/home.jpg" alt="" />-->
                     <h4 class="h4 text-center">Patients & Visitors Guidelines</h4>
                     </div><!-- End of head title -->
                     <hr class="divider"> 
                    <ul class="nav navbar-nav navbar-left">
                     <li <?php if(MODULE_URL==BASE_URL.'admission/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>admission" <?php if(MODULE_URL==BASE_URL.'admission/'){ echo 'class="active"';}?>>MVMC Admission Procedures</a></li>
                     

                     <li <?php if(MODULE_URL==BASE_URL.'room/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>room" <?php if(MODULE_URL==BASE_URL.'room/'){ echo 'class="active"';}?>>Room Accommodations</a></li>
                      
                      <li <?php if(MODULE_URL==BASE_URL.'hmo/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>hmo" <?php if(MODULE_URL==BASE_URL.'hmo/'){ echo 'class="active"';}?>>HMOs</a></li>
                      
                      <li <?php if(MODULE_URL==BASE_URL.'philhealth/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>philhealth" <?php if(MODULE_URL==BASE_URL.'philhealth/'){ echo 'class="active"';}?>>Philhealth</a></li>
                      
                      <li <?php if(MODULE_URL==BASE_URL.'billing/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>billing" <?php if(MODULE_URL==BASE_URL.'billing/'){ echo 'class="active"';}?>>Billing and Payment</a></li>

                      
                      <li <?php if(MODULE_URL==BASE_URL.'reminders/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>reminders" <?php if(MODULE_URL==BASE_URL.'reminders/'){ echo 'class="active"';}?>>Reminders for Patients & Guests</a></li>
</ul>
                     <div class="main_about_area"> 
                     
                         <div class="row">
                         
                             </div>
                            <br>
                             <?php
                             foreach($admission as $row){
                             echo $row->content;
                             }
                             
                             ?>
                                                                        
                                     </div>
                                     </div>
                                     </div>
                                     </div>

                                   
                     </div>
                 </div><!-- End of col-sm-12 -->
             </div><!-- End of row -->
         </div><!-- End of Container -->
         <hr />
     </section><!-- End of about Section -->