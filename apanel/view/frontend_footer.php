 <!-- footer Section -->
 <div id="foot">
     <footer class="footer fixed-bottom">
        <div class="container">
            <div class="main_footer">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="copyright_text">
                                    <p class=" wow fadeInRight" data-wow-duration="1s">Copyright 2017 Marikina Valley Medical Center. All Rights Reserved </p>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="copyright_text">
                                    <a href="aboutus"><p class=" wow fadeInRight" data-wow-duration="1s">About Us</p></a>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="copyright_text">
                                    <a href="<?= BASE_URL ?>careers"><p class=" wow fadeInRight" data-wow-duration="1s">Careers</p></a>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="copyright_text">
                                    <a href="contact"><p class=" wow fadeInRight" data-wow-duration="1s">Contact Us</p></a>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="copyright_text">
                                    <a href="privacy_policy"><p class=" wow fadeInRight" data-wow-duration="1s">Privacy Policy</p></a>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="copyright_text">
                                    <a data-toggle="modal" href="#newsletter"><p class=" wow fadeInRight" data-wow-duration="1s">Sign up for Newsletter</p></a>
                                </div>
                            </div> 
                            <div class="col-sm-2">
                                <div class="copyright_text">
                                    <p class=" wow fadeInRight" data-wow-duration="1s">Follow us on <a href="https://www.facebook.com/MarikinaValleyMedicalCenter/" class="fa fa-facebook"></a><a href="https://www.instagram.com/marikinavalleymedicalcenter/" class="fa fa-instagram"></a></p>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End of container -->
    </footer><!-- End of footer -->
</div>

<div class="modal fade mod" id="newsletter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="exampleModalLabel">Get Updated! Sign up for our Newsletter!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="row">
            <div class="form-group col-md-2">
                <label for="name"><span style="color:red">*</span>Name</label>
            </div>
            <div class="form-group col-md-6">
               <input type="text" class="aa" name="name" id="name" required="required">
            </div>
        </div>

        <div class="row">
        <div class="form-group col-md-2">
                <label for="email"><span style="color:red">*</span>E-mail</label>
            </div>
            <div class="form-group col-md-6">
                <input type="email" class="aa" name="email" id="emails" required="required">  
                <span id="exists" style="color:red" hidden>Email already exists</span>
            </div>
        </div>
        <div class="row">
        <div class="form-group col-md-1">&nbsp;
        </div>
        <div class="form-group col-md-9">
        <input type="checkbox" id="tick" required> I agree to the <a href="<?php echo BASE_URL.'privacy_policy'?>">terms and conditions.</a>
        </div>
        </div>
        </div>
        
      <div class="modal-footer">
        <button type="button" class = "btn" id = "submit_newsletter" disabled>Submit</button>
      </div>
    </div>
  </div>
</div>       

<div class="modal fade mod" id="thanks" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="exampleModalLabel">Thank you.</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <span>You are now subscribed to Marikina.</span>
        </div>
      <div class="modal-footer">
        <button type="button" class = "btn" id = "close" data-dismiss = "modal">Okay</button>
      </div>
    </div>
  </div>
</div>         

<script>
$(document).ready(function(){
    if($('#tick').is(":checked") === false){
         $('#submit_newsletter').attr("disabled","disabled");
    }else{
         $('#submit_newsletter').removeAttr("disabled");
    }
});
$('#tick').on('click', function (){
    var name  = $('#name').val();
    var email = $('#email').val();
    if($('#tick').is(":checked") === true){
         $('#submit_newsletter').removeAttr("disabled");        
    }else{
         $('#submit_newsletter').attr("disabled","disabled");        
    }
});

$('#newsletter').on('click' ,'#submit_newsletter', function(){
    var name  = $('#name').val();
    var email = $('#email').val();
    var tick  = $('#tick').is(":checked");

    // if(name != '' && email != ''){
        $.post('<?=BASE_URL?>newsletter/ajax/ajax_create', 'name='+ name + '&email=' + email , function(data) {
            if (data.success) {
                $('#newsletter').modal('hide'); 
                $('#thanks').modal('show'); 
                $('#name').val('');
                $('#email').val('');         
            }
        });
    // }else{
    //     $('#submit_newsletter').attr("disabled","disabled");
    // }

});



$('#emails').on('input', function(){
    var email = $(this).val();
    $.post('<?=BASE_URL?>newsletter/ajax/check_duplicate', 'email='+email, function(data) {
        if(data === true){
            $('#submit_newsletter').attr("disabled","disabled");
            $('#exists').show();
		}else{
            $('#submit_newsletter').removeAttr("disabled");
            $('#exists').hide();
		}
        });   
});
</script>

<div class="scrollup">
    <a href="#"><i class="fa fa-chevron-up"></i></a>
</div>

<script src="<?= BASE_URL ?>assets/home/js/jquery.easing.1.3.js"></script>
<script src="<?= BASE_URL ?>assets/home/js/masonry/masonry.min.js"></script>
 
<script src="<?= BASE_URL ?>assets/home/js/plugins.js"></script>
<script src="<?= BASE_URL ?>assets/home/js/main.js"></script>
<script src="<?= BASE_URL ?>assets/home/js/script.js"></script>
</body>
</html>

<style>
    #footer {
        position: absolute;
        right: 0;
        bottom: -20px;
        left: 0;
        text-align: center;
    }
</style>
