 <!-- About Section -->
 <section id="aboutus" class="aboutus">
 <div class="container">
     <div class="row">
         <div class="col-sm-12">
             <div class="head_title text-center margin-top-60">
             <!-- <img class="img2" src="<?php echo BASE_URL; ?>assets/home/images/home.jpg" alt="" />
                 <h1>Careers</h1> -->
             </div><!-- End of head title -->

             <div class="main_about_area"> 
             
              
             <div class="col-md-14">
             <h4 class="h4 text-center">Our Doctors</h4><br><br>
             <div class="single_about">
              <br>
              <div class="row2">
              <hr class="divider">
                        <form action="" method="post" id="form">
                            <div class="panel4"><br><br>
                            <div class="row">
                            <div class="col-sm-14">
                        <?php
                            echo $ui->formField('text')
                            ->setLabel('First Name')
                            ->setSplit('col-md-2', 'col-md-3')
                            ->setName('firstname')
                            ->setId('firstname')
                            ->setClass('input2')
                            ->setPlaceholder('First Name')
                            ->setValidation('required')
                            ->draw($show_input);
                        ?>

                        <?php
                            echo $ui->formField('text')
                            ->setLabel('Last Name')
                            ->setSplit('col-md-2', 'col-md-3')
                            ->setName('lastname')
                            ->setId('lastname')
                            ->setClass('input2')
                            ->setPlaceholder('Last Name')
                            ->setValidation('required')
                            ->draw($show_input);
                        ?>
                            </div></div>
                        <div class="row">
                            <div class="col-sm-14">
                        <?php
                            echo $ui->formField('dropdown')
                            ->setLabel('Department')
                            ->setPlaceholder('Select Group')
                            ->setSplit('col-md-2', 'col-md-3')
                            ->setName('department')
                            ->setId('department')
                            ->setClass('input2')
                            ->setList($department_list)
                            //->setValue($department)
                            ->setValidation('required')
                            ->draw($show_input);
                       
                            echo $ui->formField('dropdown')
                                ->setLabel('Specialization')
                                ->setPlaceholder('Select Group')
                                ->setSplit('col-md-2', 'col-md-3')
                                ->setName('specialization')
                                ->setId('specialization')
                                ->setClass('input2')
                                ->setList($specialization_list)
                                // ->setValue($specialization)
                                ->setValidation('required')
                                ->draw($show_input);
                                
									?>
                            </div></div><br>
                            <div class="row">
                                <div class="col-md-14">

									<?php
                                    //$schedules = explode('/', $schedule);
                                    // foreach($sched as $row){
									// 	echo $ui->formField('checkbox')
                                    //         ->setLabel($row->day_code)
                                    //         ->setSplit('col-md-1', 'col-md-1')
                                    //         ->setName('schedule[]')
                                    //         ->setId($row->id)
                                    //         //->setValue((in_array($row->day_code, $schedules)) ? $row->day_code : '')
                                    //         ->setDefault($row->day_code)
                                    //         ->draw($show_input);
                                    //     }
									?>
                                </div><input type = "submit" value = "Search" id = "search" name="search" class="btn btn-default" style="margin-right: 105px;"></div>
                                <br>
                        </div>                
                        </div><br><br>
             </form>
            
                    <div id="right_sub_content" class = "col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id="content"></div>
                    <div id="pagination"></div><br>
                  </div>
                    </div>
                    <br><br>
                           
      
                         <!--  <a href="" class="btn">learn more</a> -->
                      </div>                     
                  </div><!-- End of row -->
             
               
               
            </div><!-- End of Contaier -->
            
        </section><!-- End of portfolio Section -->   

<script>                       
   var ajax = filterFromURL();
    var ajax_call = '';
    ajaxToFilter(ajax, {});
    function  getList() {
      filterToURL();
      if (ajax_call != '') {
        ajax_call.abort();
      }

      ajax_call = $.post('<?php echo MODULE_URL?>ajax/ajax_list', ajax, function(data) {
        $('#right_sub_content #content').html(data.table);
        $('#right_sub_content #pagination').html(data.pagination);
      });
      
      $('#form').submit(function(e) {
        e.preventDefault();
        ajax.firstname = $('#firstname').val();
        ajax.lastname = $('#lastname').val();
        ajax.department = $('#department').val();
        ajax.specialization = $('#specialization').val();
        ajax_call = $.post('<?=MODULE_URL?>ajax/ajax_list', ajax, function(data) {
            $('#right_sub_content #content').html(data.table);
            $('#right_sub_content #pagination').html(data.pagination);
            window.location.hash = '#right_sub_content';
             //var hash = window.location.hash.substr(1);
            //alert(hash);
        });
      });
    }
    $('#pagination').on('click', '.pagination li a', function(e) {
      e.preventDefault();
      ajax.page = $(this).attr('data-page');
      getList();
    });
    getList(); 
</script>