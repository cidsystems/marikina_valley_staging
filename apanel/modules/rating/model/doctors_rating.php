<?php
class doctors_rating extends wc_model {

	public function __construct() {
		parent::__construct();
		$this->log = new log();
	}

	public function getRating($sort,$datefilter) {
			if($sort){
				$sort = $sort;
			}else{
				$sort = '';
			}

			$datefilterArr		= explode(' - ',$datefilter);
			$datefilterFrom		= (!empty($datefilterArr[0])) ? date("Y-m-d",strtotime($datefilterArr[0])) : "";
			$datefilterTo		= (!empty($datefilterArr[1])) ? date("Y-m-d",strtotime($datefilterArr[1])) : "";

			
			if ($datefilter) {
				$condition = " r.entereddate BETWEEN '$datefilterFrom 00:00:00' AND '$datefilterTo 23:59:59'";
			}
			
			$result = $this->db->setTable('rating_doctors r')
													->leftJoin('doctors d ON d.id = r.doctor_id')
													->setFields('firstname,lastname,AVG(rating) rating,ip_address,doctor_id, COUNT(r.id) count')
													->setWhere($condition)
													->setGroupBy('doctor_id')
													->setOrderBy($sort)
													->runPagination();

    	return $result;
	}

	public function getRatingDetails($id,$sort,$datefilter) {
		if($sort){
			$sort = $sort;
		}else{
			$sort = '';
		}

		$datefilterArr		= explode(' - ',$datefilter);
		$datefilterFrom		= (!empty($datefilterArr[0])) ? date("Y-m-d",strtotime($datefilterArr[0])) : "";
		$datefilterTo		= (!empty($datefilterArr[1])) ? date("Y-m-d",strtotime($datefilterArr[1])) : "";

		$condition = "doctor_id = '$id'";
		
		if ($datefilter) {
			$condition .= " AND entereddate BETWEEN '$datefilterFrom 00:00:00' AND '$datefilterTo 23:59:59'";
		}
		
		$result = $this->db->setTable('rating_doctors')
							->setFields('rating,ip_address,entereddate')
							->setWhere($condition)
							->setOrderBy($sort)
							->runPagination();

	return $result;
}

	public function getRatingCsv($sort,$datefilter) {
		if($sort){
			$sort = $sort;
		}else{
			$sort = '';
		}
		$datefilterArr		= explode(' - ',$datefilter);
			$datefilterFrom		= (!empty($datefilterArr[0])) ? date("Y-m-d",strtotime($datefilterArr[0])) : "";
			$datefilterTo		= (!empty($datefilterArr[1])) ? date("Y-m-d",strtotime($datefilterArr[1])) : "";

		$condition = '';
			
			if ($datefilter) {
				$condition = " r.entereddate BETWEEN '$datefilterFrom 00:00:00' AND '$datefilterTo 23:59:59'";
			}
			
			$result = $this->db->setTable('rating_doctors r')
													->leftJoin('doctors d ON d.id = r.doctor_id')
													->setFields('firstname,lastname,AVG(rating) rating,ip_address,doctor_id, COUNT(r.id) count')
													->setWhere($condition)
													->setGroupBy('doctor_id')
													->setOrderBy($sort)
													->runSelect()
													->getResult();

	return $result;
}

} 