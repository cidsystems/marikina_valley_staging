<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->hospital_rating	= new hospital_rating();
		$this->input			= new input();
		$this->fields			= array('rating','ip_address');
	}

	public function listing() {
		$this->view->title	= 'Hospital Rating';
		$data['datefilter'] 		= $this->date->datefilterMonth();
		$data['ui']			= $this->ui;
		$data['show_input'] = true;
		$this->view->load('hospital_rating', $data);
	}

	public function ajax($ajax_function) {
		$result = $this->{$ajax_function}();
		header('Content-type: application/json');
		echo json_encode($result);
	}	
 
	private function ajax_list() {
		$sort = $this->input->post('sort');
		$datefilter = $this->input->post('daterangefilter');

		$avg = $this->hospital_rating->getAvgRating($datefilter);
		$istars = number_format(round($avg->avg*20, -1));

		$table = '';
		
		$stars = 100;
		$aa = 5;
		for($x=1;$x<=5;$x++){
			$count = $this->hospital_rating->getCount($sort,$datefilter,$aa);
			$table .= '<tr>
							<td class="rate">' . '<div id="fixture'.$x.'" class="stars-80"><span class="stars-container stars-'.$stars.'">★★★★★</span></div>' . '</td>
							<td class="text-left"><a data-toggle="modal" id="ra" href="#raters" data-id="'.$aa.'">' . $count[0]->count . '</a></td>
						</tr>';
			$stars -= 20;
			$aa--;
		}

		return array('table' => $table, 'csv' => $this->get_export(), 'avg' => $istars);
	}

	private function ajax_list1() {
		$star = $this->input->post('star');
		$sort = $this->input->post('sort');
		$datefilter = $this->input->post('daterangefilter');
		$pagination = $this->hospital_rating->getRating($star,$sort,$datefilter);
		$table = '';
		
		if (empty($pagination->result)) {
			$table = '<tr><td colspan="5" class="text-center"><b>No Records Found</b></td></tr>';
		}
			$rate = array();
			$a = 1;
			foreach($pagination->result as $row) {
			$dropdown = $this->ui->loadElement('check_task')
			->addView()
			->addEdit()
			->addPrint()
			->addDelete()
			->addCheckbox()
			->setValue($row->rating)
			->draw();
			$table .= '<tr>
							<td class="rate">' . '<div id="fixtures'.$a.'"></div>' . '</td>	
							<td>' . $row->ip_address . '</td>
							<td>' . $this->date->dateFormat($row->entereddate) . '</td>
						</tr>';
			$rate[] = number_format(round($row->rating*20, -1));
			$a++;
		} 
		$pagination->table = $table;
		$pagination->rate  = $rate;

		return $pagination;
	}

	private function get_export() {
		$sort = $this->input->post('sort');
		$datefilter = $this->input->post('daterangefilter');
		$result = $this->hospital_rating->getRatingCsv($sort,$datefilter);

		$avg = $this->hospital_rating->getAvgRating($datefilter);
		$istars = number_format(round($avg->avg*20, -1));

		$header = array(
			'Hospital Rating',
			'No. of Raters'
		);

		$csv = '';
		$csv .= 'Hospital Ratings';
		$csv .= "\n\n";
		$csv .= '"Date:","' . $datefilter . '"';
		$csv .= "\n\n";
		$csv .= '"Avg:","' . $istars . '"';
		$csv .= "\n\n";
		$csv .= '"' . implode('","', $header) . '"';
		if (empty($result)) {
			$csv .= "\n";
			$csv .= '"",';
			$csv .= '"No Records Found"';
		}
		
		$stars = 100;
		$aa = 5;
		for($x=1;$x<=5;$x++){
			$count = $this->hospital_rating->getCount($sort,$datefilter,$aa);
			$csv .= "\n";
			$csv .= '"' . $aa . '",';
			$csv .= '"' . $count[0]->count . '",';
			$stars -= 20;
			$aa--;
		}
		
		return $csv;
	}
	
}