       <!-- About Section -->
         <section id="aboutus" class="aboutus">
         <div class="container">
             <div class="row">
                 <div class="col-sm-12">
                     <div class="head_title text-center margin-top-60">
                     <!-- <img class="img2" src="<?php echo BASE_URL; ?>assets/home/images/home.jpg" alt="" />-->
                     <!-- <h4 class="h4 text-center">Patients & Visitors Guidelines</h4> -->
                     </div><!-- End of head title -->
                     <!-- <hr class="divider">  -->
                    <ul class="nav navbar-nav navbar-left">
                    <!--  <li <?php if(MODULE_URL==BASE_URL.'admission/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>admission" <?php if(MODULE_URL==BASE_URL.'admission/'){ echo 'class="active"';}?>>MVMC Admission Procedures</a></li> -->
                     

                     <li <?php if(MODULE_URL==BASE_URL.'room/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>room" <?php if(MODULE_URL==BASE_URL.'room/'){ echo 'class="active"';}?>>Room Accommodations</a></li>
                      
                      <li <?php if(MODULE_URL==BASE_URL.'hmo/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>hmo" <?php if(MODULE_URL==BASE_URL.'hmo/'){ echo 'class="active"';}?>>HMOs & Corporate Partners</a></li>
                      
                      <li <?php if(MODULE_URL==BASE_URL.'philhealth/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>philhealth" <?php if(MODULE_URL==BASE_URL.'philhealth/'){ echo 'class="active"';}?>>Philhealth</a></li>
                      
                  <!--     <li <?php if(MODULE_URL==BASE_URL.'billing/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>billing" <?php if(MODULE_URL==BASE_URL.'billing/'){ echo 'class="active"';}?>>Billing and Payment</a></li> -->

                      
                      <li <?php if(MODULE_URL==BASE_URL.'reminders/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>reminders" <?php if(MODULE_URL==BASE_URL.'reminders/'){ echo 'class="active"';}?>>Reminders for Patients & Guests</a></li>
                      <li <?php if(MODULE_URL==BASE_URL.'directory/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>directory" <?php if(MODULE_URL==BASE_URL.'directory/'){ echo 'class="active"';}?>>Hospital Directory</a></li>
</ul>
<style>
.dir{
    background-color: #94D9BD;
}
b{
    color: black;
}
</style>

                        <div class="main_about_area"> 
                        <div class="row col-md-12"><br>
                         <h4 class="aboutus">Hospital Directory</h4><br>
                             </div>

                 

                         <div class="row col-md-12">
                         <h4 class="aboutus">Contact Numbers</h4><br>
                             </div>

                             <div class="col-md-12">
                                <div class="col-md-12">
                                   <p>Trunkline no. 8-682-2222  <br>
                                        Main Hospital Building Dial 1 + Local Number  <br>
                                        Medical Arts Building (Doctor’s Clinic)  Dial 2 + Local Number  <br><br><br> </p>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-5">
                                        <div class="col-md-7">
                                            <p>Admitting Office </p>
                                            <p>Billing Unit </p>
                                            <p>Infusion Therapy Unit </p>
                                            <p>Renal Unit </p>
                                            <p>Emergency Room </p>
                                            <p>Eye Center </p>
                                            <p>Heart Station / Cardiac & Wellness Center </p>
                                            <p>Heart & Vascular Center </p>
                                            <p>ICU</p><br>
                                        </div>
                                        <div class="col-md-1 text-right">
                                            <p>181</p>
                                            <p>138</p>
                                            <p>307</p>
                                            <p>209</p>
                                            <p>103</p>
                                            <p>305</p>
                                            <p>211</p>
                                            <p>559</p><br>
                                            <p>210</p>
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-5">
                                    <div class="col-md-7">
                                            <p>Information </p>
                                            <p>Laboratory </p>
                                            <p>MAB Info </p>
                                            <p>MRI </p>
                                            <p>Medical Records </p>
                                            <p>NICU </p>
                                            <p>Pharmacy </p>
                                            <p>Philhealth </p>
                                            <p>Physical & Rehabiliation Medicine Department</p>
                                            <p>Pulmonary Unit </p>
                                            <p>Radiology </p>
                                        </div>
                                        <div class="col-md-1 text-right">
                                            <p>100</p>
                                            <p>323</p>
                                            <p>174</p>
                                            <p>552</p>
                                            <p>302</p>
                                            <p>207</p>
                                            <p>109</p>
                                            <p>106</p>
                                            <p>312</p><br>
                                            <p>557</p>
                                            <p>115</p>
                                        </div>
                                    </div>
                                </div>
                              </div>
                       


                       <div class="row col-md-12"><br>
                            <h4 class="aboutus">Main Hospital Building</h4><br>
                             </div>
                             <br>
                                                      
                             <div class="col-md-12">
                             <?php
                             foreach($main as $row){
                                                        
                             ?>

                            <div class="col-md-3">
                            <div class="text-center dir"><?php echo '<b>'. $row->directory. '</b>'; ?></div>
                            <?php echo $row->content; ?>
                            
                            </div>
                              <?php
                          }
                             ?></div>

                            <div class="row col-md-12"><br>
                            <h4 class="aboutus">Medical Arts Building</h4><br>
                             </div>
                             <br>
                         <div class="col-md-12">
                             <?php
                             foreach($medical as $row){
                                                        
                             ?>

                            <div class="col-md-3">
                            <div class="text-center dir"><?php echo '<b>'. $row->directory. '</b>'; ?></div>
                            <?php echo $row->content; ?>
                            
                            </div>
                              <?php
                          }
                             ?></div>

                                
                            
                                                                        
                                     </div>
                                     </div>
                                     </div>
                                     </div>

                        
                         
                        
                            <br>
                            
                           

                                   
                     </div>
                 </div><!-- End of col-sm-12 -->
             </div><!-- End of row -->
         </div><!-- End of Container -->
         <hr />
     </section><!-- End of about Section -->