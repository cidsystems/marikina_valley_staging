<?php
class doctors_model extends wc_model {

	public function __construct() {
		parent::__construct();
		$this->log = new log();
	}

	public function saveUser($data) {
		$result = $this->db->setTable('doctors')
							->setValues($data)
							->runInsert();

		return $result;
    }
    
    public function saveDept($data) {
		$data['module'] = 'department';
		$result = $this->db->setTable('department')
							->setValues($data)
							->runInsert();

		return $result;
    }
    
    public function saveSP($data) {
        $data['module'] = 'specialization';
		$result = $this->db->setTable('specialization')
							->setValues($data)
							->runInsert();

		return $result;
	}


	public function updateUser($data, $id) {
		$result = $this->db->setTable('doctors')
							->setValues($data)
							->setWhere("id = '$id'")
							->setLimit(1)
							->runUpdate();

		if ($result) {
			$this->log->saveActivity("Update User [$id]");
		}

		return $result;
    }
    
    public function updateDept($data, $id) {
		$result = $this->db->setTable('department')
							->setValues($data)
							->setWhere("id = '$id'")
							->setLimit(1)
							->runUpdate();

		if ($result) {
			$this->log->saveActivity("Department User [$id]");
		}

		return $result;
    } 
    
    public function updateSP($data, $id) {
		$result = $this->db->setTable('specialization')
							->setValues($data)
							->setWhere("id = '$id'")
							->setLimit(1)
							->runUpdate();

		if ($result) {
			$this->log->saveActivity("Update Specialization [$id]");
		}

		return $result;
	}

	public function deleteUsers($data) {
		$error_id = array();
		foreach ($data as $id) {
			$result =  $this->db->setTable('doctors')
								->setWhere("id = '$id'")
								->setLimit(1)
								->runDelete();
		
			if ($result) {
				$this->log->saveActivity("Delete Item Type [$id]");
			} else {
				if ($this->db->getError() == 'locked') {
					$error_id[] = $id;
				}
			}
		}

		return $error_id;
    }
    
    public function deleteDept($data) {
		$error_id = array();
		foreach ($data as $id) {
			$result =  $this->db->setTable('department')
								->setWhere("id = '$id'")
								->setLimit(1)
								->runDelete();
		
			if ($result) {
				$this->log->saveActivity("Delete Item Type [$id]");
			} else {
				if ($this->db->getError() == 'locked') {
					$error_id[] = $id;
				}
			}
		}

		return $error_id;
    }
    
    public function deleteSP($data) {
		$error_id = array();
		foreach ($data as $id) {
			$result =  $this->db->setTable('specialization')
								->setWhere("id = '$id'")
								->setLimit(1)
								->runDelete();
		
			if ($result) {
				$this->log->saveActivity("Delete Item Type [$id]");
			} else {
				if ($this->db->getError() == 'locked') {
					$error_id[] = $id;
				}
			}
		}

		return $error_id;
	}


	public function getUserById($fields, $id) {
		return $this->db->setTable('doctors')
						->setFields($fields)
						->setWhere("id = '$id'")
						->setLimit(1)
						->runSelect()
						->getRow();
    }
    
    public function getDeptById($fields, $id) {
		return $this->db->setTable('department')
						->setFields($fields)
						->setWhere("id = '$id'")
						->setLimit(1)
						->runSelect()
						->getRow();
    }

    public function getSPById($fields, $id) {
		return $this->db->setTable('specialization')
						->setFields($fields)
						->setWhere("id = '$id'")
						->setLimit(1)
						->runSelect()
						->getRow();
	}

	public function getGroupList($fields , $sort , $search) {
	$condition = '';
		if ($search) {
			$condition .= $this->generateSearch($search, array('firstname' , 'lastname', 'department', 'specialization'));
		}
		$result = $this->db->setTable('doctors')
		->setFields($fields)
		->setWhere($condition)
		->setOrderBy($sort)
		->runPagination();
		return $result;

            return $result;

    }
    
    private function generateSearch($search, $array) {
		$temp = array();
		foreach ($array as $arr) {
			$temp[] = $arr . " LIKE '%" . str_replace(' ', '%', $search) . "%'";
		}
		return '(' . implode(' OR ', $temp) . ')';
	}

    public function getSched() {
		$result = $this->db->setTable('schedule')
                    ->setFields('id,day,day_code')
					->runSelect()
					->getResult();

            return $result;

    }
    
    public function getDeptList() {
		$result = $this->db->setTable('department')
                    ->setFields('id,name')
                    ->runPagination();

            return $result;

    }
    
    public function getSPList() {
		$result = $this->db->setTable('specialization')
                    ->setFields('id,name')
                    ->runPagination();

            return $result;

    }
    
    public function getDept($search = '') {
		$condition = '';
		if ($search) {
			$condition = " department = '$search'";
		}
		$result = $this->db->setTable('department')
						->setFields('name ind, name val')
						->setWhere($condition)
						->setOrderBy('name')
						->runSelect()
						->getResult();

		return $result;
    }
    
    public function getSP($search = '') {
		$condition = '';
		if ($search) {
			$condition = " specialization = '$search'";
		}
		$result = $this->db->setTable('specialization')
						->setFields('name ind, name val')
						->setWhere($condition)
						->setOrderBy('name')
						->runSelect()
						->getResult();

		return $result;
	}


	public function search($firstname,$lastname,$department,$specialization) {
		$result = $this->db->setTable('doctors')
					->setFields('id,firstname,lastname,department,specialization,contact,room,schedule,image')
					->setWhere("firstname like '%$firstname%' AND lastname like '%$lastname%' AND department like '%$department%' AND specialization like '%$specialization%' ")
					->setOrderBy('lastname')
					->setLimit(3)
					->runPagination();
            return $result;
	}
	
	public function search1($firstname,$lastname,$department,$specialization) {
		$result = $this->db->setTable('doctors')
					->setFields('id,firstname,lastname,department,specialization,contact,schedule')
					->setWhere("firstname like '%$search1%' AND lastname like '%$search1%' AND department like '%$department%' AND specialization like '%$specialization%' ")
					->setLimit(3)
                    ->runPagination();
            return $result;
	}
	
	public function saveRate($id,$rate,$ip_address) {
		$result = $this->db->setTable('rating_doctors')
							->setValues(array('doctor_id' => $id ,'rating' => $rate, 'ip_address' => $ip_address))
							->runInsert();

		return $result;
	}

	public function saveRateSecretary($id,$rate,$ip_address) {
		$result = $this->db->setTable('rating_secretary')
							->setValues(array('doctor_id' => $id ,'rating' => $rate, 'ip_address' => $ip_address))
							->runInsert();

		return $result;
	}
	
	public function getIP($id) {
		$date = $this->date->dateDbFormat();
		$result = $this->db->setTable('rating_doctors')
		->setFields('ip_address')
		->setWhere("entereddate BETWEEN '$date 00:00:00' AND '$date 23:59:59' AND doctor_id = '$id'")
		->runSelect()
		->getResult();

			$ip = array();
			foreach($result as $row){
				$ip[] = $row->ip_address;
			}

		return $ip;
	}

	public function getIPSecretary($id) {
		$date = $this->date->dateDbFormat();
		$result = $this->db->setTable('rating_secretary')
		->setFields('ip_address')
		->setWhere("entereddate BETWEEN '$date 00:00:00' AND '$date 23:59:59' AND doctor_id = '$id'")
		->runSelect()
		->getResult();

			$ip = array();
			foreach($result as $row){
				$ip[] = $row->ip_address;
			}

		return $ip;
	}

}