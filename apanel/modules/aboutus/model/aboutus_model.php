<?php
class aboutus_model extends wc_model {

	public function __construct() {
		parent::__construct();
		$this->log = new log();
	}

	public function saveContent($data) {
		$result = $this->db->setTable('aboutus')
							->setValues($data)
							->runInsert();

		return $result;
	}
	
	public function saveImage($data) {
		$result = $this->db->setTable('mvmc')
							->setValues($data)
							->runInsert();

		return $result;
	}
	
	public function updateMVMC($data, $id) {
		$result = $this->db->setTable('mvmc')
							->setValues($data)
							->setWhere("id = '$id'")
							->setLimit(1)
							->runUpdate();

		if ($result) {
			$this->log->saveActivity("Update Content [$id]");
		}

		return $result;
    }
    
    
	public function updateContent($data, $id) {
		$result = $this->db->setTable('aboutus')
							->setValues($data)
							->setWhere("id = '$id'")
							->setLimit(1)
							->runUpdate();

		if ($result) {
			$this->log->saveActivity("Update Content [$id]");
		}

		return $result;
	}
	
	public function updateContent1($data) {
		$data['category'] = 'Video';
		$result = $this->db->setTable('aboutus')
							->setValues($data)
							->setWhere("category='Video'")
							->setLimit(1)
							->runUpdate();

		return $result;
    }
    
	public function deleteContent($data) {
		$error_id = array();
		foreach ($data as $id) {
			$result =  $this->db->setTable('aboutus')
								->setWhere("id = '$id'")
								->setLimit(1)
								->runDelete();
		
			if ($result) {
				$this->log->saveActivity("Delete Item Type [$id]");
			} else {
				if ($this->db->getError() == 'locked') {
					$error_id[] = $id;
				}
			}
		}

		return $error_id;
	}
	
	public function deleteMVMC($data) {
		$error_id = array();
		foreach ($data as $id) {
			$result =  $this->db->setTable('mvmc')
								->setWhere("id = '$id'")
								->setLimit(1)
								->runDelete();
		
			if ($result) {
				$this->log->saveActivity("Delete Item Type [$id]");
			} else {
				if ($this->db->getError() == 'locked') {
					$error_id[] = $id;
				}
			}
		}

		return $error_id;
    }
    
   

	public function getContentById($fields, $id) {
		return $this->db->setTable('aboutus')
						->setFields($fields)
						->setWhere("id = '$id'")
						->setLimit(1)
						->runSelect()
						->getRow();
	}

	
	
	public function getMVMCById($fields, $id) {
		return $this->db->setTable('mvmc')
						->setFields($fields)
						->setWhere("id = '$id'")
						->setLimit(1)
						->runSelect()
						->getRow();
    }
	
	public function getContent() {
		$result = $this->db->setTable('aboutus')
					->setFields('id,content,category')
					->setWhere("category != 'Video'")
                    			->runPagination();

            				return $result;

	}

	public function getMVMC($fields, $sort, $search) {
		$condition = '';
		if ($search) {
			$condition .= $this->generateSearch($search, array('name' , 'title', 'content' , 'category'));
		}

		$result = $this->db->setTable('mvmc')
		->setFields($fields)
		->setWhere($condition)
		->setOrderBy($sort)
		->runPagination();

		return $result;

	}

	private function generateSearch($search, $array) {
		$temp = array();
		foreach ($array as $arr) {
			$temp[] = $arr . " LIKE '%" . str_replace(' ', '%', $search) . "%'";
		}
		return '(' . implode(' OR ', $temp) . ')';
	}

	public function getExec() {
		$result = $this->db->setTable('mvmc')
					->setFields('id,name,title,content,category,image')
					->setWhere('category="MANAGEMENT COMMITTEE "')
					->runSelect()
                    ->getResult();

            return $result;

	}
	public function getMed() {
		$result = $this->db->setTable('mvmc')
					->setFields('id,name,title,content,category,image')
					->setWhere('category="MEDICAL EXECUTIVE COMMITTEE"')
					->runSelect()
                    ->getResult();

            return $result;

	}

	public function getMission() {
		$result = $this->db->setTable('aboutus')
					->setFields('id,content')
					->setWhere('category="Mission Statement"')
                    ->runPagination();

            return $result;

	}
	public function getValues() {
		$result = $this->db->setTable('aboutus')
					->setFields('id,content')
					->setWhere('category="Values"')
                    ->runPagination();

            return $result;

	}
	public function getVision() {
		$result = $this->db->setTable('aboutus')
					->setFields('id,content')
					->setWhere('category="Vision"')
                    ->runPagination();

            return $result;

	}

	public function getVideo() {
		$result = $this->db->setTable('aboutus')
					->setFields('id,content')
					->setWhere('category="Video"')
					->runSelect()
					->getRow();

return $result;
	}

	public function getHistory() {
		$result = $this->db->setTable('aboutus')
					->setFields('id,content')
					->setWhere('category="History"')
					->runPagination();

return $result;
	}

	public function getVideo1() {
		return $this->db->setTable('aboutus')
						->setFields('id,content,category')
						->setWhere("category='Video'")
						->setLimit(1)
						->runSelect()
						->getRow();
	}

	public function getCategory($search = '') {
		$condition = '';
		if ($search) {
			$condition = " category = '$search'";
		}
		$result = $this->db->setTable('cat')
						->setFields('name ind, name val')
						->setWhere($condition)
						->setGroupBy('name')
						->runSelect()
						->getResult();

		return $result;
	}
	public function getCategory1($search = '') {
		$condition = '';
		if ($search) {
			$condition = " category = '$search'";
		}
		$result = $this->db->setTable('mvmccat')
						->setFields('name ind, name val')
						->setWhere($condition)
						->setGroupBy('name')
						->runSelect()
						->getResult();

		return $result;
	}
	
	public function getPPContentList() {
		$result = $this->db->setTable('aboutus')
					->setFields('id,content,category')
					->setWhere("category = 'Private Policy'")
					->runPagination();

            				return $result;

	}

	public function getPPContent() {
		return $this->db->setTable('aboutus')
						->setFields('id,content')
						->setWhere("category = 'Privacy Policy'")
						->setLimit(1)
						->runSelect()
						->getRow();
	}

	public function updatePPContent($data,$id) {
		$result = $this->db->setTable('aboutus')
							->setValues($data)
							->setWhere("id = '$id' AND category='Privacy Policy'")
							->setLimit(1)
							->runUpdate();

		return $result;
    }

}