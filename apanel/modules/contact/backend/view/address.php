<section class="content">
<div class="box box-primary">
    <div class="box-body">
        <br>
        <form action="" method="post" class="form-horizontal" id="form">
            <div class="row">
                <div class="col-md-11">
                    <div class="row">
                        <div class="col-md-14">
                        <div class="row"> 
                        <div class="col-md-11">
                            <?php
                                echo $ui->formField('text')
                                    ->setLabel('Building')
                                    ->setSplit('col-md-3', 'col-md-5')
                                    ->setName('building')
                                    ->setId('building')
                                    ->setValue($building)
                                    ->draw($show_input);
                            ?>
                        </div>
                        </div>
                        </div>
                        <div class="col-md-14">
                        <div class="row"> 
                        <div class="col-md-11">
                            <?php
                                echo $ui->formField('text')
                                    ->setLabel('Street')
                                    ->setSplit('col-md-3', 'col-md-5')
                                    ->setName('street')
                                    ->setId('street')
                                    ->setValue($street)
                                    ->draw($show_input);
                            ?>
                        </div>
                        </div>
                        </div>
                        <div class="col-md-14">
                        <div class="row"> 
                        <div class="col-md-11">
                            <?php
                                echo $ui->formField('text')
                                    ->setLabel('City')
                                    ->setSplit('col-md-3', 'col-md-5')
                                    ->setName('city')
                                    ->setId('city')
                                    ->setValue($city)
                                    ->draw($show_input);
                            ?>
                        </div>
                        </div>
                        </div>
                        <div class="col-md-14">
                        <div class="row"> 
                        <div class="col-md-11">
                            <?php
                                echo $ui->formField('text')
                                    ->setLabel('Mobile')
                                    ->setSplit('col-md-3', 'col-md-5')
                                    ->setName('mobile')
                                    ->setId('mobile')
                                    ->setValue($mobile)
                                    ->draw($show_input);
                            ?>
                        </div>
                        </div>
                        </div>
                        <div class="col-md-14">
                        <div class="row"> 
                        <div class="col-md-11">
                            <?php
                                echo $ui->formField('text')
                                    ->setLabel('Tel')
                                    ->setSplit('col-md-3', 'col-md-5')
                                    ->setName('tel')
                                    ->setId('tel')
                                    ->setValue($tel)
                                    ->draw($show_input);
                            ?>
                        </div>
                        </div>
                        </div>
                        <div class="col-md-14">
                        <div class="row"> 
                        <div class="col-md-11">
                            <?php
                                echo $ui->formField('text')
                                    ->setLabel('Email')
                                    ->setSplit('col-md-3', 'col-md-5')
                                    ->setName('email')
                                    ->setId('email')
                                    ->setValue($email)
                                    ->draw($show_input);
                            ?>
                        </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php echo $ui->drawSubmit($show_input); ?>
                    <a href="<?=MODULE_URL?>" class="btn btn-default" data-toggle="back_page">Back</a>
                </div>
            </div>
        </form>
    </div>
</div>
</section>
<script>
$('form').submit(function(e) {
e.preventDefault();
$.post('<?=BASE_URL?>contact/ajax/<?=$ajax_task?>', $(this).serialize() + '<?=$ajax_post?>', function(data) {
    if (data.success) {
        window.location = data.redirect;
    }
});
});
</script>
