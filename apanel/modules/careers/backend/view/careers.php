<script src='https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=fscaicw4qlktvlve2pbdlj52q8bd8523oc4yo09t9jlqodig'></script>
<script>
	$(function () {
		tinymce.init({
			height : "50",
			selector: '#mytextarea',
			height: 500,
			theme: 'modern',
			fontsize_formats: "8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt",
			plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars code fullscreen',
			'insertdatetime media nonbreaking save table contextmenu directionality',
			'emoticons template paste textcolor colorpicker textpattern imagetools'
			],
			toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | numlist bullist | fontsizeselect | fontselect',
			toolbar2: 'print preview media | forecolor backcolor emoticons',
			image_advtab: true,
			setup: function (editor) {
				editor.on('change', function () {
					editor.save();
				});
			}
		});	
	});
	
</script>
<section class="content">
		<div class="box box-primary">
			<div class="box-body">
				<br>
				<form action="" method="post" class="form-horizontal" id="form">
					<div class="row">
						<div class="col-md-11">
						<div class="row">
								<div class="col-md-12">
									<?php
										echo $ui->formField('text')
											->setLabel('Position')
											->setSplit('col-md-4', 'col-md-6')
											->setName('position')
											->setId('position')
											->setValue($position)
											->setValidation('required')
											->draw($show_input);
									?>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-4"></div>
								<div class="col-md-6">
								<textarea id="mytextarea" name = "mytextarea"><?php echo $jobbrief; ?></textarea>
							</div>
								</div>
							</div>
							
							
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-12 text-center">
							<?php echo $ui->drawSubmit($show_input); ?>
							<a href="<?=MODULE_URL?>" class="btn btn-default" data-toggle="back_page">Cancel</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
    <script>
    $('form').submit(function(e) {
        e.preventDefault();
        $.post('<?=BASE_URL?>careers/ajax/<?=$ajax_task?>', $(this).serialize() + '<?=$ajax_post?>', function(data) {
            if (data.success) {
				window.location = data.redirect;
			}
        });
    });
   
    

    </script>
