<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->aboutus_model	= new aboutus_model();
		$this->session			= new session();
		$this->fields 			= array(
			'id',
			'name',
			'title',
			'image',
			'category'
		);
	}

	public function listing() {
		$this->view->title = 'MVMC Leadership';
		$data['ui'] = $this->ui;
		$all = (object) array('ind' => 'null', 'val' => 'Filter: All');
		$data['category_list'] = array_merge(array($all),  $this->aboutus_model->getCategory1(''));
        $this->view->load('mvmc_list', $data);
	}

	public function create() {
		$this->view->title = 'Add';
		$data = $this->input->post($this->fields);
		$data['ui'] = $this->ui;
		$data['category_list'] = $this->aboutus_model->getCategory1('');
		$data['ajax_task'] = 'ajax_create';
		$data['ajax_post'] = '';
		$data['show_input'] = true;
		$this->view->load('mvmc', $data);
	}

	public function edit($id) {
		$this->view->title = 'Edit';
		$data = (array) $this->aboutus_model->getMVMCById($this->fields, $id);
		$data['ui'] = $this->ui;
		$data['category_list'] = $this->aboutus_model->getCategory1('');
		$data['ajax_task'] = 'ajax_edit';
		$data['ajax_post'] = "&id=$id";
		$data['show_input'] = true;
		$this->view->load('mvmc', $data);
	}

	public function view($id) {
		$this->view->title = 'View';
        $data = (array) $this->aboutus_model->getMVMCById($this->fields, $id);
        $data['ui'] = $this->ui;
        $data['ajax_task'] = 'ajax_view';
        $data['category_list'] = $this->aboutus_model->getCategory1('');
		$data['show_input'] = false;
		$this->view->load('mvmc', $data);
	}
	
	public function ajax($task) {
		$ajax = $this->{$task}();
		if ($ajax) {
			header('Content-type: application/json');
			echo json_encode($ajax);
		}
	}

	private function ajax_list() {
        $data  = $this->input->post(array('search', 'sort'));
        $search  = $data['search'];
        $sort  = $data['sort'];
    
        $pagination = $this->aboutus_model->getMVMC($this->fields, $sort , $search);
        $table = '';
        if (empty($pagination->result)) {
          $table = '<tr><td colspan="9" class="text-center"><b>No Records Found</b></td></tr>';
        }
        foreach ($pagination->result as $key => $row) {
          $table .= '<tr>';
          $dropdown = $this->ui->loadElement('check_task')
          ->addView()
          ->addEdit()
          ->addPrint()
          ->addDelete()
          ->addCheckbox()
          ->setValue($row->id)
          ->draw();
		  $table .= '<td align = "center">' . $dropdown . '</td>';
		  $table .= '<td>' . $row->name . '</td>';
		  $table .= '<td>' . $row->title . '</td>';
		  $table .= '<td>' . $row->category . '</td>';
          $table .= '<td>
          <img style = "width: 120px;
          height: 120px;
          border-radius: 20px;" src="' . str_replace('/apanel', '', BASE_URL) . "uploads/items/large/".$row->image.'"></td>';
          $table .= '</tr>';
        }
        
        $pagination->table = $table;
        return $pagination;
      }
            

	private function ajax_create() {
        $image_uploader = new image_uploader();
        $filename = $image_uploader->setSize(array('large','thumb'))
        ->setFolderName('../uploads/items')
        ->getImage('item_image');

        $data = $this->input->post($this->fields);
        $data['image'] = $filename;
        
        $result = $this->aboutus_model->saveImage($data);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
		);
	}

	private function ajax_edit() {
        $image_uploader = new image_uploader();
        $filename = $image_uploader->setSize(array('large','thumb'))
        ->setFolderName('../uploads/items')
        ->getImage('item_image');

        $data = $this->input->post($this->fields);
        $data['image'] = $filename;

		$id = $this->input->post('id');
		$result = $this->aboutus_model->updateMVMC($data, $id);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
		);
	}

	private function ajax_delete() {
		$delete_id = $this->input->post('delete_id');
		$error_id = array();
		if ($delete_id) {
			$error_id = $this->aboutus_model->deleteMVMC($delete_id);
		}
		return array(
			'success'	=> (empty($error_id)),
			'error_id'	=> $error_id
		);
	}


}