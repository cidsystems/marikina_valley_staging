<?php
class patients_model extends wc_model {

	public function __construct() {
		parent::__construct();
		$this->log = new log();
	}

	public function saveContent($data) {
$data['module'] = 'patients';
		$result = $this->db->setTable('patients')
							->setValues($data)
							->runInsert();

		return $result;
    }
    
    
	public function updateContent($data, $id) {
		$result = $this->db->setTable('patients')
							->setValues($data)
							->setWhere("id = '$id'")
							->setLimit(1)
							->runUpdate();

		if ($result) {
			$this->log->saveActivity("Update Content [$id]");
		}

		return $result;
    }
    
	public function deleteContent($data) {
		$error_id = array();
		foreach ($data as $id) {
			$result =  $this->db->setTable('patients')
								->setWhere("id = '$id'")
								->setLimit(1)
								->runDelete();
		
			if ($result) {
				$this->log->saveActivity("Delete Item Type [$id]");
			} else {
				if ($this->db->getError() == 'locked') {
					$error_id[] = $id;
				}
			}
		}

		return $error_id;
    }
    
   

	public function getContentById($fields, $id) {
		return $this->db->setTable('patients')
						->setFields($fields)
						->setWhere("id = '$id'")
						->setLimit(1)
						->runSelect()
						->getRow();
    }
	
	public function getPatients() {
		$result = $this->db->setTable('patients')
					->setFields('id,image,title,link')
                    ->runPagination();

            return $result;

    }

	public function getContent() {
		$result = $this->db->setTable('patients')
					->setFields('id,image,title')
                    ->runPagination();

            return $result;

    }

    public function getContents() {
		$result = $this->db->setTable('patients')
					->setFields('id,image,title,link')
					//->setWhere('type="content"')
                    ->runPagination();

            return $result;

	}
	//pv

	public function getFacility($data) {
		$result = $this->db->setTable('pv')
					->setFields($data)
                    ->runPagination();

            return $result;

	}

	public function saveFacility($data) {
		$result = $this->db->setTable('pv')
							->setValues($data)
							->runInsert();

		return $result;
    }
    
    
	public function updateFacility($data, $id) {
		$result = $this->db->setTable('pv')
							->setValues($data)
							->setWhere("id = '$id'")
							->setLimit(1)
							->runUpdate();

		if ($result) {
			$this->log->saveActivity("Update Content [$id]");
		}

		return $result;
    }
    
	public function deleteFacility($data) {
		$error_id = array();
		foreach ($data as $id) {
			$result =  $this->db->setTable('pv')
								->setWhere("id = '$id'")
								->setLimit(1)
								->runDelete();
		
			if ($result) {
				$this->log->saveActivity("Delete Item Type [$id]");
			} else {
				if ($this->db->getError() == 'locked') {
					$error_id[] = $id;
				}
			}
		}

		return $error_id;
    }
    
   

	public function getFacilityById($fields, $id) {
		return $this->db->setTable('pv')
						->setFields($fields)
						->setWhere("id = '$id'")
						->setLimit(1)
						->runSelect()
						->getRow();
	}
	
	public function getCategory($search = '') {
		$condition = '';
		if ($search) {
			$condition = " Category = '$search'";
		}
		$result = $this->db->setTable('category')
						->setFields('name ind, name val')
						->setWhere($condition)
						->runSelect()
						->getResult();

		return $result;
	}
	
	//rooms
	public function saveRoom($data) {
		$result = $this->db->setTable('rooms')
							->setValues($data)
							->runInsert();

		return $result;
    }
    
    
	public function updateRoom($data, $id) {
		$result = $this->db->setTable('rooms')
							->setValues($data)
							->setWhere("id = '$id'")
							->setLimit(1)
							->runUpdate();

		if ($result) {
			$this->log->saveActivity("Update Content [$id]");
		}

		return $result;
    }
    
	public function deleteRoom($data) {
		$error_id = array();
		foreach ($data as $id) {
			$result =  $this->db->setTable('rooms')
								->setWhere("id = '$id'")
								->setLimit(1)
								->runDelete();
		
			if ($result) {
				$this->log->saveActivity("Delete Item Type [$id]");
			} else {
				if ($this->db->getError() == 'locked') {
					$error_id[] = $id;
				}
			}
		}

		return $error_id;
    }
    
   

	public function getRoomById($fields, $id) {
		return $this->db->setTable('rooms')
						->setFields('id,content,image')
						->setWhere("id = '$id'")
						->setLimit(1)
						->runSelect()
						->getRow();
    }
	public function getRoom() {
		$result = $this->db->setTable('rooms')
					->setFields('id,content,image')
                    ->runPagination();

            return $result;

    }
	public function getRooms() {
		$result = $this->db->setTable('rooms')
					->setFields('id,content,image')
                    ->runSelect()
                    ->getResult();

            return $result;

    }
    
    //get
    public function getAdmission() {
		$result = $this->db->setTable('pv')
		    ->setFields('id,content,category')
	   	    ->setWhere('category="MVMC Admission Procedures"')	 		 
                    ->runSelect()
                    ->getResult();

            return $result;

    }
     public function getHMO() {
		$result = $this->db->setTable('pv')
		    ->setFields('id,content,category')
	   	    ->setWhere('category="HMOs"')	 		 
                    ->runSelect()
                    ->getResult();

            return $result;

    }
    public function getPhilhealth() {
		$result = $this->db->setTable('pv')
		    ->setFields('id,content,category')
	   	    ->setWhere('category="Philhealth"')	 		 
                    ->runSelect()
                    ->getResult();

            return $result;

    }
    public function getBilling() {
		$result = $this->db->setTable('pv')
		    ->setFields('id,content,category')
	   	    ->setWhere('category="Billing and Payment"')	 		 
                    ->runSelect()
                    ->getResult();

            return $result;

    }
    public function getReminders() {
		$result = $this->db->setTable('pv')
		    ->setFields('id,content,category')
	   	    ->setWhere('category="Reminders for Patients and Guests"')	 		 
                    ->runSelect()
                    ->getResult();

            return $result;

    }

   //hmo image
    public function getHMOimg() {
		$result = $this->db->setTable('hmo')
					->setFields('id,name,image')
                    ->runSelect()
                    ->getResult();

            return $result;

    }
    public function getHMOimg1() {
		$result = $this->db->setTable('hmo')
					->setFields('id,name,image')
                    ->runPagination();

            return $result;

    }
    	public function saveHMO($data) {
		$result = $this->db->setTable('hmo')
							->setValues($data)
							->runInsert();

		return $result;
    }
    
    
	public function updateHMO($data, $id) {
		$result = $this->db->setTable('hmo')
							->setValues($data)
							->setWhere("id = '$id'")
							->setLimit(1)
							->runUpdate();

		if ($result) {
			$this->log->saveActivity("Update Content [$id]");
		}

		return $result;
    }
    public function getHMOById($fields, $id) {
		return $this->db->setTable('hmo')
						->setFields('id,name,image')
						->setWhere("id = '$id'")
						->setLimit(1)
						->runSelect()
						->getRow();
    }
    
	public function deleteHMO($data) {
		$error_id = array();
		foreach ($data as $id) {
			$result =  $this->db->setTable('hmo')
								->setWhere("id = '$id'")
								->setLimit(1)
								->runDelete();
		
			if ($result) {
				$this->log->saveActivity("Delete Item Type [$id]");
			} else {
				if ($this->db->getError() == 'locked') {
					$error_id[] = $id;
				}
			}
		}

		return $error_id;
    }
    
    //Directory
public function getDirectory($data) {
	$result = $this->db->setTable('directory')
				->setFields($data)
				->runPagination();

		return $result;

}

public function saveDirectory($data) {
	$result = $this->db->setTable('directory')
						->setValues($data)
						->runInsert();

	return $result;
}


public function updateDirectory($data, $id) {
	$result = $this->db->setTable('directory')
						->setValues($data)
						->setWhere("id = '$id'")
						->setLimit(1)
						->runUpdate();

	if ($result) {
		$this->log->saveActivity("Update Directory [$id]");
	}

	return $result;
}

public function deleteDirectory($data) {
	$error_id = array();
	foreach ($data as $id) {
		$result =  $this->db->setTable('directory')
							->setWhere("id = '$id'")
							->setLimit(1)
							->runDelete();
	
		if ($result) {
			$this->log->saveActivity("Delete Directory [$id]");
		} else {
			if ($this->db->getError() == 'locked') {
				$error_id[] = $id;
			}
		}
	}

	return $error_id;
}



public function getDirectoryById($fields, $id) {
	return $this->db->setTable('directory')
					->setFields($fields)
					->setWhere("id = '$id'")
					->setLimit(1)
					->runSelect()
					->getRow();
}

public function getMain($data) {
	$result = $this->db->setTable('directory')
				->setFields($data)
				->setWhere("type = 'Main Hospital Building'")
				->runSelect()
				->getResult();

		return $result;

}

public function getMedical($data) {
	$result = $this->db->setTable('directory')
				->setFields($data)
				->setWhere("type = 'Medical Arts Building'")
				->runSelect()
				->getResult();

		return $result;

}
    


}