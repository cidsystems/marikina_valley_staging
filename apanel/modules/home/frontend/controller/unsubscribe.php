<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->home_model		= new home_model();
		$this->session			= new session();
	}

	public function listing() {
		$data['ui']			= $this->ui;
		$this->view->load('unsubscribe', $data);
		
	}

		
	public function ajax($task) {
		$ajax = $this->{$task}();
		if ($ajax) {
			header('Content-type: application/json');
			echo json_encode($ajax);
		}
	}

	private function ajax_edit_status() {
		$email = $this->input->post('email');
		
		$result = $this->home_model->unsubscribe($email);
		
		return array(
			'redirect'	=> BASE_URL,
			'success'	=> $result
		);
	}


	
}