<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->home_model		= new home_model();
		$this->session			= new session();
		$this->fields 			= array(
            'id',
            'firstname',
			'lastname',
			'department',
			'specialization',
            'schedule'
		);
		
	}

	public function listing() {
        $searchbar = $this->input->post('searchbar');
		$data['fac'] = $this->home_model->getFacilityList1($searchbar);
		$data['search'] = $this->home_model->search($searchbar);
        $data['doc'] = $this->home_model->search($searchbar);
        $data['date'] = $this->home_model->getNewsList1($searchbar);
		$data['service'] = $this->home_model->getService1($searchbar);
$data['doctors'] = $this->home_model->getDoctors1($searchbar);
$data['patients'] = $this->home_model->getPatients1($searchbar);
$data['dept'] = $this->home_model->getDeptList1($searchbar);
		$data['spec'] = $this->home_model->getSpecializationList1($searchbar);
		$data['show_input'] = true;
		$data['ui']			= $this->ui;
		$this->view->load('searchbar', $data);
		
	}

	public function view($id) {
		$this->view->title = 'View Doctor';
		$data = (array) $this->home_model->getUserById($this->fields, $id);
		$data['ui'] = $this->ui;
        $data['date'] = $this->home_model->getNewsList1($searchbar);
        $data['department_list'] = $this->home_model->getDept('');
        $data['specialization_list'] = $this->home_model->getSP('');
        $data['sched'] = $this->home_model->getSched();
		$data['show_input'] = false;
		$this->view->load('search_list', $data);
	}


	

}