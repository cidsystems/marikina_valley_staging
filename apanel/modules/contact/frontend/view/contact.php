<style>
.a-messenger, .img-messenger {
    position: relative;
    float:left;
}
.img-messenger {
    min-width:200px;
    max-width: 70%;
    margin-left: -15px;
}
.contact-send-form {
    height:0;
}
@media only screen and (max-width: 991px) {
    .contact-send-form {
        height:45px;
    }
    .img-messenger {
        margin-left: -10px;
    }
}
@media only screen and (max-width: 799px) {
   .contact-send-form {
        height:0;
    } 
    .img-messenger {
        margin-left: -12px;
    }
}
</style>
<!-- Contact Section -->
<section id="contact" class="contactus">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                
                    <!-- <div class="head_title text-center margin-top-80">
                    <img class="img2"src="<?php echo BASE_URL; ?>assets/home/images/home.jpg">
                        <h1>Contact Us</h1>
                    </div> -->
                    <div class="main_contact">
                    <div class="row">
                        <div class="contact_contant">
                            <div class="col-sm-6 col-xs-12">
                                <div class="single_message_right_info">
                                    <ul class="contact">
                                     <?php foreach($content->result as $row){ ?>


                                        <li><i class="fa fa-map-marker"></i> <span>
                                        <?php echo $row->building.','. $row->street.','. $row->city ?></span></li>

                                        <li><i class="fa fa-phone"></i> <span> <?php echo $row->tel ?></span></li>

                                        <li><i class="fa fa-mobile-phone"></i> <span> <?php echo $row->mobile ?></span></li>

                                        <li><i class="fa fa-envelope-o"></i> <span> <?php echo $row->email ?></span></li>
                                    </ul>
                                   <?php } ?>
                                    <!-- <div class="work_socail transition">
                                        <a href=""><i class="fa fa-facebook img-circle"></i></a>
                                        <a href=""><i class="fa fa-twitter img-circle"></i></a>
                                        <a href=""><i class="fa fa-google-plus img-circle"></i></a>
                                        <a href=""><i class="fa fa-pinterest img-circle"></i></a>
                                        <a href=""><i class="fa fa-instagram img-circle"></i></a>
                                    </div> -->
                                </div>
                                <div id="map"></div><br>

                                <script>
function initMap() {
var uluru = {lat: 14.634855, lng: 121.103888};
var map = new google.maps.Map(document.getElementById('map'), {
zoom: 18,
center: uluru
});

var marker = new google.maps.Marker({
position: uluru,
map: map
});
}
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDAGzyqpBDTeSafNvshNyv6U375oFMbhVQ&callback=initMap">
</script>
                </div><!-- End of col-sm-6 -->
                
<div class="modal fade mod" id="hey" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="exampleModalLabel">Thank you.</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <span>Your inquiry has been sent.</span>
        </div>
      <div class="modal-footer">
        <button type="button" class = "btn" id = "close" data-dismiss = "modal">Okay</button>
      </div>
    </div>
  </div>
</div>         
                            <div class="panel4">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single_contant_left margin-top-60">
                                        <form action="" method="post" id="form">
                                            <!--<div class="col-lg-8 col-md-8 col-sm-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-1">-->

                                            <div class="form-group">
                                                Name
                                               <input type="text" class="form-control" name="name" id="name" placeholder="name" required="required">
                                              
                                            </div>

                                            <div class="form-group">
                                                E-mail
                                                <input type="email" class="form-control" name="email" id="email" placeholder="email" required="required">
                                                

                                            </div>

                                            <div class="form-group">
                                                Your message
                                                <textarea class="form-control" name="message" id="message" placeholder="message" cols="52" rows="8" required="required"></textarea>
                                                 
                                            </div>&nbsp;
                                            <div id = "html_element" class="g-recaptcha" data-sitekey="6LctrjwUAAAAAO7iu8SdJtV8WNEhJ_lDJUqoGFBx"></div>
                                            <!-- 6LetdEkUAAAAACIIcEl-CeUBtAZia65j9bYcqazY -->
                                            <div id = "error"></div>
                                            <div class="form-group contact-send-form" text-center>
                                                <input type="submit" class="btn btn-default2" name="send" value="Send"><br><br><br>
                                            </div>
                                            <!--</div>--> 
                                        </form>
                                        <a href="https://m.me/MarikinaValleyMedicalCenter" target="_blank" class="a-messenger"><image src="<?= BASE_URL ?>assets/images/messenger.png" class="img-messenger"></image>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- End of messsage contant-->
                    </div>
                </div>
            </div>
        </div><!-- End of row -->
    </div><!-- End of container -->
</section><!-- End of contact Section -->

<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
$('form').submit(function(e) {
e.preventDefault();
if (grecaptcha && grecaptcha.getResponse().length > 0) { 
    $('#hey').modal('show');
    $.post('<?=BASE_URL?>contact/ajax/ajax_create', $(this).serialize() + '<?=$ajax_post?>', function(data) {
    if (data.success) {
        $('#close').on('click', function() {
            window.location = data.redirect;
             });            
    }
});
} else {
    document.getElementById('error').innerHTML="<span style='color: red;'>*Check if you're not a robot.</span>";
            return false;
}

});
</script>

