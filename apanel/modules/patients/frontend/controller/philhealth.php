<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->patients_model	= new patients_model();
		$this->session			= new session();
		$this->fields 			= array(
			'id',
			'content'
			
		);
	}

	public function listing() {
		$data['philhealth'] = $this->patients_model->getPhilhealth();
		$data['patients'] = $this->patients_model->getContents();
		$this->view->title	= 'Patients & Visitors';
		$data['ui']			= $this->ui;
		$this->view->load('philhealth', $data);
	}
}