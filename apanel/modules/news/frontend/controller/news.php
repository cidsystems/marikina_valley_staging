<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->news_model		= new news_model();
		//$this->about			= $this->checkOutModel('about/about');
		$this->session			= new session();
		$this->fields 			= array(
			'id',
			'title',
			'date',
			'content',
			'image'
		);
	}

	public function listing() {
		$data['news'] = $this->news_model->getNewsListFront($this->fields);
		$data['date'] = $this->news_model->getLatestNews();
		$data['month'] = $this->news_model->getMonth();
		$this->view->title	= 'News List';
		$data['ui']			= $this->ui;
		$this->view->load('news_list', $data);
	}

	public function view($id) {
		$data = (array) $this->news_model->getNewsById($this->fields, $id);
		$data['date'] = $this->news_model->getNewsListFront($this->fields);
		$data['ui']			= $this->ui;
		$data['show_input'] = false;
		$this->view->load('news', $data);
	}

	public function ajax($task) {
		header('Content-type: application/json');
		$result = $this->{$task}();
		echo json_encode($result);
	}
	
	public function ajax_list() {
		$pagination = $this->news_model->getLatestNews();
		$table = '';
		
		

// 		$pagination->table = $table;

		return $pagination;
	}


}