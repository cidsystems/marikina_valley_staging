 <!-- About Section -->
 <section id="aboutus" class="aboutus">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="head_title text-center margin-top-80">
                        <img class="img2" src="<?php echo BASE_URL; ?>assets/home/images/home.jpg" alt="" />
                            <h1>News</h1>
                        </div><!-- End of head title -->

                        <div class="main_about_area"> 
                        
                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="signle_about_left">
                                        <img src="assets/home/images/service10.jpg" alt="" />
                                        <div class="single_about_left_text">
                                           <!--  <div class="separator2"></div> -->
                                           
                                        </div>
                                    </div>


                                </div>

                                <div class="col-sm-6 col-sm-push-1">
                                    <div class="single_about">
                                        <!-- <div class="separator2"></div> -->
                                        <?php  foreach ($date->result as $row) {
											?>
											<h4><?php
                                            echo $row->title;
                                        ?>
                                            <p class="about"><?php
                                            echo $row->content;
                                        ?>
                                        </p>
                                      <?php  }
                                        ?> 

                                        

                                       <!--  <a href="" class="btn btn-default">get in touch</a> -->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div><!-- End of col-sm-12 -->
                </div><!-- End of row -->
            </div><!-- End of Container -->
            <hr />
        </section><!-- End of about Section -->