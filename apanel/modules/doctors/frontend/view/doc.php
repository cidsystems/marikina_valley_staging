<section class="content">
    <div class="box box-primary">
        <div class="box-body">
            <br>
            <form action="" method="post" class="form-horizontal">
                <div class="row" style = "width: 100%;">
                    <div class="col-md-4 text-center">
                        <p class="doc1">
                            <a href="#" data-toggle="back_page"><i class="fa fa-long-arrow-left"></i> Back to Listing</a></p>
                        </div></div>
                        <div class="row" style = "width: 100%;">
                            <div class="col-md-11">

                                <div class="row3">
                                    <div class="col-md-4 text-center">
                                        Doctor Details <br><br>
                                        <div align="right">
                                            <img class="img4" src="<?= BASE_URL ?>uploads/items/large/<?php echo $image ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6"><br><br><br>
                                        <!-- <div class="row"> -->
                                            <!-- <div class="col-md-6"> -->
                                            <h4 class="doc"><?php echo $lastname.', '.$firstname ?></h4>
                                            <!-- </div> -->
                                            <!-- <div class="col-md-6"><h4></h4> -->
                                               
                                            <!-- </div> -->
                                        <!-- </div> -->
                                       
                                        <h4 class="doc"><?php echo $department ?></h4>

                                         <?php 
                                                        if(!in_array($_SERVER['REMOTE_ADDR'],$ip)): ?>
                                                        <!-- <div class="starrr1">
                                                            <label for="asd">Rate me:</label>
                                                            <div class='starrr' id='star1'></div>
                                                                <div>&nbsp;
                                                                <span class='your-choice-was' style='display: none;'>
                                                                    Your rating was <span class='choice'></span>. Thank you.
                                                                </span>
                                                            </div>
                                                        </div> -->
                                                <?php endif ?>
                                        <b class="doctor">Room: <?php echo $room ?></b><br>
                                        <b class="doctor">Schedule: <?php echo $schedule ?></b><br>
                                        <p class="doctor"><i>Schedules may change without prior notice due to Medical Conventions and
                                            other events.</i></p>
                                            <b class="doctor">Telephone: <?php echo $contact ?></b><br>

                                            <b class="doctor">HMO Affiliations: <?php echo $affiliations ?></b><br>
                                            
                                            <?php $spec_arr = explode('/', $specialization); ?>
                                            <b class = "doctor">Specialization : <?php echo $spec_arr[0]; ?></b>
                                            <br>
                                            
                                            <?php unset($spec_arr[0]); ?>
                                             <?php if($spec_arr) { ?>
                                            <a style = "cursor : pointer;" data-toggle="collapse" data-target="#demo"><i>See more...</i></a>
                                            <?php } ?>
                                            
                                            <div id="demo" class="collapse">
                                                <?php foreach ($spec_arr as $spec) {?>
                                                <li><i> <?php echo $spec ?></i></li>
                                                <?php } ?>
                                            </div>
                                            <br>
                                            <?php //if($secretary):?>
                                            <?php 
                                                if(!in_array($_SERVER['REMOTE_ADDR'],$ip_sec)): ?>
                                                <div class="starrr2">
                                                    <label for="asd">Rate my secretary:</label>
                                                    <div class='starrr2' id='star2'></div>
                                                        <div>&nbsp;
                                                        <span class='your-choice-was2' style='display: none;'>
                                                            <span class='choice2'></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            <?php endif ?>
                                            <?php //endif ?>
                                            
                                            <br><br><br><br>
                                            For emergencies, please call 682-2222 <br><br>
                                        </div>
                                    </div>                  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.js"></script>
  <script src="<?= BASE_URL ?>assets/home/dist/starrr.js"></script>
  <script>
    $('#star1').starrr({
      change: function(e, value){
        if (value) {
          $('.your-choice-was').show();
          $('.choice').text(value);
          $.post('<?=MODULE_URL?>ajax/<?=$ajax_task?>', 'id=' + '<?php echo $id ?>' + '&rate='+ value + '&ip_address='+'<?php echo $_SERVER['REMOTE_ADDR'];?>', function(data) {
                    $( ".starrr1, .your-choice-was" ).delay(1000).fadeOut("slow");
          });
          
        } else {
          $('.your-choice-was').hide();
        }
      }
    });

    $('#star2').starrr({
      change: function(e, value){
          var msg = '';
        if (value) {
          $('.your-choice-was2').show();
          if(value == 1){
              msg = 'You have given a poor rating, 1 star. Thank you.';
          }else if(value == 2){
            msg = 'You have given a fair rating, 2 stars. Thank you.';
          }else if(value == 3){
            msg = 'You have given a good rating, 3 stars. Thank you.';
          }else if(value == 4){
            msg = 'You have given a very good rating, 4 stars. Thank you.';
          }else if(value == 5){
            msg = 'You have given the highest rating, 5 stars. Thank you.';
          }
          $('.choice2').text(msg);
          $.post('<?=MODULE_URL?>ajax/ajax_rate_secretary', 'id=' + '<?php echo $id ?>' + '&rate='+ value + '&ip_address='+'<?php echo $_SERVER['REMOTE_ADDR'];?>', function(data) {
            $( ".starrr2, .your-choice-was2" ).delay(1000).fadeOut("slow");
          });
          
        } else {
          $('.your-choice-was').hide();
        }
      }
    });

    
  </script>
  <script type="text/javascript">
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-39205841-5', 'dobtco.github.io');
    ga('send', 'pageview');
  </script>