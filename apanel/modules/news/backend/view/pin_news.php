<section class="content">
	<div class="box box-primary">
		<div class="box-body">
			<br>
			<form method="POST" class="form-horizontal" id="form">
				<div class="row">
					<div class="col-md-11">
						<div class="row">
							<div class="col-md-12">
								<?php
								foreach ($all as $row) :
									if($row->pin == 'pinned') { 
										echo $ui->formField('checkbox')
										->setLabel($row->title)
										->setSplit('col-md-4', 'col-md-6')
										->setName('checkbox[]')
										->setId('checkbox')
										->setValidation('required')
										->setValue($row->id)
										->setDefault($row->id)
										->draw($show_input);
									} else {
										echo $ui->formField('checkbox')
										->setLabel($row->title)
										->setSplit('col-md-4', 'col-md-6')
										->setName('checkbox[]')
										->setId('checkbox')
										->setValidation('required')
										->setDefault($row->id)
										->draw($show_input);
									}

									endforeach;
									?>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-12 text-center">
							<?php echo $ui->drawSubmit($show_input); ?>
							<a href="<?=MODULE_URL?>" class="btn btn-default" data-toggle="back_page">Cancel</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
	<?php if ($show_input): ?>
		<script>
			$(document).ready(function() {
				$('#checkbox').change(function() {
					if ($(':checkbox[id=checkbox]:checked').length == 2) {
						alert("a");
					} else {
						alert("h");
					}
				});
			});

			$('form').submit(function(e) {
				e.preventDefault();
				$(this).find('.form-group').find('input, textarea, select').trigger('blur');
				if ($(this).find('.form-group.has-error').length == 0) {
					$.post('<?=MODULE_URL?>ajax/<?=$ajax_task?>', $(this).serialize() + '<?=$ajax_post?>', function(data) {
						if (data.success) {
							window.location = data.redirect;
						}
					});
				} else {
					$(this).find('.form-group.has-error').first().find('input, textarea, select').focus();
				}
			});

		</script>

	<?php endif ?>