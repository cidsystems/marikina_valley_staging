<section class="content">
<div class="box box-primary">
    <div class="box-body">
        <br>
        <form action="" method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-11">
                    <div class="row">
                        <div class="col-md-4 text-center">
                             <br><br>
                        </div>
                        </div>
                        <div class="col-md-4"><br><br><br>
                        
                            
                        </div>

                    <div class="col-md-6"><br><br><br>
					<div class="col-md-4 col-sm-3 col-xs-2"></div>
					<div class="col-md-10 col-sm-6 col-xs-8 center">
                    <div class="panel-heading"><h1 style="color:#1565C0">Do you really have to go?</h1></div>
                    <div class="panel-success" style="background-color: white;padding: 0px 4px 0px 34px;">&nbsp;
						<label align="left">We've received your request to unsubscribe. If you go, you will be missed.</label><br><br>
						<label align="left">Are you really sure about this?</label><br><br>
						
						<div class="row col-md-12"><button type="button" class="btn btn-danger btn-md btn-block" id="btn_yes" data-dismiss="modal">Yes, I really have to go.</button></div><br><br>
						<div class="row col-md-12"><button type="button" class="btn btn-primary btn-md btn-block" id="btn_no" data-dismiss="modal">Nah, I can stay a bit longer.</button></div><br><br>
		
                    <br><br>
										
					<br><br>
					<input type="text" id="email" value="" hidden="">
					
					
					</div>
					<div class="col-md-4 col-sm-3 col-xs-2"></div>
				</div>
                            
                        </div>

            
        </div>                  
    </div>
</div>
</div>
</div>
</section>
<script>
$('#btn_yes').on('click', function(){
    var email = "<?php echo $_GET['email'] ?>";
    $.post('<?=BASE_URL?>unsubscribe/ajax/ajax_edit_status', 'email='+email, function(data) {

        if (data.success) {
            window.location = data.redirect;
        }
    });
});
</script>