<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->mail_config		= new mail_config();
		$this->session			= new session();
		$this->fields			= array(
			'mailserver',
			'servername',
			'username',
			'password',
			'sender',
			'recipient',
			'replyto'
		);
	}

	public function edit() {
		$this->view->title = 'Edit Mail Configuration';
		$data['fields'] = $this->mail_config->retrieveMailConfig($this->fields);
		$data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_edit';
		$data['show_input'] = true;
		$this->view->load('mail_config/mail_config', $data);
	}
	
	public function ajax($task) {
		$ajax = $this->{$task}();
		if ($ajax) {
			header('Content-type: application/json');
			echo json_encode($ajax);
		}
	}


	private function ajax_edit() {
		$data = $this->input->post($this->fields);
		// var_dump($data);
		$result = $this->mail_config->updateMailConfig($data);
		return array(
			'redirect' => MODULE_URL,
			'success' => $result
		);
	}

	private function ajax_create_test() {
		$data = $this->input->post($this->fields);
		
			$message = "<h4>New password has been generated for test</h4>";
			$message .= "<p><b>Account Login</b></p>";
			$message .= "<p><b>Username :</b> test</p>";
			$message .= "<p><b>Password :</b> test</p>";
			$message .= "You can now logon at <b><a target='_blank' href='".BASE_URL."'>".BASE_URL."</a> - Oojeema </b>";

			$mail = new PHPMailers();
			$mail->isSMTP();
			$mail->Host = $data['mailserver'];
			$mail->SMTPAuth = true;
			$mail->Username = $data['username'];
			$mail->Password = $data['password'];
			// $mail->SMTPSecure = 'ssl';
			// $mail->Port       = 465;
			$mail->SMTPSecure = 'TLS';
			$mail->Port       = 587;

			$mail->From = $data['sender'];
			$mail->FromName = $data['servername'];
			$mail->addAddress($data['recipient']);

			$mail->addReplyTo($data['replyto']);

			$mail->WordWrap = 50;
			$mail->isHTML(true);

			$mail->Subject = 'Forgot Password';
			$mail->Body    = $message;
			
			if(!$mail->send()) {
				return array('error' => $mail->ErrorInfo);
			  } 
			//   else{
			// 		 $updatepw = $this->mail_config->updatePassword('marion.rosales@cid-systems.com','654');

			//   }

		return array(
			'redirect' => MODULE_URL,
			'result' => $data
		);
	}
}