<style>
    .huge {
        font-size: 20px;
    }

    .panel-green {
        border-color: #5cb85c;
    }

    .panel-green .panel-heading {
        border-color: #5cb85c;
        color: #fff;
        background-color: #5cb85c;
    }

    .panel-green a {
        color: #5cb85c;
    }

    .panel-green a:hover {
        color: #3d8b3d;
    }

    .panel-red {
        border-color: #d9534f;
    }

    .panel-red .panel-heading {
        border-color: #d9534f;
        color: #fff;
        background-color: #d9534f;
    }

    .panel-red a {
        color: #d9534f;
    }

    .panel-red a:hover {
        color: #b52b27;
    }

    .panel-yellow {
        border-color: #f0ad4e;
    }

    .panel-yellow .panel-heading {
        border-color: #f0ad4e;
        color: #fff;
        background-color: #f0ad4e;
    }

    .panel-yellow a {
        color: #f0ad4e;
    }

    .panel-yellow a:hover {
        color: #df8a13;
    }

    .panel-pink {
        border-color: #FF69B4;
    }

    .panel-pink .panel-heading {
        color: white;
        background-color: #FF69B4;
    }

</style>
<section class="content">
    <div class="box box-primary">
        <div class="box-header pb-none">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-newspaper-o fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <?php $i = 0; ?>
                                    <?php foreach($applicants as $app) : ?>
                                        <?php $i++; ?>
                                    <?php endforeach; ?>
                                    <div class="huge">You have <?php echo $i; ?></div>
                                    <div>Applicants</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?= BASE_URL?>careersresume">
                            <div class="panel-footer">
                                <span class="pull-left">View Applicants</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-envelope fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <?php $i = 0; ?>
                                    <?php foreach($messages as $msg) : ?>
                                        <?php $i++; ?>
                                    <?php endforeach; ?>
                                    <div class="huge">You have <?php echo $i; ?></div>
                                    <div>Messages</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?= BASE_URL?>contact">
                            <div class="panel-footer">
                                <span class="pull-left">View Messages</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
