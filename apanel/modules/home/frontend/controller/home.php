<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->home_model		= new home_model();
		$this->session			= new session();
		$this->fields			= array(
            'id', 'title', 'image'
		);
	}

	public function index() {
		$search = $this->input->post('search');
		$searchbar = $this->input->post('searchbar');
		$data['banner'] = $this->home_model->getBanner();
		$data['search'] = $this->home_model->search($search);
		$data['doc'] = $this->home_model->search($search);
		$data['date'] = $this->home_model->getNewsList();
		$data['service'] = $this->home_model->getService();
		$this->view->title	= 'About List';
		$ip = $this->home_model->getIP();
		$data['ip'] = $ip;
		$data['ui']			= $this->ui;
		$this->view->load('home', $data);
		
	}

	public function listing() {
		$search = $this->input->post('search');
		$data['banner'] = $this->home_model->getBanner();
		$data['search'] = $this->home_model->search($search);
		$data['doc'] = $this->home_model->search($search);
		$data['date'] = $this->home_model->getNewsList();
		$data['service'] = $this->home_model->getService();
		$this->view->title	= 'About List';
		$data['ui']			= $this->ui;
		$this->view->load('home', $data);
		
	}

	public function view($id) {
		$this->view->title = 'View';
		$data = (array) $this->home_model->getBannerById($this->fields, $id);
		$data['banner'] = $this->home_model->getBanner();
		$data['ui'] = $this->ui;
		$data['show_input'] = false;
		$this->view->load('banner', $data);
		}

		
	public function ajax($task) {
		$ajax = $this->{$task}();
		if ($ajax) {
			header('Content-type: application/json');
			echo json_encode($ajax);
		}
	}


	public function ajax_search($search) {
		$data['result'] = $this->home_model->searchService($search);
		$this->view->load('service', $data);
	  }

	  private function ajax_rate() {
		$rate 		= $this->input->post('rate');
		$ip_address = $this->input->post('ip_address');
		
		$result = $this->home_model->saveRate($rate,$ip_address);

		return array(
			'redirect'	=> MODULE_URL,
			'result'	=> $result
			);
	}
}