<?php
class banner_model extends wc_model {

	public function __construct() {
		parent::__construct();
		$this->log = new log();
	}

    public function getBanner() {
        $result = $this->db->setTable('banner')
                            ->setFields('id, image,title')
                            ->runPagination();

        return $result;
    }

    public function saveBanner($data) {
		$result = $this->db->setTable('banner')
							->setValues($data)
							->runInsert();

		return $result;
    }


    public function getBannerById($fields, $id) {
		return $this->db->setTable('banner')
						->setFields($fields)
						->setWhere("id = '$id'")
						->setLimit(1)
						->runSelect()
						->getRow();
						
		return $result;
    }

    public function updateBanner($data, $id) {
		$result = $this->db->setTable('banner')
							->setValues($data)
							->setWhere("id = '$id'")
							->setLimit(1)
							->runUpdate();

		if ($result) {
			$this->log->saveActivity("Update Banner [$id]");
		}

		return $result;
    }

    public function deleteBanner($data) {
		$error_id = array();
		foreach ($data as $id) {
			$result =  $this->db->setTable('banner')
								->setWhere("id = '$id'")
								->setLimit(1)
								->runDelete();
		
			if ($result) {
				$this->log->saveActivity("Delete Item Type [$id]");
			} else {
				if ($this->db->getError() == 'locked') {
					$error_id[] = $id;
				}
			}
		}

		return $error_id;
    }
	
}