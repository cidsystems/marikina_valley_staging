         <!-- About Section -->
         <section id="aboutus" class="aboutus">
         <div class="container">
             <div class="row">
                 <div class="col-sm-12">
                      <div class="head_title text-center margin-top-60">
                 <!-- <img class="img2" src="<?php echo BASE_URL; ?>assets/home/images/home.jpg" alt="" />-->
                     <h4 class="h4">About Us</h4>
                     </div><!-- End of head title -->
                     <hr class="divider"> 
                     <ul class="nav navbar-nav navbar-left">
                                         <li <?php if(MODULE_URL==BASE_URL.'aboutus/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>aboutus" <?php if(MODULE_URL==BASE_URL.'aboutus/'){ echo 'class="active"';}?>>Company Profile</a></li>
                     <li <?php if(MODULE_URL==BASE_URL.'mission/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>mission" <?php if(MODULE_URL==BASE_URL.'mission/'){ echo 'class="active"';}?>>Mission Statement</a></li>
                      <li <?php if(MODULE_URL==BASE_URL.'mvmc/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>mvmc" <?php if(MODULE_URL==BASE_URL.'mvmc/'){ echo 'class="active"';}?>>MVMC Leadership</a></li>
                      <li <?php if(MODULE_URL==BASE_URL.'board/'){ echo 'class="active"';}?>><a href="<?= BASE_URL ?>board" <?php if(MODULE_URL==BASE_URL.'board/'){ echo 'class="active"';}?>>MVMC Board of Directors</a></li>
</ul>
                     <div class="main_about_area"> 
                         <div class="row">
                             </div><br>
                             <h4 class="aboutus">MVMC Board of Directors</h4><br>
                             <!-- <b class="exec">EXECUTIVE COMMITTEE</b><br> -->
                             <?php  foreach ($exec->result as $row) {
                                            ?>
                                           
                                           <div class="row">
                             <div class="col-md-14">
                             <div class="col-md-4">
                                            <img class="mvmcimg" src="<?= BASE_URL ?>uploads/items/large/<?php echo $row->image ?>" align="left"></div>
                                            <div class="col-md-6">
                                            <b class="mvmc"><?php echo $row->name.', '.$row->title; ?></b><br>
                                            <p class="mvmc"><?php echo $row->content; ?></p>
                             </div>
                             </div>
                             </div><br>
                                      <?php  }
                                        ?> 
                            
                                        
                                        
                                        
<!--                                   
                                   <br><br>
                                   <b class="exec">MEDICAL EXECUTIVE COMMITTEE</b><br>
                             <?php  //foreach ($med->result as $row) {
                                            ?>
                                           
                                           <div class="row">
                             <div class="col-md-14">
                             <div class="col-md-4">
                                            <img class="mvmcimg" src="<?= BASE_URL ?>uploads/items/large/<?php //echo $row->image ?>" align="left"></div>
                                            <div class="col-md-6">
                                            <b class="mvmc"><?php //echo $row->name.', '.$row->title; ?></b><br>
                                            <p class="mvmc"><?php //echo $row->content; ?></p>
                             </div>
                             </div> </div><br>End of row -->
                                      <?php  //}
                                        ?> 
                                   </div><!-- End of main_about_area -->
                               </div><!-- End of col-sm-12 -->
                           </div><!-- End of row -->
                       </div><!-- End of Container -->
                       <hr />
                   </section><!-- End of about Section -->
           
                                         

   
                                   
