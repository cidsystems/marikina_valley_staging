<?php
class controller extends wc_controller {
	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->doctors_model	= new doctors_model();
		$this->session			= new session();
		$this->fields 			= array(
            'id',
            'firstname',
			'lastname',
			'department',
			'specialization',
			'contact',
			'schedule',
			'room',
			'affiliations',
			'image',
			'secretary'
		);
	}

	public function listing() {
		//$data['search1'] = $this->doctors_model->search($firstname,$lastname,$department,$specialization);
		$firstname = $this->input->post('firstname');
		$lastname = $this->input->post('lastname');
		$department = $this->input->post('department');
		$specialization = $this->input->post('specialization');
		$data['firstname'] = "$firstname";
		$data['ui'] = $this->ui;
		$data['show_input'] = true;
		$data['department_list'] = $this->doctors_model->getDept('');
        $data['specialization_list'] = $this->doctors_model->getSP('');
		$this->view->load('doctors',$data);
	}

	public function view($id) {
		$this->view->title = 'View Doctor';
		$data 				= (array) $this->doctors_model->getUserById($this->fields, $id);
		$ip 				= $this->doctors_model->getIP($id);
		$data['ip'] 		= $ip;
		$ip_sec 			= $this->doctors_model->getIPSecretary($id);
		$data['ip_sec'] 	= $ip_sec;
        $data['ui'] 		= $this->ui;
        $data['department_list'] = $this->doctors_model->getDept('');
        $data['specialization_list'] = $this->doctors_model->getSP('');
        $data['sched'] 		= $this->doctors_model->getSched();
		$data['show_input'] = false;
		$data['ajax_task']  = 'ajax_rate';
		$this->view->load('doc', $data);
	}
	
	public function ajax($task) {
		$ajax = $this->{$task}();
		if ($ajax) {
			header('Content-type: application/json');
			echo json_encode($ajax);
		}
	} 

	private function ajax_list() {
		$firstname = $this->input->post('firstname');
		$lastname = $this->input->post('lastname');
		$department = $this->input->post('department');
		$specialization = $this->input->post('specialization');
	
		$pagination = $this->doctors_model->search($firstname,$lastname,$department,$specialization);
	
		$table = '';
		if (empty($pagination->result)) {
			$table = '<tr><td colspan="9" class="text-center"><b>No Records Found</b></td></tr>';
		}
		foreach ($pagination->result as $row) {
			$table .= '<tr>';
			$table .= '<td><div class = "thumbnail">
			<img src="' . str_replace('/apanel', '', BASE_URL) . "uploads/items/large/".$row->image.'"></div></td>';
					$table .= '<td valign="top"><a href="'.MODULE_URL.'view/'.$row->id.'">' . '<p class="doctor1" align="left">'.strtoupper($row->lastname) .  ", " . strtoupper($row->firstname). '<br>'.$row->department. '<br>'.$row->schedule.'<br>'.$row->contact.'<br>Room '.$row->room.'</p></a><br></td>';
			$table .= '</tr>';
		  }
		  
		$pagination->table = $table;
		return $pagination;
		} 

	private function ajax_rate() {
		$id 		= $this->input->post('id');
		$rate 		= $this->input->post('rate');
		$ip_address = $this->input->post('ip_address');
		
		$result = $this->doctors_model->saveRate($id,$rate,$ip_address);

		return array(
			'redirect'	=> MODULE_URL.'view/'.$id,
			'success'	=> $result
			);
	}

	private function ajax_rate_secretary() {
		$id 		= $this->input->post('id');
		$rate 		= $this->input->post('rate');
		$ip_address = $this->input->post('ip_address');
		
		$result = $this->doctors_model->saveRateSecretary($id,$rate,$ip_address);

		return array(
			'redirect'	=> MODULE_URL.'view/'.$id,
			'success'	=> $result
			);
	}

}