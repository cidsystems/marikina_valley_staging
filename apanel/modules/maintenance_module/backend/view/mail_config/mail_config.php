<script src='https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=ch223xqxfn7t8v1kauqe1bf82k8ut75dwppo50p0d7l63eh7'></script>
<script>
	$(function () {
		tinymce.init({
			height : "50",
			selector: '#content',
			height: 500,
			theme: 'modern',
			fontsize_formats: "8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt",
			plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars code fullscreen',
			'insertdatetime media nonbreaking save table contextmenu directionality',
			'emoticons paste textcolor colorpicker textpattern imagetools'
			],
			toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | numlist bullist | fontsizeselect | fontselect',
			toolbar2: 'print preview media | forecolor backcolor emoticons',
	
			image_advtab: true,
			setup: function (editor) {
				editor.on('change', function () {
					editor.save();
				});
			},
			convert_urls: false,
      images_upload_handler: function (blobInfo, success, failure) { 
        var xhr, formData; 
        var json = '';

        xhr = new XMLHttpRequest();
        xhr.withCredentials = false; 
        xhr.open('POST', '<?=MODULE_URL?>ajax/ajax_create_upload'); 

        xhr.onload = function() {

          if (xhr.status != 200) { 
            failure('HTTP Error: ' + xhr.status); 
            return; 
          } 
					
          json = JSON.parse(xhr.response);
          success(json);
        };

        formData = new FormData(); 
        formData.append('file', blobInfo.blob(), blobInfo.filename());

        xhr.send(formData); 
      },
      setup: function (editor) {
        editor.on('change', function () {
          editor.save();
        });
      }
		});	
	});
	
</script>
	<section class="content">
		<div class="box box-primary">
			<div class="box-body">
				<br>
				<form action="" id="form" method="post" class="form-horizontal">
				<div class="row">
					<div class="col-md-6">
						<?php
							echo $ui->formField('text')
								->setLabel('Mail Server')
								->setSplit('col-md-4', 'col-md-6')
								->setName('mailserver')
								->setId('mailserver')
								->setValue((isset($fields->mailserver) ? $fields->mailserver : ''))
								->setValidation('required')
								->draw($show_input);
						?>
					</div>
                    <div class="col-md-6">
						<?php
							echo $ui->formField('text')
								->setLabel('Sender Name')
								->setSplit('col-md-2', 'col-md-6')
								->setName('servername')
								->setId('servername')
								->setValue((isset($fields->servername) ? $fields->servername : ''))
								->draw($show_input);
						?>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<?php
							echo $ui->formField('text')
							->setLabel('Username')
							->setSplit('col-md-4', 'col-md-6')
							->setName('username')
							->setId('username')
							->setValue((isset($fields->username) ? $fields->username : ''))
							->setAttribute(array("maxlength" => "250"))
							->setValidation('required')
							->draw($show_input);
						?>
					</div>
                    <div class="col-md-6">
						<?php
							echo $ui->formField('password')
								->setLabel('Password')
								->setSplit('col-md-2', 'col-md-6')
								->setName('password')
								->setId('password')
								->setValue('')
								->setValidation((isset($fields->password) ? '' : 'required'))
								->draw($show_input);
						?>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<?php
							echo $ui->formField('text')
							->setLabel('Sender Email')
							->setSplit('col-md-4', 'col-md-6')
							->setName('sender')
							->setId('sender')
							->setValue((isset($fields->sender) ? $fields->sender : ''))
							->setAttribute(array("maxlength" => "250"))
							->setValidation('required')
							->draw($show_input);
						?>
					</div>
                    <div class="col-md-6">
						<?php
							echo $ui->formField('text')
								->setLabel('Recipient Email')
								->setSplit('col-md-2', 'col-md-6')
								->setName('recipient')
								->setId('recipient')
								->setValue((isset($fields->recipient) ? $fields->recipient : ''))
								->setAttribute(array("maxlength" => "250"))
								->setValidation('required')
								->draw($show_input);
						?>
						
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<?php
							echo $ui->formField('text')
								->setLabel('Reply To Email')
								->setSplit('col-md-4', 'col-md-6')
								->setName('replyto')
								->setId('replyto')
								->setValue((isset($fields->replyto) ? $fields->replyto : ''))
								->setValidation('required')
								->draw($show_input);
						?>
					</div>
                  
				</div>
				
				<!-- <div class="row text-center"">
					<div class="col-md-12">
						<input type="button" id="test" class="btn btn-info" value="Test Configuration">
					</div>
				</div> -->

				<!-- <div class="row">
					<div class="col-md-12">
					<div class="col-md-2 text-right"><label for="">Content</label></div>
					<div class="col-md-8">
						<textarea id="content" name = "content"><?php echo ''; ?></textarea>					
					</div>
					<div class="col-md-2"></div>
					</div>
				</div> -->

			
					<hr>
					<div class="row">
						<div class="col-md-12 text-center">
							<?php echo $ui->drawSubmit($show_input); ?>
							<a href="<?=MODULE_URL?>" class="btn btn-default" data-toggle="back_page">Cancel</a>
							<input type="button" id="test" class="btn btn-info" value="Test Configuration" style="float:right">
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
	<div id="successmodal" class="modal fade" role="dialog">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title modal-success"><span class="glyphicon glyphicon-ok"></span> Success!</h4>
				</div>
				<div class="modal-body">
					<p>Successfully Tested</p>
				</div>
				<div class="modal-footer">
				<!--	<button type="button" class="btn btn-success" data-dismiss="modal">Ok</button> -->
				</div>
			</div>
		</div>
	</div>
	<div id="failedmodal" class="modal fade" role="dialog">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title modal-success"><span class="glyphicon glyphicon-warning"></span> Error in Configuration</h4>
				</div>
				<div class="modal-body">
					<p>SMTP failed to connect.</p>
				</div>
				<div class="modal-footer">
				<!--	<button type="button" class="btn btn-success" data-dismiss="modal">Ok</button> -->
				</div>
			</div>
		</div>
	</div>
	<?php if ($show_input): ?>
	<script>
		$('form').submit(function(e) {
			e.preventDefault();
			$(this).find('.form-group').find('input, textarea, select').trigger('blur');
			if ($(this).find('.form-group.has-error').length == 0) {
				$.post('<?=MODULE_URL?>ajax/<?=$ajax_task?>', $(this).serialize(), function(data) {
					if (data.success) {
						$('#delay_modal').modal('show');
							setTimeout(function() {							
								window.location = data.redirect;									
						}, 1000)	
					}
				});
			} else {
				$(this).find('.form-group.has-error').first().find('input, textarea, select').focus();
			}
		});

		$('#test').on('click', function (){
			$('form').find('.form-group').find('input, textarea, select').trigger('blur');
			if ($('form').find('.form-group.has-error').length == 0) {
				$.post('<?=MODULE_URL?>ajax/ajax_create_test', $('form').serialize(), function(data) {
					if (data.result) {
						$('#successmodal').modal('show');
							setTimeout(function() {							
								$('#successmodal').modal('hide');							
						}, 2000)	
					}
					if(data.error){
						$('#failedmodal').modal('show');
						setTimeout(function() {							
								$('#failedmodal').modal('hide');							
						}, 2000)
					}
				});
			} else {
				$('form').find('.form-group.has-error').first().find('input, textarea, select').focus();
			}
		});

	</script>
	<?php endif ?>