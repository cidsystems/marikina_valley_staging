<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->careers_model		= new careers_model();
		//$this->about			= $this->checkOutModel('about/about');
		$this->session			= new session();
		$this->fields 			= array(
			'id',
			'position',
			'jobbrief',
			'responsibilities',
			'requirements'
		);
		$this->fields1			= array(
			'id',
			'position',
			'firstname',
			'lastname',
			'address',
			'contact',
			'email',
			'message',
			'resume');
	}

	public function listing() {
		$data['careers'] = $this->careers_model->getCareersList();
		$this->view->title = 'Careers List';
		$data['ui'] = $this->ui;
		$all = (object) array('ind' => 'null', 'val' => 'Filter: All');
		$data['ajax_task'] = '';
		$this->view->load('careers_list', $data);
	}

	public function view($id) {
		$data = $this->input->post($this->fields1);
		$data = (array) $this->careers_model->getCareersById($this->fields, $id);
		$data['careers'] = $this->careers_model->getCareersList();
		$data['ui']			= $this->ui;
		$data['show_input'] = false;
		$this->view->load('careers', $data);
	}
	
	public function create($id) {
		$data = $this->input->post($this->fields1);
		$data = (array) $this->careers_model->getCareersById($this->fields, $id);
		$data['careers'] = $this->careers_model->getCareersList();
		$data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_create';
		$data['ajax_post'] = '';
		$data['show_input'] = true;
		$this->view->load('resume', $data);
	}

	public function general_resume() {
		$data = $this->input->post($this->fields1);
		// $data = (array) $this->careers_model->getCareersById($this->fields, $id);
		$data['careers'] = $this->careers_model->getCareersList();
		$data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_drop_resume';		
		$data['ajax_post'] = '';
		$data['show_input'] = true;
		$this->view->load('general_resume', $data);
	}

	public function ajax($task) {
		header('Content-type: application/json');
		$result = $this->{$task}();
		echo json_encode($result);
	}

	private function ajax_list() {
	
		$pagination = $this->careers_model->getCareersList();
	
		$table = '';
		if (empty($pagination->result)) {
			$table = '<tr><td colspan="9" class="text-center"><b>No Records Found</b></td></tr>';
		}

		foreach ($pagination->result as $row) {
			$table .= '<tr>';
			$table .= '<td class="col-md-9"> <a href="'.BASE_URL.'careers/view/'.$row->id.'">'.$row->position.'</td>';
			$table .= '<td class="col-md-8">'.date("M j, Y",strtotime($row->entereddate)).'</a></td>';
			$table .= '</tr>';
	    }
		$table .= '</tr>';
		  
		  
		$pagination->table = $table;
		return $pagination;
	} 

	public function ajax_create() {
		$data = $this->input->post($this->fields1);
		$last = $this->careers_model->getResumeLast();
		$last = $last->id;

				$position = $this->input->post('position');
				$firstname = $this->input->post('firstname');
				$lastname = $this->input->post('lastname');
				$address = $this->input->post('address');
				$contact = $this->input->post('contact');
				$email = $this->input->post('email');
				//$message = $this->input->post('message');
				$message = $_POST['message'];
				$message = mb_convert_encoding($message, "HTML-ENTITIES", "UTF-8"); 
				$message = str_replace('&Acirc;','',$message);
				$message = str_replace('&nbsp;',' ',$message);
				$resume = $this->input->post('resume');

				$data['firstname'] = str_replace('ñ', 'n', $data['firstname']);
				$data['lastname'] = str_replace('ñ', 'n', $data['lastname']);
				$firstname	= str_replace('ñ', 'n', $firstname);
				$lastname 	= str_replace('ñ', 'n', $lastname);

				$uploaddir = 'uploads/';
				$name = str_replace('ñ', 'n', $_FILES['resume']['name']);
				$name_ext = '.'.substr($name, strrpos($name, '.') + 1);
				$name = md5($name).$last.$name_ext;
				$uploadfile = $uploaddir . $name;
				$filename = basename($name);
				
				if (move_uploaded_file($_FILES['resume']['tmp_name'], $uploadfile)) {
					$name = str_replace('ñ', 'n', $_FILES['resume']['name']);
					$data['resume'] = $name;
					
				} else {
					echo "Possible file upload attack!\n";
				}

		$html = "
		<p style='font-family:Open Sans;'><b>From: </b>Marikina Valley Medical Center<br>
				<b style='font-family:Open Sans;'>To: </b>".$firstname.' '.$lastname."<br>
				<b style='font-family:Open Sans;'>Subject: </b>Marikina Valley Medical Center</p>
				<p style='font-family:Sans-serif;'>Marikina Valley Medical Center</p>
				<p style='font-family:Sans-serif;'><b>Hello ".$firstname.' '.$lastname.",</b></p>
				<p style='font-family:Sans-serif;'>This letter is to let you know that we have received your application. We appreciate your interest in Marikina Valley Medical Center and the position of ".$position." for which you applied. We are reviewing applications currently and expect to schedule interviews in the next couple of days/weeks. </p>
				<p style='font-family:Sans-serif;'>If you are selected for an interview, you can expect a phone call from our Human Resources staff shortly.</p>
				
				<p style='color:gray;font-size: 86%;font-family:Sans-serif;'>If you have some questions or you need some help. contact <a href='#' style='color:gray'>info@mvmc.com</a></p>
				<p style='color:gray;font-size: 86%;font-family:Sans-serif;'>To keep updated on what's happening.</p>
				<p style='color:gray;font-size: 86%;font-family:Sans-serif;'><u><a href='https://www.facebook.com/MarikinaValleyMedicalCenter/' style='color:gray'>Like us on Facebook</a></u></p>
		";
		
		$mail = new PHPMailer();
				$mail->IsSendmail();
				try {  
					$mail->Sender = 'admin@marikinavalleymedicalcenter.com';        
					$mail->SetFrom('hrdrecruitment@mvmc.com.ph');
					$mail->AddReplyTo('hrdrecruitment@mvmc.com.ph');
					$mail->AddAddress($email);
					$mail->Subject = "MVMC Application";      
					$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
					$mail->MsgHTML($html);
					$mail->Send();      
				} catch (phpmailerException $e) {
					$e->errorMessage();
					$json['email_sent'] = "Message Not Sent!";
				}
                
                $html1 = "
		<h1>".$position." Applicant</h1>
		<p style='font-family:Sans-serif;'><b>Name: </b><br>".$firstname.' '.$lastname."</p>
		<p style='font-family:Sans-serif;'><b>Address: </b><br>".$address."</p>
		<p style='font-family:Sans-serif;'><b>Contact: </b><br>".$contact."</p>
		<p style='font-family:Sans-serif;'><b>Email: </b><br>".$email."</p>
		<p style='font-family:Sans-serif;'><b>Message: </b><br>".stripslashes($message)."</p>
		<p style='font-family:Sans-serif;'><b>Resume: </b><br><a href='".str_replace('/apanel', '', BASE_URL)."uploads/".$filename."'>$filename</a></p>
		";
		
		$mail = new PHPMailer();
		$mail->IsSendmail();
		try {  
			$mail->Sender = 'admin@marikinavalleymedicalcenter.com';        
			$mail->SetFrom($email);
			$mail->AddReplyTo($email);
			$mail->AddAddress('hrdrecruitment@mvmc.com.ph'); 
            $mail->AddBCC('zmvilla@mvmc.com.ph');
            $mail->AddBCC('roberto.felicio@cid-systems.com');
			$mail->Subject = "Applicant";      
			$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
			$mail->MsgHTML($html1);
			$mail->Send();      
		} catch (phpmailerException $e) {
			$e->errorMessage();
			$json['email_sent'] = "Message Not Sent!";
		}

		$data['resume'] = str_replace('ñ', 'n', $data['resume']);

		$result = $this->careers_model->saveResume($data);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
		);
	   }

	   public function ajax_drop_resume() {
		$data = $this->input->post($this->fields1);
		$last = $this->careers_model->getResumeLast();
		$last = $last->id;

				$position = $this->input->post('position');
				$firstname = $this->input->post('firstname');
				$lastname = $this->input->post('lastname');
				$address = $this->input->post('address');
				$contact = $this->input->post('contact');
        		$email = $this->input->post('email');
				//$message = $this->input->post('message');
				$message = $_POST['message'];
				$message = mb_convert_encoding($message, "HTML-ENTITIES", "UTF-8"); 
				$message = str_replace('&Acirc;','',$message);
				$message = str_replace('&nbsp;',' ',$message);
				$resume = $this->input->post('resume');
				$data['position'] = 'Others';
				$data['firstname'] = str_replace('ñ', 'n', $data['firstname']);
				$data['lastname'] = str_replace('ñ', 'n', $data['lastname']);
				$firstname	= str_replace('ñ', 'n', $firstname);
				$lastname 	= str_replace('ñ', 'n', $lastname);
				$uploaddir = 'uploads/';
				$name = str_replace('ñ', 'n', $_FILES['resume']['name']);
				$name_ext = '.'.substr($name, strrpos($name, '.') + 1);
				$name = md5($name).$last.$name_ext;
				$uploadfile = $uploaddir . $name;
				$filename = basename($name);
				
				if (move_uploaded_file($_FILES['resume']['tmp_name'], $uploadfile)) {
					$name = str_replace('ñ', 'n', $_FILES['resume']['name']);
					$data['resume'] = $name;
					
				} else {
					echo "Possible file upload attack!\n";
				}
	
		$html = "
		<p style='font-family:Open Sans;'><b>From: </b>Marikina Valley Medical Center<br>
				<b style='font-family:Open Sans;'>To: </b>".$firstname.' '.$lastname."<br>
				<b style='font-family:Open Sans;'>Subject: </b>Marikina Valley Medical Center</p>
				<p style='font-family:Sans-serif;'>Marikina Valley Medical Center</p>
				<p style='font-family:Sans-serif;'><b>Hello ".$firstname.' '.$lastname.",</b></p>
				<p style='font-family:Sans-serif;'>This letter is to let you know that we have received your application. We appreciate your interest in Marikina Valley Medical Center. We are reviewing applications currently and expect to schedule interviews in the next couple of days/weeks. </p>
				<p style='font-family:Sans-serif;'>If you are selected for an interview, you can expect a phone call from our Human Resources staff shortly.</p>
				
				<p style='color:gray;font-size: 86%;font-family:Sans-serif;'>If you have some questions or you need some help. contact <a href='#' style='color:gray'>info@mvmc.com.ph</a></p>
				<p style='color:gray;font-size: 86%;font-family:Sans-serif;'>To keep updated on what's happening.</p>
				<p style='color:gray;font-size: 86%;font-family:Sans-serif;'><u><a href='https://www.facebook.com/MarikinaValleyMedicalCenter' style='color:gray'>Like us on Facebook</a></u></p>
		";
		
		$mail = new PHPMailer();
				$mail->IsSendmail();
				try {  
					$mail->Sender = $email;        
					$mail->SetFrom('hrdrecruitment@mvmc.com.ph');
					$mail->AddReplyTo('hrdrecruitment@mvmc.com.ph');
					$mail->AddAddress($email);
					$mail->Subject = "MVMC Application";      
					$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
					$mail->MsgHTML($html);
					$mail->Send();      
				} catch (phpmailerException $e) {
					$e->errorMessage();
					$json['email_sent'] = "Message Not Sent!";
				}

				$html1 = "
		<h1>Applicant</h1>
		<p style='font-family:Sans-serif;'><b>Name: </b><br>".$firstname.' '.$lastname."</p>
		<p style='font-family:Sans-serif;'><b>Address: </b><br>".$address."</p>
		<p style='font-family:Sans-serif;'><b>Contact: </b><br>".$contact."</p>
		<p style='font-family:Sans-serif;'><b>Email: </b><br>".$email."</p>
		<p style='font-family:Sans-serif;'><b>Message: </b><br>".stripslashes($message)."</p>
		<p style='font-family:Sans-serif;'><b>Resume: </b><br><a href='".str_replace('/apanel', '', BASE_URL)."uploads/".$filename."'>$filename</a></p>
		";
		
		$mail = new PHPMailer();
		$mail->IsSendmail();
		try {  
			$mail->Sender = $email;        
			$mail->SetFrom($email);
			$mail->AddReplyTo($email);
			$mail->AddAddress('hrdrecruitment@mvmc.com.ph');
			$mail->AddBCC('roberto.felicio@cid-systems.com');
			$mail->Subject = "Applicant";      
			$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
			$mail->MsgHTML($html1);
			$mail->Send();      
		} catch (phpmailerException $e) {
			$e->errorMessage();
			$json['email_sent'] = "Message Not Sent!";
		}
		
		$data['resume'] = str_replace('ñ', 'n', $data['resume']);

		$result = $this->careers_model->saveResume($data);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
		);
	   }     
	   


}



// $targetfolder = "uploads/";
		// $targetfolder = $targetfolder . basename( $_FILES['file']['name']) ;