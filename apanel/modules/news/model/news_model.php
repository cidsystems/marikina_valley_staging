<?php
class news_model extends wc_model {

    public function getNewsList($search) {
        $fields = array('id','image','title', 'content', 'pin' , 'date','status');
     $condition = '';
        if ($search) {
            $condition .= $this->generateSearch($search, array('title', 'content'));
        }
        $result = $this->db->setTable('news')
        ->setFields($fields)
        ->setWhere($condition)
        ->setOrderBy('entereddate DESC')
        ->runPagination();

        return $result;
    }
    
    private function generateSearch($search, $array) {
    $temp = array();
    foreach ($array as $arr) {
      $temp[] = $arr . " LIKE '%" . str_replace(' ', '%', $search) . "%'";
  }
  return '(' . implode(' OR ', $temp) . ')';
}

 public function getNewsListFront($fields) {


        $result = $this->db->setTable('news')
        ->setFields($fields)
        ->setWhere("status = 'Published'")
        ->setOrderBy('entereddate DESC')
        ->runPagination();
        return $result;
    }

    public function getAllNews() {
        $result = $this->db->setTable('news')
        ->setFields('id, image, title, date, content, pin')
        ->setOrderBy('date DESC, pin')
        ->runSelect()
        ->getResult();

        return $result;
    }  

   //  public function getNewsForPins($news_id) {
   //      if(empty($news_id)) {

   //         $result = $this->db->setTable('news')
   //         ->setValues(array('pin' => 'unpinned'))
   //         ->setWhere('1 = 1')
   //         ->runUpdate(); 

   //     } else {
        
   //         $ids = implode(',', $news_id);

   //         $result = $this->db->setTable('news')
   //         ->setValues(array('pin' => 'unpinned'))
   //         ->setWhere('1 = 1')
   //         ->runUpdate(); 

   //         $result = $this->db->setTable('news')
   //         ->setValues(array('pin' => 'pinned'))
   //         ->setWhere("id IN($ids)")
   //         ->setLimit(2)
   //         ->runUpdate();
   //     }

   //     return $result;
   // }

   public function getMonth() {
    $result = $this->db->setTable('news')
    ->setFields('id, date')
    //->setGroupBy('MONTH(date)')
    ->runPagination();

    return $result;
}

public function saveNews($fields) {
    $fields['module'] = 'news';
    $fields['date'] = $this->date->dateDbFormat();
    $result = $this->db->setTable('news')
    ->setValues($fields)
    ->runInsert();

    return $result;

}

public function getNewsById($fields, $id) {
  return $this->db->setTable('news')
  ->setFields($fields)
  ->setWhere("id = '$id'")
  ->setLimit(1)
  ->runSelect()
  ->getRow();
}

public function getNewsByDate($fields, $date) {
  return $this->db->setTable('news')
  ->setFields($fields)
  ->setWhere("MONTH(date) = '$date'")
  ->runSelect()
  ->getResult();
}

public function getLatestNews() {
    $result = $this->db->setTable('news')
    ->setFields('id, date, title, content, image','entereddate')
	->setWhere("status = 'Published'")
    ->setOrderBy('entereddate DESC')
    ->setLimit(1) 
    -> runSelect() 
    ->getResult();


    return $result;
}

public function updateNews($data, $id) {

  $result = $this->db->setTable('news')
  ->setValues($data)
  ->setWhere("id = '$id'")
  ->setLimit(1)
  ->runUpdate();

  return $result;
}

public function deleteNews($data) {
  $error_id = array();
  foreach ($data as $id) {
     $result =  $this->db->setTable('news')
     ->setWhere("id = '$id'")
     ->runDelete();
 }

 return $error_id;
}


}