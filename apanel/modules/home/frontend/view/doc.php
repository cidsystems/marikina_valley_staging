<section class="content">
<div class="box box-primary">
    <div class="box-body">
        <br>
        <form action="" method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-11">
                    <div class="row">
                        <div class="col-md-4 text-center">
                            Doctor Details <br><br>
                            <div align="right">
                            <img class="img4" src="<?= BASE_URL ?>uploads/items/large/<?php echo $image ?>">
                        </div>
                        
                        </div>
                        <div class="col-md-6"><br><br><br>
                            <h4 class="doc"><?php echo $lastname.', '.$firstname ?></h4>
                            <h4 class="doc"><?php echo $specialization ?></h4>
                            <b class="doctor">Room: <?php echo $room ?></b><br>
                            <b class="doctor">Schedule: <?php echo $schedule ?></b><br>
                            <p class="doctor"><i>Schedules may change without prior notice due to Medical Conventions and
                                other events.</i></p>
                            <b class="doctor">Telephone: <?php echo $contact ?></b><br>
                            <b class="doctor">HMO Affiliations: <?php echo $affiliations ?></b><br><br><br><br>
                            For emergencies, please call 682-2222 <br><br>
                            
                        </div>

            
        </div>                  
    </div>
</div>
</div>
</div>
</section>
