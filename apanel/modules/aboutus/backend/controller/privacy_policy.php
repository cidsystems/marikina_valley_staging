<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->aboutus_model	= new aboutus_model();
		$this->session			= new session();
		$this->fields 			= array(
			'id',
			'content',
			'category'
			);
	}

	public function listing() {
		$this->view->title = 'Privacy Policy';
        $data = (array) $this->aboutus_model->getPPContent();
		$data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_edit';
		$data['show_input'] = true;
		$this->view->load('privacy_policy', $data);
	}
	
	public function ajax($task) {
		$ajax = $this->{$task}();
		if ($ajax) {
			header('Content-type: application/json');
			echo json_encode($ajax);
		}
	}

	private function ajax_edit() {

		$data = $this->input->post($this->fields);
		$textarea = addslashes($_POST['mytextarea']);		
		$data['content'] = $textarea;
		$data['category'] = 'Privacy Policy';

        $id = $this->input->post('id');
        
		$result = $this->aboutus_model->updatePPContent($data, $id);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
			);
	}



}