 <!-- About Section -->
 <section id="aboutus" class="aboutus">
 <div class="container">
     <div class="row">
         <div class="col-sm-12">
             <div class="head_title text-center margin-top-80">
             <!-- <img class="img2" src="<?php echo BASE_URL; ?>assets/home/images/home.jpg" alt="" />
                 <h1 >Careers</h1> -->
             </div><!-- End of head title -->

             <div class="main_about_area"> 
             
                 <div class="row">

                     <div class="col-sm-3">
                         <div class="signle_about_left">
                             <div class="single_about_left_text">
                                <!--  <div class="separator2"></div> -->
                                <h4 class="careers"><a href="<?php echo BASE_URL;?>careers/">Careers<a></h4>
                                <hr class="divider">
                                <?php foreach($careers->result as $row){
                                 ?><h6><a href="<?= BASE_URL ?>careers/view/<?= $row->id ?>"><?php echo $row->position.'<br>'?></h6></a>
                                 <?php } ?><br>
                             </div>
                       




                             

                            <!--  <a href="" class="btn btn-default">get in touch</a> -->
                         </div>
                     </div>
                     <div class="modal fade mod" id="yow" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="exampleModalLabel">Thank you.</h5>
        <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
        <!--  <span aria-hidden="true">&times;</span>-->
        <!--</button>-->
      </div>
      <div class="modal-body">
      <span>Your application has been sent.</span>
        </div>
      <div class="modal-footer">
        <button type="button" class = "btn" id = "sara">Okay</button>
      </div>
    </div>
  </div>
</div>     
                     <div class="col-sm-9">
                         <div class="signle_about_left">
                             <div class="single_about_left_text">
                                <!--  <div class="separator2"></div> -->
                                <h4 class="careers"><?php echo 'General Resume'; ?></h4>
                                <hr class="divider">
                                <div class="row">
                                 <div class="col-md-11">
                                     <div class="row">
                                         <div class="col-md-12">
                                         <form method="POST" id="form">
                                         <div class="row"><div class="col-md-12"><div class="col-md-4">
                                         <input type="hidden" class="input2" name="position" value="<?php echo $position ?>">
                                         <span class="required"> *</span><label>First Name:</label></div><div class="col-sm-1">
                                         <input type="text" class="input2" name="firstname" required></div></div></div><br>
                                         <div class="row"><div class="col-md-12"><div class="col-md-4">
                                         <span class="required"> *</span><label>Last Name: </label></div><div class="col-sm-1">
                                         <input type="text" class="input2" name="lastname" required></div></div></div><br>
                                         <div class="row"><div class="col-md-12"><div class="col-md-4">
                                         <span style="color:white;"> *</span><label>Address: </label></div><div class="col-sm-1">
                                         <input type="text" class="input2" name="address"></div></div></div><br>
                                         <div class="row"><div class="col-md-12"><div class="col-md-4">
                                         <span style="color:white;"> *</span><label>Telephone: </label></div><div class="col-sm-1">
                                         <input type="text" class="input2" name="contact"></div></div></div><br>
                                         <div class="row"><div class="col-md-12"><div class="col-md-4">
                                         <span class="required"> *</span><label>Email: </label></div><div class="col-sm-1">
                                         <input type="email" class="input2" name="email" required></div></div></div><br>
                                         <div class="row"><div class="col-md-12"><div class="col-md-4">
                                         <span style="color:white;"> *</span><label>Message: </label></div><div class="col-sm-1">
                                         <textarea class="textarea" name="message" id="" cols="21" rows="5"></textarea></div></div></div><br>
                                         <div class="row"><div class="col-md-12"><div class="col-md-4">
                                         <label><span class="required"> *</span>Upload Resume - <br>PDF or Word File: </label></div><div class="col-sm-1">
                                         <input id="resume" name="resume" type="file" required accept=".docx, .doc, .pdf"></div></div></div><br>
                                         <div class="row"><div class="col-md-12"><div class="col-md-14"><div class="col-sm-10">
                                         <input type="submit" name="submit" class="btn btn-default" value="Submit"></div></div></div><br>
                                     </form>




                        </div>
                 </div>
             </div>
         </div><!-- End of col-sm-12 -->
     </div><!-- End of row -->
 </div><!-- End of Container -->
 <hr /><br>
</section><!-- End of about Section -->


<script>
$('form').submit(function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    $('#form [name="submit"]').val('Sending...').attr('disabled', true);
     $.ajax({
         url: '<?=MODULE_URL?>ajax/<?=$ajax_task?>',
         type: "POST",
         data: formData,
         processData: false,
         contentType: false,
         success: function(data) {
            $('#form [name="submit"]').val('Submit').attr('disabled', false);
            $('#yow').modal('show');
         }
     });
});
$('#sara').on('click', function() {
    $(this).closest('.modal').modal('hide');
    window.location = '<?php echo FULL_URL ?>';
});
</script>