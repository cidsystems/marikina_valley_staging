<style>
.stars-container {
	font-size: 140%;
    position: relative;
    display: inline-block;
    color: transparent;
  }
  
  .stars-container:before {
    position: absolute;
    top: 0;
    left: 0;
    content: '★★★★★';
    color: lightgray;
  }
  
  .stars-container:after {
    position: absolute;
    top: 0;
    left: 0;
    content: '★★★★★';
    color: gold;
    overflow: hidden;
  }
  
  .stars-0:after { width: 0%; }
  .stars-10:after { width: 10%; }
  .stars-20:after { width: 20%; }
  .stars-30:after { width: 30%; }
  .stars-40:after { width: 40%; }
  .stars-50:after { width: 50%; }
  .stars-60:after { width: 60%; }
  .stars-70:after { width: 70%; }
  .stars-80:after { width: 80%; }
  .stars-90:after { width: 90%; }
  .stars-100:after { width: 100; }

</style>
<section class="content">
		<div class="box box-primary">
			<div class="box-header pb-none">
				<div class="row">
					<div class="col-md-8">
						<div class="row">
							<div class="col-md-4">
								<!-- <div class="form-group">
									<a href="<?= MODULE_URL ?>create" class="btn btn-primary">Create Newsletter</a>
									<button type="button" id="item_multiple_delete" class="btn btn-danger delete_button">Delete<span></span></button>
								</div> -->
							</div>
						</div>
					</div>
					
				</div>
				<div class="row">
					<div class="col-md-12 ">
						<div class="row">
							<div class="col-sm-8 col-xs-6">
							<div class="col-md-4">
								<?php
									echo $ui->formField('text')
									->setName('daterangefilter')
									->setId('daterangefilter')
									->setAttribute(array('data-daterangefilter' => 'month'))
									->setAddon('calendar')
									->setValue($datefilter)
									->setValidation('required')
									->draw(true);
								?>
							</div>
							<div class="col-md-1 text-right">
								<a href="" id="export_csv" download="DoctorsRating.csv" class="btn btn-primary"><span class="glyphicon glyphicon-export"></span> Export</a>
							</div>
								<!-- <label for="" class="padded col-md-offset-6 text-right">Items: </label> -->
							</div>
							<!-- <div class="col-sm-1 col-xs-6 col-md-offset-3">
								<div class="form-group">
									<select id="items">
										<option value="10">10</option>
										<option value="20">20</option>
										<option value="50">50</option>
										<option value="100">100</option>
									</select>
								</div>
							</div> -->
						</div>
					</div>
				</div>
			</div>
			<div class="box-body table-responsive no-padding">
				<table id="tableList" class="table table-hover table-sidepad">
					<?php
						echo $ui->loadElement('table')
								->setHeaderClass('info')
								// ->addHeader(
								// 	'<input type="checkbox" class="checkall">',
								// 	array(
								// 		'class' => 'text-center',
								// 		'style' => 'width: 15px'
								// 	)
								// )
								->addHeader('Doctor', array('class' => 'col-md-3'), 'sort', 'lastname')
								->addHeader('Doctors Rating', array('class' => 'col-md-3'), 'sort', 'rating')
								->addHeader('No. of Raters', array('class' => 'col-md-3'), 'sort', 'count')
								->draw();
					?>
					<tbody>
 
					</tbody>
				</table>

			</div>
		</div>
		<div id="pagination"></div>
	</section>

	<div class="modal fade mod" id="raters" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
		<span>Raters</span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
		<table id="tableList1" class="table table-hover table-sidepad">
					<?php
						echo $ui->loadElement('table')
								->setHeaderClass('info')
								->addHeader('Rate', array('class' => 'col-md-3'), 'sort', 'rating')
								->addHeader('IP Address', array('class' => 'col-md-3'), 'sort', 'ip_address')
								->addHeader('Date', array('class' => 'col-md-3'), 'sort', 'entereddate')
								->draw();
					?>
					<tbody>
					
					</tbody>
				</table>
				<div id="pagination1"></div>
        </div>
        </div>
    </div>
  </div>
</div>       

	<div class="delete-modal">
		<div class="modal modal-danger">
			<div class="modal-dialog" style = "width: 300px;">
				<div class="modal-content">
					<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Confirmation</h4>
					</div>
					<div class="modal-body">
					<p>Are you sure you want to delete this record?</p>
					</div>
					<div class="modal-footer text-center">
						<button type="button" class="btn btn-outline btn-flat" id = "delete-yes">Yes</button>
						<button type="button" class="btn btn-outline btn-flat" data-dismiss="modal">No</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- <div id="fixture"></div> -->
		


<script>

		var ajax = filterFromURL();
		var ajax_call = '';
		ajaxToFilter(ajax, { search : '#table_search', limit : '#items' });
	
		tableSort('#tableList', function(value, getlist) {
			ajax.sort = value;
			ajax.page = 1;
			if (getlist) {
				getList();
			}
		});
		
		$('#items').on('change', function() {
			ajax.limit = $(this).val();
			ajax.page = 1;
			getList();
		});
		$('#pagination').on('click', 'a', function(e) {
			e.preventDefault();
			ajax.page = $(this).attr('data-page');
			getList();
		});


		$('#tableList').on('click','a', function() {
			var id = $(this).attr('data-id');
			var datefilter = $('#daterangefilter').val();
			$.post('<?=MODULE_URL?>ajax/ajax_list1', 'daterangefilter=' + datefilter + '&id='+ id, function(data) {
				$('#tableList1 tbody').html(data.table);
				$('#pagination1').html(data.pagination);
				var content = data.rate;	
				var a = 1;
				content.forEach(function(element) {
					$("<span class='stars-container'>")
						.addClass("stars-" + element)
						.text("★★★★★")
						.appendTo($('#fixtures'+a));
						a+=1;	
				});
			});
		});
		
		function getList() {
			filterToURL();
			if (ajax_call != '') {
				ajax_call.abort();
			}
			ajax_call = $.post('<?=MODULE_URL?>ajax/ajax_list', ajax, function(data) {
				$('#tableList tbody').html(data.table);
				$('#pagination').html(data.pagination);
				$("#export_csv").attr('href', 'data:text/csv;filename=testing.csv;charset=utf-8,' + encodeURIComponent(data.csv));
				var content = data.rate;	
				var a = 1;
				content.forEach(function(element) {
					$("<span class='stars-container'>")
						.addClass("stars-" + element)
						.text("★★★★★")
						.appendTo($('#fixture'+a));
						a+=1;	
				});
				if (ajax.page > data.page_limit && data.page_limit > 0) {
					ajax.page = data.page_limit;
					getList();
				}
			});
		}
		getList();

		$("#daterangefilter").on("change",function(){
		ajax.daterangefilter = $(this).val();
		ajax.page = 1;
		getList();
		}).trigger('change');

	</script>