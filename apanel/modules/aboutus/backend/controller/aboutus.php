<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->aboutus_model	= new aboutus_model();
		$this->session			= new session();
		$this->fields 			= array(
			'id',
			'content',
			'category'
			);
	}

	public function listing() {
		$this->view->title = 'About';
		$data['ui'] = $this->ui;
		$all = (object) array('ind' => 'null', 'val' => 'Filter: All');
		$data['category_list'] = array_merge(array($all),  $this->aboutus_model->getCategory(''));
		$this->view->load('aboutus_list', $data);
	}

	public function create() {
		$this->view->title = 'Add Content';
		$data = $this->input->post($this->fields);
		$data['ui'] = $this->ui;
		$data['category_list'] = $this->aboutus_model->getCategory('');
		$data['ajax_task'] = 'ajax_create';
		$data['ajax_post'] = '';
		$data['show_input'] = true;
		$this->view->load('aboutus', $data);
	}

	public function edit($id) {
		$this->view->title = 'Edit Content';
		$data = (array) $this->aboutus_model->getContentById($this->fields, $id);
		$data['ui'] = $this->ui;
		$data['category_list'] = $this->aboutus_model->getCategory('');
		$data['ajax_task'] = 'ajax_edit';
		$data['ajax_post'] = "&id=$id";
		$data['show_input'] = true;
		$this->view->load('aboutus', $data);
	}

	public function edit1() {
		$this->view->title = 'Edit Content';
		$data = (array) $this->aboutus_model->getVideo1($this->fields);
		$data['ui'] = $this->ui;
		$data['ajax_task'] = 'ajax_edit1';
		$data['ajax_post'] = "&id=";
		$data['show_input'] = true;
		$this->view->load('video', $data);
	}

	public function view($id) {
		$this->view->title = 'View Content';
		$data = (array) $this->aboutus_model->getContentById($this->fields, $id);
		$data['category_list'] = $this->aboutus_model->getCategory('');
		$data['ui'] = $this->ui;
		$data['show_input'] = false;
		$this->view->load('aboutus', $data);
	}
	
	public function ajax($task) {
		$ajax = $this->{$task}();
		if ($ajax) {
			header('Content-type: application/json');
			echo json_encode($ajax);
		}
	}

	public function ajax_list() {
		$pagination = $this->aboutus_model->getContent();
		$table = '';
		
		foreach($pagination->result as $row) {
			$dropdown = $this->ui->loadElement('check_task')
			->addView()
			->addEdit()
			->addPrint()
			->addDelete()
			->addCheckbox()
			->setValue($row->id)
			->draw();

			$table .= '<tr>';
			$table .= '<td align = "center">' . $dropdown . '</td>';
			$table .= '<td>' . $row->content . '</td>';
			$table .= '<td>' . $row->category . '</td>';
			$table .= '</tr>';
		}

		$pagination->table = $table;

		return $pagination;
	}

	private function ajax_create() {
		$data = $this->input->post($this->fields);
		$textarea = $_POST['mytextarea'];		
		$data['content'] = $textarea;
		$result = $this->aboutus_model->saveContent($data);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
			);
	}

	private function ajax_edit() {

		$data = $this->input->post($this->fields);
		$textarea = $_POST['mytextarea'];		
		$data['content'] = $textarea;

		$id = $this->input->post('id');
		$result = $this->aboutus_model->updateContent($data, $id);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
			);
	}

	private function ajax_edit1() {
		$data = $this->input->post($this->fields);
		$id = $this->input->post('id');
		$result = $this->aboutus_model->updateContent1($data, $id);
		return array(
			'redirect'	=> MODULE_URL,
			'success'	=> $result
			);
	}

	private function ajax_delete() {
		$delete_id = $this->input->post('delete_id');
		$error_id = array();
		if ($delete_id) {
			$error_id = $this->aboutus_model->deleteContent($delete_id);
		}
		return array(
			'success'	=> (empty($error_id)),
			'error_id'	=> $error_id
			);
	}


}