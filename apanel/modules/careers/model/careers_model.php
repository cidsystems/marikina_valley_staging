<?php
class careers_model extends wc_model {

	public function __construct() {
		parent::__construct();
		$this->log = new log();
	}

	public function saveCareers($data) {
		$result = $this->db->setTable('careers')
							->setValues($data)
							->runInsert();

		return $result;
    }
	
	public function saveResume($data) {

		// fix for special character "A"
		$data['message'] = mb_convert_encoding($data['message'], "HTML-ENTITIES", "UTF-8"); 
		$data['message'] = str_replace('&Acirc;','',$data['message']);
		$data['message'] = str_replace('&nbsp;',' ',$data['message']);

		// fix resume randomized name and extension
		$last = $this->getResumeLast();
		$last = $last->id;
		$ext = '.'.substr($data['resume'], strrpos($data['resume'], '.') + 1);
		$data['resume'] = md5($data['resume']).$last.$ext;

		$result = $this->db->setTable('resume')
							->setValues($data)
							->runInsert();

		return $result;
    }
    
	public function updateCareers($data, $id) {
		$result = $this->db->setTable('careers')
							->setValues($data)
							->setWhere("id = '$id'")
							->setLimit(1)
							->runUpdate();

		if ($result) {
			$this->log->saveActivity("Update Careers [$id]");
		}

		return $result;
    }
    
	public function deleteCareers($data) {
		$error_id = array();
		foreach ($data as $id) {
			$result =  $this->db->setTable('careers')
								->setWhere("id = '$id'")
								->setLimit(1)
								->runDelete();
		
			if ($result) {
				$this->log->saveActivity("Delete Item Type [$id]");
			} else {
				if ($this->db->getError() == 'locked') {
					$error_id[] = $id;
				}
			}
		}

		return $error_id;
	}
	
	public function deleteResume($data) {
		$error_id = array();
		foreach ($data as $id) {
			$result =  $this->db->setTable('resume')
								->setWhere("id = '$id'")
								->setLimit(1)
								->runDelete();
		
			if ($result) {
				$this->log->saveActivity("Delete Item Type [$id]");
			} else {
				if ($this->db->getError() == 'locked') {
					$error_id[] = $id;
				}
			}
		}

		return $error_id;
    }
    
   

	public function getCareersById($fields, $id) {
		return $this->db->setTable('careers')
						->setFields($fields)
						->setWhere("id = '$id'")
						->setLimit(1)
						->runSelect()
						->getRow();
	}
	
	public function getResumeById($fields, $id) {
		return $this->db->setTable('resume')
						->setFields($fields)
						->setWhere("id = '$id'")
						->setLimit(1)
						->runSelect()
						->getRow();
	}
	
	public function getResumeLast() {
		return $this->db->setTable('resume')
						->setFields('id')
						->setOrderBy('id desc')
						->setLimit(1)
						->runSelect()
						->getRow();
    }
    

	public function getCareersList() {
		$result = $this->db->setTable('careers')
					->setFields('id,position,jobbrief,responsibilities,requirements,entereddate')
					->setOrderBy('entereddate DESC')
                    ->runPagination();

            return $result;

	}
	
	public function getResumeList() {
		$result = $this->db->setTable('resume')
					->setFields('id,position,firstname,lastname,address,contact,email,message,resume')
					->setOrderBy('position')
                    ->runPagination();

            return $result;

    }
public function getResumeList1($search, $sort) {
		$sort		= ($sort) ? $sort : 'entereddate DESC';
		$result = $this->db->setTable('resume')
					->setFields('id,position,firstname,lastname,address,contact,email,message,resume,entereddate')
					->setOrderBy($sort)
                    ->runPagination();

            return $result;

    }

private function generateSearch($search, $array) {
		$temp = array();
		foreach ($array as $arr) {
			$temp[] = $arr . " LIKE '%" . str_replace(' ', '%', $search) . "%'";
		}
		return '(' . implode(' OR ', $temp) . ')';
	}

}