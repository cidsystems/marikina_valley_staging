 <!-- About Section -->
 <section id="aboutus" class="aboutus">
 <div class="container">
     <div class="row">
         <div class="col-sm-12">
             <div class="head_title text-center margin-top-60">
             <!-- <img class="img2" src="<?php echo BASE_URL; ?>assets/home/images/home.jpg" alt="" />
                 <h1>Careers</h1> -->
             </div><!-- End of head title -->

             <div class="main_about_area"> 
             
              
             <div class="col-md-14">
             <h4 class="h4 text-center">Our Doctors</h4><br><br>
             <div class="single_about">
              <br>
              <div class="row">
              <hr class="divider">
                        <form action="" method="post" id="form">
                            <div class="panel4"><br><br>
                            <div class="row">
                            <div class="col-sm-14">
                        <?php
                            echo $ui->formField('text')
                            ->setLabel('First Name')
                            ->setSplit('col-md-2', 'col-md-3')
                            ->setName('firstname')
                            ->setId('firstname')
                            ->setClass('input2')
                            ->setPlaceholder('First Name')
                            ->setValidation('required')
                            ->draw($show_input);
                        ?>
                            
                           
                            
                        <?php
                            echo $ui->formField('text')
                            ->setLabel('Last Name')
                            ->setSplit('col-md-2', 'col-md-3')
                            ->setName('lastname')
                            ->setId('lastname')
                            ->setClass('input2')
                            ->setPlaceholder('Last Name')
                            ->setValidation('required')
                            ->draw($show_input);
                        ?>
                            </div></div>
                        <div class="row">
                            <div class="col-sm-14">
                        <?php
                            echo $ui->formField('dropdown')
                            ->setLabel('Department')
                            ->setPlaceholder('Select Group')
                            ->setSplit('col-md-2', 'col-md-3')
                            ->setName('department')
                            ->setId('department')
                            ->setClass('input2')
                            ->setList($department_list)
                            //->setValue($department)
                            ->setValidation('required')
                            ->draw($show_input);
                       
                            echo $ui->formField('dropdown')
                                ->setLabel('Specialization')
                                ->setPlaceholder('Select Group')
                                ->setSplit('col-md-2', 'col-md-3')
                                ->setName('specialization')
                                ->setId('specialization')
                                ->setClass('input2')
                                ->setList($specialization_list)
                                // ->setValue($specialization)
                                ->setValidation('required')
                                ->draw($show_input);
                                
									?>
                            </div></div><br>
                            <div class="row">
                                <div class="col-md-14">

									<?php
                                    //$schedules = explode('/', $schedule);
                                    // foreach($sched as $row){
									// 	echo $ui->formField('checkbox')
                                    //         ->setLabel($row->day_code)
                                    //         ->setSplit('col-md-1', 'col-md-1')
                                    //         ->setName('schedule[]')
                                    //         ->setId($row->id)
                                    //         //->setValue((in_array($row->day_code, $schedules)) ? $row->day_code : '')
                                    //         ->setDefault($row->day_code)
                                    //         ->draw($show_input);
                                    //     }
									?>
                                </div><button name="search1" class="btn btn-default" style="margin-right: 105px;">Search</button></div>
                                <br>
                        </div>                
                        </div><br><br>
             </form>
            
            <?php 
                if(isset($_POST['search'])){ ?>
                    <br><br>
                    <div id="mixcontent" class="mixcontent">
                        
                    <?php 
                                        foreach($search as $row){
                                            $name = $row->lastname.', '.$row->firstname;
                                            $department = $row->department;
                                            $specialization = $row->specialization;
                                            $schedule = $row->schedule;
                                            $image = $row->image;
                                            $contact = $row->contact;
                                    ?>
                    <a href="<?= MODULE_URL ?>view/<?= $row->id ?>">
                    <div class="panel7">
                         <div class="row">
                         <div class="col-sm-3" align="center">
                     <img class="img5" src="<?= BASE_URL ?>uploads/items/large/<?php echo $image ?>">              
                          </div>
                      <div class="col-sm-6"><br>
                      <?php echo $name ?><br>
                      <?php echo $specialization ?><br>
                      Schedule: <?php echo $schedule ?><br>
                      Contact: <?php echo $contact ?>
                      </div>                
                                          </div><hr/></div></a>
                          <?php } ?>
                                          
                                          </div><br><br>
                           
      
                        
                      </div>                     
                  </div><!-- End of row -->
              <?php  } 
             ?>
             <?php
             if(isset($_POST['search1'])){ ?>
                    <br><br>
                    <div id="mixcontent" class="mixcontent">
                        
                    <?php 
                                        foreach($search1 as $row){
                                            $name = $row->lastname.', '.$row->firstname;
                                            $department = $row->department;
                                            $specialization = $row->specialization;
                                            $schedule = $row->schedule;
                                            $image = $row->image;
                                            $contact = $row->contact;
                                    ?>
                    <a href="<?= MODULE_URL ?>view/<?= $row->id ?>">
                    <div class="panel7">
                         <div class="row">
                         <div class="col-sm-3" align="center">
                     <img class="img5" src="<?= BASE_URL ?>uploads/items/thumb/<?php echo $image ?>">              
                          </div>
                      <div class="col-sm-6"><br>
                      <?php echo $name ?><br>
                      <?php echo $specialization ?><br>
                      Schedule: <?php echo $schedule ?><br>
                      Contact: <?php echo $contact ?>
                      </div>                
                                          </div><hr/></div></a>
                          <?php }  }?>
                                          
                                          </div><br><br>
                           
      
                         <!--  <a href="" class="btn">learn more</a> -->
                      </div>                     
                  </div><!-- End of row -->
               
            </div><!-- End of Contaier -->
        </section><!-- End of portfolio Section -->   

        <!-- <script>
  $('form').submit(function(e) {
    e.preventDefault();
    $.post('<?=BASE_URL?>ajax/<?=$ajax_task?>', $(this).serialize() + '<?=$ajax_post?>', function(data) {
        if (data.success) {
            window.location = data.redirect;
        }
    });
});
</script> -->