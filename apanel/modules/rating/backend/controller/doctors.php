<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->doctors_rating	= new doctors_rating();
		$this->input			= new input();
		$this->fields			= array('rating','ip_address');
	}

	public function listing() {
		$this->view->title	= 'Doctors Rating';
		$data['datefilter'] 		= $this->date->datefilterMonth();
		$data['ui']			= $this->ui;
		$data['show_input'] = true;
		$this->view->load('doctors_rating', $data);
	}

	public function ajax($ajax_function) {
		$result = $this->{$ajax_function}();
		header('Content-type: application/json');
		echo json_encode($result);
	}	

	private function ajax_list() {
		$sort = $this->input->post('sort');
		$datefilter = $this->input->post('daterangefilter');
		$pagination = $this->doctors_rating->getRating($sort,$datefilter);
		$table = '';
		
		if (empty($pagination->result)) {
			$table = '<tr><td colspan="5" class="text-center"><b>No Records Found</b></td></tr>';
		}
			$rate = array();
			$a = 1;
			foreach($pagination->result as $row) {
			$dropdown = $this->ui->loadElement('check_task')
			->addView()
			->addEdit()
			->addPrint()
			->addDelete()
			->addCheckbox()
			->setValue($row->rating)
			->draw();
			$table .= '<tr>
							<td>' . 'Dr. '.$row->lastname.', '. $row->firstname . '</td>
							<td class="rate">' . '<div id="fixture'.$a.'"></div>' . '</td>
							<td class="text-left"><a data-toggle="modal" id="ra" href="#raters" data-id="'.$row->doctor_id.'">' . $row->count . '</a></td>
						</tr>';
			$rate[] = number_format(round($row->rating*20, -1));
			$a++;
		} 
		$pagination->table = $table;
		$pagination->rate  = $rate;
		$pagination->csv	= $this->get_export();

		return $pagination;
	}

	private function ajax_list1() {
		$id = $this->input->post('id');
		$sort = $this->input->post('sort');
		$datefilter = $this->input->post('daterangefilter');
		$pagination = $this->doctors_rating->getRatingDetails($id,$sort,$datefilter);
		$table = '';
		
		if (empty($pagination->result)) {
			$table = '<tr><td colspan="5" class="text-center"><b>No Records Found</b></td></tr>';
		}
			$rate = array();
			$a = 1;
			foreach($pagination->result as $row) {
			$dropdown = $this->ui->loadElement('check_task')
			->addView()
			->addEdit()
			->addPrint()
			->addDelete()
			->addCheckbox()
			->setValue($row->rating)
			->draw();
			$table .= '<tr>
							<td class="rate">' . '<div id="fixtures'.$a.'"></div>' . '</td>	
							<td>' . $row->ip_address . '</td>
							<td>' . $this->date->dateFormat($row->entereddate) . '</td>
						</tr>';
			$rate[] = number_format(round($row->rating*20, -1));
			$a++;
		} 
		$pagination->table = $table;
		$pagination->rate  = $rate;

		return $pagination;
	}

	private function get_export() {
		$sort = $this->input->post('sort');
		$datefilter = $this->input->post('daterangefilter');
		$result = $this->doctors_rating->getRatingCsv($sort,$datefilter);

		$header = array(
			'Doctor',
			'Doctors Rating',
			'No. of Raters'
		);

		$csv = '';
		$csv .= 'Doctors Ratings';
		$csv .= "\n\n";
		$csv .= '"Date:","' . $datefilter . '"';
		$csv .= "\n\n";
		$csv .= '"' . implode('","', $header) . '"';
		if (empty($result)) {
			$csv .= "\n";
			$csv .= '"",';
			$csv .= '"No Records Found"';
		}
		
		foreach ($result as $key => $row) {
			$csv .= "\n";
			$csv .= '"' . 'Dr. '.$row->lastname.', '. $row->firstname . '",';
			$csv .= '"' . number_format($row->rating, 2) . '",';
			$csv .= '"' . $row->count . '",';
		}
		
		// var_dump($csv);
		
		return $csv;
	}
	
}