<?php
class controller extends wc_controller {

	public function __construct() {
		parent::__construct();
		$this->ui				= new ui();
		$this->input			= new input();
		$this->patients_model	= new patients_model();
		$this->session			= new session();
		$this->fields 			= array(
			'id',
			'directory',
			'content'
			
		);
	}

	public function listing() {
		$data['main'] = $this->patients_model->getMain($this->fields);
		$data['medical'] = $this->patients_model->getMedical($this->fields);
		$this->view->title	= 'Patients & Visitors';
		$data['ui']			= $this->ui;
		$this->view->load('directory', $data);
	}
}